﻿using System;
using System.Threading;
using System.Threading.Tasks;

using Youbora.chrono;
using Youbora.log;

namespace Youbora.timer
{
    /// <summary>
    /// Instances of this class will call a callback every setted interval.
    /// </summary>
    public class Timer
    {
        /// <summary>
        /// Interval, in ms.
        /// </summary>
        public int Interval { get; set; }

        /// <summary>
        /// True if the Timer is running.
        /// </summary>
        public bool IsRunning { get; set; }

        /// <summary>
        /// Internal chrono for measuring time spent between ticks.
        /// </summary>
        public Chrono Chrono = new Chrono();

        private CancellationTokenSource cts;
        private SendOrPostCallback callback;
        private SynchronizationContext context;

        /// <summary>
        /// Constructor. 5000ms interval by default.
        /// </summary>
        /// <param name="callback">Callback</param>
        public Timer(SendOrPostCallback callback) : this(callback, 5000) { }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="callback">Callback</param>
        /// <param name="interval">Interval, in ms.</param>
        public Timer(SendOrPostCallback callback, int interval)
        {
            this.callback = callback;
            this.Interval = interval;
            this.IsRunning = false;
        }

        /// <summary>
        /// Starts the timer.
        /// </summary>
        public void Start()
        {
            try
            {
                this.IsRunning = true;

                // Renew token
                this.cts?.Dispose();
                this.cts = new CancellationTokenSource();

                // Save main context
                this.context = SynchronizationContext.Current;

                // Set first tick
                this.SetNextTick();

            }
            catch (Exception ex)
            {
                Log.Exception(ex);
            }
        }

        /// <summary>
        /// Stops the timer.
        /// </summary>
        public void Stop()
        {
            try
            {
                this.IsRunning = false;
                this.cts?.Cancel(false);
            }
            catch (Exception ex)
            {
                Log.Exception(ex);
            }
        }

        private async void SetNextTick()
        {
            try
            {
                if (this.IsRunning)
                {
                    // Start the chrono
                    this.Chrono.Start();

                    // wait interval
                    await Task.Delay(this.Interval, this.cts.Token).
                        ContinueWith((t) => {
                            // Invoke callback in the main thread
                            if (callback != null)
                            {
                                if (context == null)
                                {
                                    callback.Invoke(this.Chrono.Stop());
                                }
                                else
                                {
                                    context.Post(callback, this.Chrono.Stop());
                                }
                            }
                        }, CancellationToken.None,
                        TaskContinuationOptions.ExecuteSynchronously | TaskContinuationOptions.OnlyOnRanToCompletion,
                        TaskScheduler.Default
                    );

                    // Set next ping
                    this.SetNextTick();
                }
            }
            catch (TaskCanceledException) { /* Timer stoped. Do nothing*/ }
            catch (Exception ex)
            {
                Log.Exception(ex);
            }
        }
    }
}
