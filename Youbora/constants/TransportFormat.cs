﻿namespace Youbora.constants
{
    public static class TransportFormat
    {
        public static string MP4 = "MP4";
        public static string MPEG2 = "TS";
        public static string CMF = "CMF";
    }
}
