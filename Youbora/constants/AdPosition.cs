﻿namespace Youbora.constants
{
    public class AdPosition
    {
        public static string Preroll = "pre";
        public static string Midroll = "mid";
        public static string Postroll = "post";
    }
}
