﻿namespace Youbora.constants
{      
    /**
    * List of will-send events.
    *
    * @memberof youbora.Plugin
    * @enum
    * @event
    */
    public class WillSendEvent
    {
        public static string WILL_SEND_INIT = "will-send-init";
        public static string WILL_SEND_START = "will-send-start";
        public static string WILL_SEND_JOIN = "will-send-join";
        public static string WILL_SEND_PAUSE = "will-send-pause";
        public static string WILL_SEND_RESUME = "will-send-resume";
        public static string WILL_SEND_SEEK = "will-send-seek";
        public static string WILL_SEND_BUFFER = "will-send-buffer";
        public static string WILL_SEND_ERROR = "will-send-error";
        public static string WILL_SEND_FATAL_ERROR = "will-send-fatal-error";
        public static string WILL_SEND_STOP = "will-send-stop";
        public static string WILL_SEND_PING = "will-send-ping";
        public static string WILL_SEND_VIDEO_EVENT = "will-send-video-event";

        public static string WILL_SEND_AD_INIT = "will-send-ad-init";
        public static string WILL_SEND_AD_START = "will-send-ad-start";
        public static string WILL_SEND_AD_JOIN = "will-send-ad-join";
        public static string WILL_SEND_AD_PAUSE = "will-send-ad-pause";
        public static string WILL_SEND_AD_RESUME = "will-send-ad-resume";
        public static string WILL_SEND_AD_BUFFER = "will-send-ad-buffer";
        public static string WILL_SEND_AD_STOP = "will-send-ad-stop";
        public static string WILL_SEND_AD_CLICK = "will-send-ad-click";
        public static string WILL_SEND_AD_ERROR = "will-send-ad-error";
        public static string WILL_SEND_AD_MANIFEST = "will-send-ad-manifest";
        public static string WILL_SEND_AD_POD_START = "will-send-ad-break-start";
        public static string WILL_SEND_AD_POD_STOP = "will-send-ad-break-stop";
        public static string WILL_SEND_AD_QUARTILE = "will-send-ad-quartile";

        public static string WILL_SEND_SESSION_START = "will-send-session-start";
        public static string WILL_SEND_SESSION_STOP = "will-send-session-stop";
        public static string WILL_SEND_NAV = "will-send-nav";
        public static string WILL_SEND_BEAT = "will-send-beat";
        public static string WILL_SEND_EVENT = "will-send-event";

        public static string WILL_SEND_OFFLINE_EVENTS = "will-send-offline-event";
    }
}
