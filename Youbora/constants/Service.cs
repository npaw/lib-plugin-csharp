﻿namespace Youbora.constants
{
    public static class Service
    {
        public static string DATA = "/data";

        // Video
        public static string INIT = "/init";
        public static string START = "/start";
        public static string JOIN = "/joinTime";
        public static string PAUSE = "/pause";
        public static string RESUME = "/resume";
        public static string SEEK = "/seek";
        public static string BUFFER = "/bufferUnderrun";
        public static string ERROR = "/error";
        public static string STOP = "/stop";
        public static string PING = "/ping";
        public static string VIDEO_EVENT = "/infinity/video/event";

        // Ads
        public static string AD_INIT = "/adInit";
        public static string AD_START = "/adStart";
        public static string AD_JOIN = "/adJoin";
        public static string AD_PAUSE = "/adPause";
        public static string AD_RESUME = "/adResume";
        public static string AD_BUFFER = "/adBufferUnderrun";
        public static string AD_STOP = "/adStop";
        public static string AD_CLICK = "/adClick";
        public static string AD_ERROR = "/adError";
        public static string AD_MANIFEST = "/adManifest";
        public static string AD_POD_START = "/adBreakStart";
        public static string AD_POD_STOP = "/adBreakStop";
        public static string AD_QUARTILE = "/adQuartile";

        // Infinity
        public static string EVENT = "/infinity/event";
        public static string SESSION_START = "/infinity/session/start";
        public static string SESSION_STOP = "/infinity/session/stop";
        public static string NAV = "/infinity/session/nav";
        public static string BEAT = "/infinity/session/beat";

        // Offline
        public static string OFFLINE_EVENTS = "/offlineEvents";
    }
}
