﻿namespace Youbora.constants
{
    public static class Event
    {
        public static string INIT = "init";
        public static string START = "start";
        public static string JOIN = "join";
        public static string PAUSE = "pause";
        public static string RESUME = "resume";
        public static string SEEK_BEGIN = "seek-begin";
        public static string SEEK_END = "seek-end";
        public static string BUFFER_BEGIN = "buffer-begin";
        public static string BUFFER_END = "buffer-end";
        public static string ERROR = "error";
        public static string STOP = "stop";
        public static string CLICK = "click";
        public static string MANIFEST = "manifest";
        public static string PODSTART = "break-start";
        public static string PODSTOP = "break-stop";
        public static string QUARTILE = "quartile";
        public static string VIDEO_EVENT = "video-event";
    }
}
