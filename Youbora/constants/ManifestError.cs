﻿namespace Youbora.constants
{
    public class ManifestError
    {
        public static string NO_RESPONSE = "NO_RESPONSE";
        public static string EMPTY = "EMPTY_RESPONSE";
        public static string WRONG = "WRONG_RESPONSE";
    }
}
