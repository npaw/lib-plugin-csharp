﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using Youbora.emitter;
using Youbora.log;

namespace Youbora.plugin
{
    public partial class Plugin : Emitter
    {
        /**
          * Returns ads"s PlayerVersion
          *
          * @memberof youbora.Plugin.prototype
          */
        public string GetAdPlayerVersion()
        {
            string ret = "";
            if ( adsAdapter != null)
            {
                try
                {
                    ret = adsAdapter.GetPlayerVersion();
                }
                catch (Exception err)
                {
                    Log.Warn("An error occured while calling getAdPlayerVersion: {0}", err.ToString());
              }
            }
            return ret;
        }

        /**
        * Returns ad"s position
        *
        * @memberof youbora.Plugin.prototype
        */
        public string GetAdPosition()
        {
            string ret = "pre";
            if (adsAdapter != null)
            {
                try
                {
                    ret = adsAdapter.GetPosition();
                }
                catch (Exception err)
                {
                    Log.Warn("An error occured while calling getAdPosition: {0}", err.ToString());
              }
            }
            if (ret == null && adapter != null)
            {
                ret = (adapter.flags.isJoined) ? "mid" : "pre";
            }
            return ret;
        }


        /**
        * Returns ad"s position number
        *
        * @memberof youbora.Plugin.prototype
        */
        public int GetAdNumber()
        {
            int ret = 0;
                if (requestBuilder.lastSent.ContainsKey("adNumber")) {
                    ret = (int)requestBuilder.lastSent["adNumber"];
                }
            return ret;
        }


        /**
        * Returns break"s position number
        *
        * @memberof youbora.Plugin.prototype
        */
        public int GetBreakNumber()
        {
            int ret = 0;
            if (requestBuilder.lastSent.ContainsKey("breakNumber"))
            {
                ret = (int)requestBuilder.lastSent["breakNumber"];
            }
            return ret;
        }


        /**
        * Returns ad"s AdPlayhead
        *
        * @memberof youbora.Plugin.prototype
        */
        public double? GetAdPlayhead()
        {
            double? ret = null;
            if (adsAdapter != null)
            {
                try
                {
                    ret = Math.Round(adsAdapter.GetPlayhead());
                }
                catch (Exception err)
                {
                    Log.Warn("An error occured while calling getAdPlayhead: {0}", err.ToString());
              }
            }
            return ret;
        }

        /**
        * Returns ad"s AdDuration
        *
        * @memberof youbora.Plugin.prototype
        */
        public double? GetAdDuration()
        {
            double? ret = null;
            if (adsAdapter != null)
            {
                try
                {
                    ret = adsAdapter.GetDuration();
                }
                catch (Exception err)
                {
                    Log.Warn("An error occured while calling getAdDuration: {0}", err.ToString());
              }
            }
            return ret;
        }

        /**
        * Returns ad"s AdBitrate
        *
        * @memberof youbora.Plugin.prototype
        */
        public double? GetAdBitrate()
        {
            double? ret = null;
            if (adsAdapter != null)
            {
                try
                {
                    ret = adsAdapter.GetBitrate();
                }
                catch (Exception err)
                {
                    Log.Warn("An error occured while calling getAdBitrate: {0}", err.ToString());
              }

            }
            return ret;
        }

        /**
        * Returns ad"s AdTitle
        *
        * @memberof youbora.Plugin.prototype
        */
        public string GetAdTitle()
        {
            string ret = null;
            if ((string)options["ad.title"] != null)
            {
                return (string) options["ad.title"];
            }
            if (adsAdapter != null)
            {
                try
                {
                    ret = adsAdapter.GetTitle();
                }
                catch (Exception err)
                {
                    Log.Warn("An error occured while calling getAdTitle: {0}", err.ToString());
              }
            }
            return ret;
        }

        /**
        * Returns ad"s AdResource
        *
        * @memberof youbora.Plugin.prototype
        */
        public string GetAdResource()
        {
            string ret = null;
            if ((string)options["ad.resource"] != null)
            {
                return (string) options["ad.resource"];
            }
            if (adsAdapter != null)
            {
                try
                {
                    ret = adsAdapter.GetResource();
                }
                catch (Exception err)
                {
                    Log.Warn("An error occured while calling getAdResource: {0}", err.ToString());
                }
            }
            return ret;
        }

        /**
        * Returns ad"s campaign
        *
        * @memberof youbora.Plugin.prototype
        */
        public string GetAdCampaign()
        {
            if ((string)options["ad.campaign"] != null)
            {
                return (string) options["ad.campaign"];
            }
            return null;
        }

        /**
        * Returns ad"s creative id
        *
        * @memberof youbora.Plugin.prototype
        */
        public string GetAdCreativeId()
        {
            string ret = null;
            if ((string)options["ad.creativeId"] != null)
            {
                return (string)options["ad.creativeId"];
            }
            if (adsAdapter != null)
            {
                try
                {
                    ret = adsAdapter.GetCreativeId();
                }
                catch (Exception err)
                {
                    Log.Warn("An error occured while calling getCreativeId: {0}", err.ToString());
                }
            }
            return ret;
        }

        /**
        * Returns ad"s provider
        *
        * @memberof youbora.Plugin.prototype
        */
        public string GetAdProvider()
        {
            string ret = null;
            if ((string)options["ad.provider"] != null)
            {
                return (string)options["ad.provider"];
            }
            if (adsAdapter != null)
            {
                try
                {
                    ret = adsAdapter.GetProvider();
                }
                catch (Exception err)
                {
                    Log.Warn("An error occured while calling getProvider: {0}", err.ToString());
                }
            }
            return ret;
        }

        /**
        * Returns ads adapter getVersion or null
        *
        * @memberof youbora.Plugin.prototype
        */
        public string GetAdAdapterVersion()
        {
            string ret = null;
            if (adsAdapter != null)
            {
                try
                {
                    ret = adsAdapter.GetVersion();
                }
                catch (Exception err)
                {
                    Log.Warn("An error occured while calling getAdsAdapterVersion: {0}", err.ToString());
              }
            }
            return ret;
         }

        /**
        * Returns ad"s AdMetadata
        *
        * @memberof youbora.Plugin.prototype
        */
        public string GetAdMetadata()
        {
            return (string) options["ad.metadata"];
        }


        public int? GetGivenBreaks()
        {
            int? ret = null;
            if (options["ad.givenBreaks"] != null)
            {
                ret = (int)options["ad.givenBreaks"];
            }
            else if (adsAdapter != null)
            {
                try
                {
                    ret = adsAdapter.GetGivenBreaks();
                }
                catch (Exception err)
                {
                    Log.Warn("An error occured while calling getGivenBreaks: {0}", err);
                }
            }
            return ret;
        }

        public int? GetExpectedBreaks()
        {
            int? ret = null;
            if (options["ad.expectedBreaks"] != null)
            {
                ret = (int)options["ad.expectedBreaks"];
            }
            else if (options["ad.expectedPattern"] != null)
            {
                ret = 0;
                try
                {
                    Dictionary<string, List<int>> expPattern = (Dictionary<string, List<int>>)options["ad.expectedPattern"];
                    ret = expPattern.ContainsKey("pre") ? expPattern["pre"].Count : 0;
                    ret = expPattern.ContainsKey("mid") ? expPattern["mid"].Count : 0;
                    ret = expPattern.ContainsKey("post") ? expPattern["post"].Count : 0;
                }
                catch (Exception err)
                {
                    Log.Warn("An error occured while getting expectedPattern, wrong format: {0}", err);
                }
            }
            else if (adsAdapter != null)
            {
                try
                {
                    ret = adsAdapter.GetExpectedBreaks();
                }
                catch (Exception err)
                {
                    Log.Warn("An error occured while calling expectedBreaks {0}", err);
                }
            }
            return ret;
        }

        public string GetExpectedPattern()
        {
            string ret = null;
            if (options["ad.expectedPattern"] != null)
            {
                try
                {
                    ret = JsonConvert.SerializeObject(options["ad.expectedPattern"]);
                }
                catch (Exception err)
                {
                    Log.Warn("An error occured while getting expectedPattern option, wrong format: {0}", err);
                }
            }
            else if (adsAdapter != null)
            {
                try
                {
                    ret = JsonConvert.SerializeObject(adsAdapter.GetExpectedPattern());
                }
                catch (Exception err)
                {
                    Log.Warn("An error occured while calling expectedPattern {0}", err);
                }
            }
            return ret;
        }

        public string GetBreaksTime()
        {
            string ret = null;
            if (options["ad.breaksTime"] != null)
            {
                ret = JsonConvert.SerializeObject(options["ad.breaksTime"]);
            }
            else if (adsAdapter != null)
            {
                try
                {
                    ret = JsonConvert.SerializeObject(adsAdapter.GetBreaksTime());
                }
                catch (Exception err)
                {
                    Log.Warn("An error occured while calling breaksTime: {0}", err);
                }
            }
            return ret;
        }

        public int? GetGivenAds()
        {
            int? ret = null;
            if (options["ad.givenAds"] != null)
            {
                ret = (int)options["ad.givenAds"];
            }
            else if (adsAdapter != null)
            {
                try
                {
                    ret = adsAdapter.GetGivenAds();
                }
                catch (Exception err)
                {
                    Log.Warn("An error occured while calling givenAds: {0}", err);
                }
            }
            return ret;
        }

        public int? GetExpectedAds()
        {
            int? ret = null;
            try
            {
                if (adsAdapter != null)
                {
                    if (options["ad.expectedPattern"] != null && GetAdPosition() != null)
                    {
                        List<int> list = new List<int>();
                        Dictionary<string, List<int>> expPattern = (Dictionary<string, List<int>>) options["ad.expectedPattern"];
                        if (expPattern.ContainsKey("pre")) list.AddRange(expPattern["pre"]);
                        if (expPattern.ContainsKey("mid")) list.AddRange(expPattern["mid"]);
                        if (expPattern.ContainsKey("post")) list.AddRange(expPattern["post"]);
                        if (list.Count > 0)
                        {
                            int position = (int) requestBuilder.lastSent["breakNumber"];
                            if (position > list.Count) position = list.Count;
                            ret = list[position - 1];
                        }
                    }
                    else
                    {
                        ret = adsAdapter.GetExpectedAds();
                    }
                }
            }
            catch (Exception err)
            {
                Log.Warn("An error occured while calling expectedAds: {0}", err);
            }
            return ret;
        }

        public bool? GetAdsExpected()
        {
            bool? ret = null;
            try
            {
                ret = (GetExpectedPattern() != null || GetGivenAds() != null) || false;
            }
            catch (Exception err)
            {
                Log.Warn("An error occured while calling givenAds or expectedPattern {0}", err);
            }
            return ret;
        }

        public bool? GetAudioEnabled()
        {
            bool? ret = null;
            try
            {
                if (adsAdapter != null)
                {
                    ret = adsAdapter.GetAudioEnabled();
                }
            }
            catch (Exception err)
            {
                Log.Warn("An error occured while calling isAudioEnabled{0}", err);
            }
            return ret;
        }

        public bool? GetIsSkippable()
        {
            bool? ret = null;
            try
            {
                if (adsAdapter != null)
                {
                    ret = adsAdapter.GetIsSkippable();
                }
            }
            catch (Exception err)
            {
                Log.Warn("An error occured while calling isSkippable{0}", err);
            }
            return ret;
        }

        public bool? GetIsFullscreen()
        {
            bool? ret = null;
             try
             {
                if (adsAdapter != null)
                {
                    ret = adsAdapter.GetIsFullscreen();
                }
             }
             catch (Exception err)
             {
                Log.Warn("An error occured while calling isFullcreen{0}", err);
             }
            return ret;
        }

        // ----------------------------------------- CHRONOS ------------------------------------------

        /**
        * Returns AdJoinDuration chrono delta time
        *
        * @memberof youbora.Plugin.prototype
        */
        public double GetAdJoinDuration()
        {
            return (adsAdapter != null) ? adsAdapter.chronos.join.GetDeltaTime(false) : -1;
        }

        /**
        * Returns AdBufferDuration chrono delta time
        *
        * @memberof youbora.Plugin.prototype
        */
        public double GetAdBufferDuration()
        {
            return (adsAdapter != null) ? adsAdapter.chronos.buffer.GetDeltaTime(false) : -1;
        }

        /**
         * Returns AdPauseDuration chrono delta time
         *
         * @memberof youbora.Plugin.prototype
         */
        public double GetAdPauseDuration()
        {
            return (adsAdapter != null) ? adsAdapter.chronos.pause.GetDeltaTime(false) : 0;
        }

        /**
         * Returns total totalAdDuration chrono delta time
         *
         * @memberof youbora.Plugin.prototype
         */
        public double GetAdTotalDuration()
        {
            return (adsAdapter != null) ? adsAdapter.chronos.total.GetDeltaTime(false) : -1;
        }


        public double GetAdViewedDuration()
        {
            double maxTime = 0;
            if (adsAdapter != null && adsAdapter.chronos.viewedMax.Count > 0)
            {
                int count = adsAdapter.chronos.viewedMax.Count;
                for (int i = 0; i<count; i++)
                {
                    maxTime += adsAdapter.chronos.viewedMax[i].GetDeltaTime(false);
                }
            }
            return maxTime;
        }

        public double GetAdViewability()
        {
            double maxTime = 0;
            if (adsAdapter != null && adsAdapter.chronos.viewedMax.Count > 0)
            {
                int count = adsAdapter.chronos.viewedMax.Count;
                for (int i = 0; i < count; i++)
                {
                    maxTime = Math.Max(maxTime,adsAdapter.chronos.viewedMax[i].GetDeltaTime(false));
                }
            }
            return maxTime;
        }
    }
}
