﻿using System.Collections.Generic;
using System.Reflection;
using Youbora.log;

namespace Youbora.plugin.requestBuilder
{
    public class RequestBuilder
    {
        private static string[] startParams ={
            "accountCode",
            "username",
            "anonymousUser",
            "rendition",
            "player",
            "title",
            "title2",
            "live",
            "mediaDuration",
            "mediaResource",
            "playhead",
            "parsedResource",
            "transactionCode",
            "properties",
            "cdn",
            "playerVersion",
            "player",
            "param1",
            "param2",
            "param3",
            "param4",
            "param5",
            "param6",
            "param7",
            "param8",
            "param9",
            "param10",
            "param11",
            "param12",
            "param13",
            "param14",
            "param15",
            "param16",
            "param17",
            "param18",
            "param19",
            "param20",
            "obfuscateIp",
            "p2pEnabled",
            "pluginVersion",
            "pluginInfo",
            "isp",
            "connectionType",
            "ip",
            "deviceInfo",
            "deviceUUID",
            "userType",
            "streamingProtocol",
            "transportFormat",
            "householdId",
            "adsBlocked",
            "smartswitchConfigCode",
            "smartswitchGroupCode",
            "smartswitchContractCode",
            "libVersion",
            "nodeHost",
            "nodeType",
            "appName",
            "appReleaseVersion",
            "saga",
            "tvshow",
            "season",
            "titleEpisode",
            "channel",
            "imdbID",
            "gracenoteID",
            "contentType",
            "genre",
            "contentLanguage",
            "subtitles",
            "cost",
            "price",
            "playbackType",
            "email",
            "drm",
            "videoCodec",
            "audioCodec",
            "codecSettings",
            "codecProfile",
            "containerFormat",
            "contentId",
            "contractedResolution",
            "linkedViewID",
            "youboraId"
        };

        private static string[] adStartParams = {
              "playhead",
              "adTitle",
              "position",
              "adDuration",
              "adCampaign",
              "adResource",
              "adPlayerVersion",
              "player",
              "playerVersion",
              "adProperties",
              "adAdapterVersion",
              "adProvider",
              "adCreativeId",
              "extraparam1",
              "extraparam2",
              "extraparam3",
              "extraparam4",
              "extraparam5",
              "extraparam6",
              "extraparam7",
              "extraparam8",
              "extraparam9",
              "extraparam10",
              "fullscreen",
              "audio",
              "skippable",
              "adNumber",
              "breakNumber"
        };

        public static Dictionary<string, string[]> parameters = new Dictionary<string, string[]>()
        {
            {"/data", new string[] {"system","pluginVersion","requestNumber","username"} },
            {"/init", startParams},
            {"/start", startParams },
            {"/joinTime",new string[] {"joinDuration","playhead"}},
            {"/pause", new string[] {"playhead"}},
            {"/resume", new string[] {"pauseDuration", "playhead"}},
            {"/seek", new string[] {"seekDuration", "playhead"}},
            {"/bufferUnderrun", new string[] {"bufferDuration", "playhead"}},
            {"/error", startParams },
            {"/stop", new string[] {"bitrate", "totalBytes", "playhead", "pauseDuration"}},
            {"/infinity/video/event", new string[]{ } },

            {"/adInit", adStartParams},
            {"/adStart", adStartParams},
            {"/adJoin", new string[] {"playhead", "position", "adJoinDuration", "adPlayhead", "adNumber", "breakNumber"}},
            {"/adPause", new string[] {"playhead", "position", "adPlayhead", "adNumber", "breakNumber"}},
            {"/adResume",new string[]  {"playhead", "position", "adPlayhead", "adPauseDuration", "adNumber", "breakNumber"}},
            {"/adBufferUnderrun", new string[] {"playhead", "position", "adPlayhead", "adBufferDuration", "adNumber", "breakNumber"}},
            {"/adStop", new string[] {"playhead", "position", "adPlayhead", "adBitrate", "adTotalDuration", "pauseDuration", "adViewedDuration", "adViewability", "adNumber", "breakNumber"}},
            {"/adClick",new string[] {"playhead", "position", "adPlayhead", "adNumber", "breakNumber"}},
            {"/adError", adStartParams},
            {"/adManifest", new string[]{ "givenBreaks", "expectedBreaks", "expectedPattern", "breaksTime"} },
            {"/adBreakStart", new string[]{ "position", "givenAds", "expectedAds", "breakNumber" } },
            {"/adBreakStop", new string[]{ "breakNumber" } },
            {"/adQuartile", new string[] { "position", "adViewedDuration", "adViewability", "adNumber", "breakNumber" } },

            {"/ping", new string[] {"droppedFrames", "playrate", "cdnDownloadedTraffic", "p2pDownloadedTraffic", "uploadTraffic", "latency", "packetLoss", "packetSent", "metrics", "totalBytes"}},

            {"/infinity/session/start", new string[]{
                "accountCode",
                "username",
                "anonymousUser",
                "navContext",
                "route",
                "page" ,
                "language",
                "deviceInfo",
                "adsBlocked",
                "libVersion",
                "appName",
                "appReleaseVersion",
                "isp",
                "ip",
                "connectionType",
                "userType",
                "obfuscateIp",
                "deviceUUID"}
            },
            {"/infinity/session/stop", new string[] { } },
            {"/infinity/session/nav",new string[] {"navContext", "route", "page"}},
            {"/infinity/session/beat", new string[]{ "sessionMetrics" } },
            {"/infinity/event", new string[]{"accountCode", "navContext"} }
        };

        public static Dictionary<string, string[]> differentParams = new Dictionary<string, string[]>()
        {
            { "entities",new string[] {
                    "rendition",
                    "title",
                    "title2",
                    "connectionType",
                    "cdn",
                    "nodeHost",
                    "nodeType",
                    "subtitles",
                    "contentLanguage",
                    "param1",
                    "param2",
                    "param3",
                    "param4",
                    "param5",
                    "param6",
                    "param7",
                    "param8",
                    "param9",
                    "param10",
                    "param11",
                    "param12",
                    "param13",
                    "param14",
                    "param15",
                    "param16",
                    "param17",
                    "param18",
                    "param19",
                    "param20",
                    "extraparam1",
                    "extraparam2",
                    "extraparam3",
                    "extraparam4",
                    "extraparam5",
                    "extraparam6",
                    "extraparam7",
                    "extraparam8",
                    "extraparam9",
                    "extraparam10"
                }
            }
        };

        public Dictionary<string, string> GetGetters()
        {
            return Getters;
        }

        public static Dictionary<string, string> Getters = new Dictionary<string, string>()
        {
            {"requestNumber","GetRequestNumber"},
            {"playhead","GetPlayhead"},
            {"playrate","GetPlayrate"},
            {"fps","GetFramesPerSecond"},
            {"droppedFrames","GetDroppedFrames"},
            {"mediaDuration","GetDuration"},
            {"bitrate","GetBitrate"},
            {"totalBytes", "GetTotalBytes"},
            {"throughput","GetThroughput"},
            {"rendition","GetRendition"},
            {"title","GetTitle"},
            {"title2","GetTitle2"},
            {"live","GetIsLive"},
            {"mediaResource","GetResource"},
            {"parsedResource", "GetParsedResource"},
            {"transactionCode","GetTransactionCode"},
            {"properties","GetMetadata"},
            {"playerVersion","GetPlayerVersion"},
            {"player","GetPlayerName"},
            {"cdn","GetCdn"},
            {"pluginVersion","GetPluginVersion"},
            {"libVersion","GetLibVersion"},
            {"userType","GetUserType"},
            {"streamingProtocol","GetStreamingProtocol"},
            {"transportFormat","GetTransportFormat" },
            {"obfuscateIp","GetObfuscateIp"},
            {"householdId","GetHouseholdId"},
            {"latency","GetLatency"},
            {"packetLoss","GetPacketLoss"},
            {"packetSent","GetPacketSent"},
            {"metrics", "GetVideoMetrics" },

            {"param1","GetExtraparam1"},
            {"param2","GetExtraparam2"},
            {"param3","GetExtraparam3"},
            {"param4","GetExtraparam4"},
            {"param5","GetExtraparam5"},
            {"param6","GetExtraparam6"},
            {"param7","GetExtraparam7"},
            {"param8","GetExtraparam8"},
            {"param9","GetExtraparam9"},
            {"param10","GetExtraparam10"},
            {"param11","GetExtraparam11"},
            {"param12","GetExtraparam12"},
            {"param13","GetExtraparam13"},
            {"param14","GetExtraparam14"},
            {"param15","GetExtraparam15"},
            {"param16","GetExtraparam16"},
            {"param17","GetExtraparam17"},
            {"param18","GetExtraparam18"},
            {"param19","GetExtraparam19"},
            {"param20","GetExtraparam20"},

            {"extraparam1","GetAdExtraparam1"},
            {"extraparam2","GetAdExtraparam2"},
            {"extraparam3","GetAdExtraparam3"},
            {"extraparam4","GetAdExtraparam4"},
            {"extraparam5","GetAdExtraparam5"},
            {"extraparam6","GetAdExtraparam6"},
            {"extraparam7","GetAdExtraparam7"},
            {"extraparam8","GetAdExtraparam8"},
            {"extraparam9","GetAdExtraparam9"},
            {"extraparam10","GetAdExtraparam10"},

            {"position","GetAdPosition"},
            {"adPlayhead","GetAdPlayhead"},
            {"adDuration","GetAdDuration"},
            {"adCampaign","GetAdCampaign"},
            {"adCreativeId", "GetAdCreativeId"},
            {"adBitrate","GetAdBitrate"},
            {"adTitle","GetAdTitle"},
            {"adResource","GetAdResource"},
            {"adPlayerVersion","GetAdPlayerVersion"},
            {"adProperties","GetAdMetadata"},
            {"adAdapterVersion","GetAdAdapterVersion"},
            {"givenBreaks", "GetGivenBreaks"},
            {"expectedBreaks","GetExpectedBreaks"},
            {"expectedPattern","GetExpectedPattern"},
            {"breaksTime","GetBreaksTime"},
            {"givenAds","GetGivenAds"},
            {"expectedAds","GetExpectedAds"},
            {"adsExpected","GetAdsExpected"},
            {"adViewedDuration","GetAdViewedDuration"},
            {"adViewability","GetAdViewability"},
            {"fullscreen","GetIsFullscreen"},
            {"audio","GetAudioEnabled"},
            {"skippable","GetIsSkippable"},
            {"adProvider", "GetAdProvider"},
            {"adNumber", "GetAdNumber" },
            {"breakNumber", "GetBreakNumber" },

            {"pluginInfo","GetPluginInfo"},

            {"isp","GetIsp"},
            {"connectionType","GetConnectionType"},
            {"ip","GetIp"},
            
            {"deviceInfo", "GetDeviceInfo"},
            {"deviceType","GetDeviceType"},
            {"deviceName","GetDeviceName"},
            {"osName","GetOsName"},
            {"osVersion","GetOsVersion"},
            {"model","GetDeviceModel"},
            {"brand","GetDeviceBrand"},

            {"system","GetAccountCode"},
            {"accountCode","GetAccountCode"},
            {"username","GetUsername"},
            {"youboraId","GetYouboraId"},

            {"joinDuration","GetJoinDuration"},
            {"bufferDuration","GetBufferDuration"},
            {"seekDuration","GetSeekDuration"},
            {"pauseDuration","GetPauseDuration"},

            {"adJoinDuration","GetAdJoinDuration"},
            {"adBufferDuration","GetAdBufferDuration"},
            {"adPauseDuration","GetAdPauseDuration"},
            {"adTotalDuration","GetAdTotalDuration"},

            {"nodeHost","GetNodeHost"},
            {"nodeType","GetNodeType"},
            {"nodeTypeString","GetNodeTypeString"},

            {"sessionId","GetSessionId"},
            {"navContext","GetContext"},
            {"page","GetPageName"},
            {"route", "GetPageName"},
            {"language", "GetLanguage"},
            {"anonymousUser", "GetAnonymousUser" },

            {"cdnDownloadedTraffic","GetCdnTraffic"},
            {"p2pDownloadedTraffic","GetP2PTraffic"},
            {"p2pEnabled","GetIsP2PEnabled"},
            {"uploadTraffic","GetUploadTraffic"},

            {"deviceUUID", "GetDeviceUUID" },
            {"sessionMetrics", "GetSessionMetrics" },

            {"adsBlocked", "GetIsBlocked" },
            {"linkedViewID", "GetLinkedViewID" },

            {"smartswitchConfigCode", "GetSmartswitchConfigCode" },
            {"smartswitchGroupCode", "GetSmartswitchGroupCode" },
            {"smartswitchContractCode", "GetSmartswitchContractCode" },

            {"appName", "GetAppName"},
            {"appReleaseVersion", "GetAppReleaseVersion"},

            {"saga", "GetSaga"},
            {"tvshow", "GetTvShow"},
            {"season", "GetSeason"},
            {"titleEpisode", "GetEpisodeTitle"},
            {"channel", "GetChannel"},
            {"drm", "GetDRM"},
            {"videoCodec", "GetVideoCodec"},
            {"audioCodec", "GetAudioCodec"},
            {"codecSettings", "GetCodecSettings"},
            {"codecProfile", "GetCodecProfile"},
            {"containerFormat", "GetContainerFormat"},
            {"contentId", "GetID"},
            {"imdbID", "GetImdbId"},
            {"gracenoteID", "GetGracenoteID"},
            {"contentType", "GetContentType"},
            {"genre", "GetGenre"},
            {"contentLanguage", "GetVideoLanguage"},
            {"subtitles", "GetSubtitles"},
            {"contractedResolution", "GetContractedResolution"},
            {"cost", "GetCost"},
            {"price", "GetPrice"},
            {"playbackType", "GetPlaybackType"},
            {"email", "GetEmail"}
        };

        Plugin pluginRef = null;
        public int adNumber = 0;
        public Dictionary<string, object> lastSent = new Dictionary<string,object>();
        public RequestBuilder(Plugin plugin)
        {
            pluginRef = plugin;
        }

        public Dictionary<string, object> FetchParams(Dictionary<string, object> paramObject, string[] paramList)
        {
            return FetchParams(paramObject, paramList, false);
        }

        public Dictionary<string, object> FetchParams(Dictionary<string, object> paramObject, string[] paramList, bool onlyDifferent)
        {
            if (paramObject == null)
            {
                paramObject = new Dictionary<string, object>();
            }
            if (paramList.Length > 0)
            {
                for (int i = 0; i < paramList.Length; i++)
                {
                    string param = paramList[i];

                    string getterName = Getters[param];

                    var methods = pluginRef.GetType().GetTypeInfo().DeclaredMethods;
                    MethodInfo getterMethod = null;
                    foreach (MethodInfo mi in methods)
                    {
                        if (mi.Name == getterName)
                        {
                            getterMethod = mi;
                        }
                    }
                    if (getterMethod != null)
                    {
                        try
                        {
                            object value = getterMethod.Invoke(pluginRef, null);
                            if (value != null && (!onlyDifferent || !lastSent.ContainsKey(param) || !lastSent.ContainsKey(param) || lastSent[param].ToString() != value.ToString()))
                            {
                                paramObject[param] = value;
                                lastSent[param] = value;
                            }
                        }
                        catch
                        {
                            paramObject[param] = null;
                            lastSent[param] = null;
                        }
                    }
                    else
                    {
                        Log.Warn("Trying to call undefined getter " + param + "," + getterName);
                    }
                    
                }
            }
            return paramObject;
        }

        public Dictionary<string, object> BuildParams(Dictionary<string, object> paramObject, string service)
        {
            paramObject = FetchParams(paramObject, parameters[service], false);
            if (differentParams.ContainsKey(service))
            {
                paramObject = FetchParams(paramObject, differentParams[service], false);
            }
            return paramObject;
        }

        public int GetNewAdNumber()
        {
            int adNumber = 0;
            if (lastSent.ContainsKey("adNumber"))
            {
                adNumber = (int) lastSent["adNumber"];
            }
            if ((adNumber != 0) && (lastSent.ContainsKey("position")) && ((string)lastSent["position"] == pluginRef.GetAdPosition()))
            {
                adNumber += 1;
            }
            else
            {
                adNumber = 1;
            }
            lastSent["adNumber"] = adNumber;
            return adNumber;
        }

        public int GetNewBreakNumber()
        {
            int breakNumber = 1;
            if (lastSent.ContainsKey("breakNumber"))
            {
                breakNumber = (int)lastSent["breakNumber"] + 1;
            }
            lastSent["breakNumber"] = breakNumber;
            return breakNumber;
        }

        public Dictionary<string, object> GetChangedEntities()
        {
            return FetchParams(null, differentParams["entities"], true);
        }

    }
}
