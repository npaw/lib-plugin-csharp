﻿using System;
using System.Collections.Generic;
using Youbora.emitter;
using Youbora.constants;
using Youbora.adapter;
using Youbora.log;
using Newtonsoft.Json;
using Youbora.chrono;

namespace Youbora.plugin
{
    public partial class Plugin : Emitter
    {
        private bool adInitSent = false;
        private bool adStartSent = false;
        private Dictionary<string, object> savedAdError = null;
        private DateTime lastAdErrorTime = DateTime.Now;
        private Dictionary<string, object> lastAdErrorParams = new Dictionary<string, object>();
        private bool adConnected = false;
        private DateTime? adConnectedTime = null;
        /**
        * Returns current adapter or null.
        *
        * @returns {Adapter}
        *
        * @memberof youbora.Plugin.prototype
        */
        public Adapter GetAdsAdapter()
        {
            return adsAdapter;
        }

        /**
        * Sets an adapter for ads.
        *
        * @param {Adapter} adsAdapter
        *
        * @memberof youbora.Plugin.prototype
        */
        public void SetAdsAdapter(Adapter adsAdapter)
        {
            if (adsAdapter.plugin != null)
            {
                Log.Warn("Adapters can only be added to a single plugin");
            }
            else
            {
                RemoveAdsAdapter();
                adsAdapter.plugin = this;
                adsAdapter.SetIsAdsAdapter(true);
                this.adsAdapter = adsAdapter;
                adsAdapter.On(Event.START, AdStartListener);
                adsAdapter.On(Event.JOIN, AdJoinListener);
                adsAdapter.On(Event.PAUSE, AdPauseListener);
                adsAdapter.On(Event.RESUME, AdResumeListener);
                adsAdapter.On(Event.BUFFER_BEGIN, AdBufferBeginListener);
                adsAdapter.On(Event.BUFFER_END, AdBufferEndListener);
                adsAdapter.On(Event.STOP, AdStopListener);
                adsAdapter.On(Event.ERROR, AdErrorListener);
                adsAdapter.On(Event.CLICK, AdClickListener);
                adsAdapter.On(Event.MANIFEST, AdManifestListener);
                adsAdapter.On(Event.PODSTART, AdBreakStartListener);
                adsAdapter.On(Event.PODSTOP, AdBreakStopListener);
                adsAdapter.On(Event.QUARTILE, AdQuartileListener);
            }
        }

        /**
        * Removes the current adapter. Fires stop if needed. Calls adapter.dispose()
        *
        * @memberof youbora.Plugin.prototype
        */
        public void RemoveAdsAdapter()
        {
            if (adsAdapter != null)
            {
                adsAdapter.Dispose();
                adsAdapter.plugin = null;
                adsAdapter.Off(Event.START, AdStartListener);
                adsAdapter.Off(Event.JOIN, AdJoinListener);
                adsAdapter.Off(Event.PAUSE, AdPauseListener);
                adsAdapter.Off(Event.RESUME, AdResumeListener);
                adsAdapter.Off(Event.BUFFER_BEGIN, AdBufferBeginListener);
                adsAdapter.Off(Event.BUFFER_END, AdBufferEndListener);
                adsAdapter.Off(Event.STOP, AdStopListener);
                adsAdapter.Off(Event.ERROR, AdErrorListener);
                adsAdapter.Off(Event.CLICK, AdClickListener);
                adsAdapter.Off(Event.MANIFEST, AdManifestListener);
                adsAdapter.Off(Event.PODSTART, AdBreakStartListener);
                adsAdapter.Off(Event.PODSTOP, AdBreakStopListener);
                adsAdapter.Off(Event.QUARTILE, AdQuartileListener);

                adsAdapter = null;
            }
        }

  // ---------------------------------------- LISTENERS -----------------------------------------
        private void AdStartListener(object e)
        {
            if (adapter != null)
            {
                adapter.FireBufferEnd();
                adapter.FireSeekEnd();
                if (isInitiated && !adapter.flags.isStarted) adapter.FireStart();
                if (adapter.flags.isPaused) adapter.chronos.pause.Reset();
            }
            else
            {
                FireInit();
            }
            if (adsAdapter != null)
            {
                adsAdapter.FireManifest();
                adsAdapter.FireBreakStart();
                adsAdapter.chronos.viewedMax = new List<Chrono>();
            }

            Dictionary<string, object> parameters = (Dictionary<string, object>)e;

            parameters["adNumber"] = requestBuilder.GetNewAdNumber();
            if ((bool)options["ad.ignore"] == false)
            {
                bool allParamsReady = ((GetAdTitle() != null || GetAdResource() != null) && GetAdDuration() != null);
                if (allParamsReady)
                {
                    adStartSent = true;
                    Send(WillSendEvent.WILL_SEND_AD_START, Service.AD_START, parameters);
                    Log.Notice("{0} {1}{2} at {3}s", Service.AD_START, parameters["position"], parameters["adNumber"], parameters["playhead"]);
                }
                else
                {
                    adInitSent = true;
                    Send(WillSendEvent.WILL_SEND_AD_INIT, Service.AD_INIT, parameters);
                    Log.Notice("{0} {1}{2} at {3}s", Service.AD_INIT, parameters["position"], parameters["adNumber"], parameters["playhead"]);
                }
            }
        }

        private void AdJoinListener( object e)
        {
            Dictionary<string, object> parameters = (Dictionary<string, object>)e;
            if (adInitSent && !adStartSent && (bool) options["ad.ignore"] == false)
            {
                Dictionary<string, object> parameters2 = new Dictionary<string, object>();
                Send(WillSendEvent.WILL_SEND_AD_START, Service.AD_START, parameters2);
                Log.Notice("{0} {1}{2} at {3}s", Service.AD_START, parameters2["position"], parameters2["adNumber"], parameters2["playhead"]);
            }
            adsAdapter.StartChronoView();

            if (adConnected)
            {
                adsAdapter.chronos.join.StartTime = adConnectedTime;
                adsAdapter.chronos.total.StartTime = adConnectedTime;
                adConnectedTime = null;
                adConnected = false;
            }
            if ((bool)options["ad.ignore"] == false)
            {
                Send(WillSendEvent.WILL_SEND_AD_JOIN, Service.AD_JOIN, parameters);
            }
            Log.Notice("{0} {1}ms", Service.AD_JOIN, parameters["adJoinDuration"]);
            adInitSent = false;
            adStartSent = false;
        }

        private void AdPauseListener( object e)
        {
            Dictionary<string, object> parameters = (Dictionary<string, object>)e;
            adsAdapter.StopChronoView();
            if ((bool)options["ad.ignore"] == false)
            {
                Send(WillSendEvent.WILL_SEND_AD_PAUSE, Service.AD_PAUSE, parameters);
            }
            Log.Notice("{0} at {1}s", Service.AD_PAUSE, parameters["adPlayhead"]);
        }

        private void AdResumeListener(object e)
        {
            Dictionary<string, object> parameters = (Dictionary<string, object>)e;
            adsAdapter.StartChronoView();
            if ((bool)options["ad.ignore"] == false)
            {
                Send(WillSendEvent.WILL_SEND_AD_RESUME, Service.AD_RESUME, parameters);
            }
            Log.Notice("{0} {1}ms",Service.AD_RESUME, parameters["adPauseDuration"]);
        }

        private void AdBufferBeginListener(object e)
        {
            adsAdapter.StopChronoView();
            Log.Notice("Ad Buffer Begin");
            if (adsAdapter.flags.isPaused)
            {
                adsAdapter.chronos.pause.Reset();
            }
        }

        private void AdBufferEndListener(object e)
        {
            Dictionary<string, object> parameters = (Dictionary<string, object>)e;
            adsAdapter.StartChronoView();
            if ((bool)options["ad.ignore"] == false)
            {
                Send(WillSendEvent.WILL_SEND_AD_BUFFER, Service.AD_BUFFER, parameters);
            }
            Log.Notice("{0} {1}ms", Service.AD_BUFFER, parameters["adBufferDuration"]);
        }

        private void AdStopListener(object e)
        {
            adsAdapter.StopChronoView();
            adsAdapter.flags.Reset();
            totalPrerollsTime += adsAdapter.chronos.total.GetDeltaTime();
            Dictionary<string, object> parameters = (Dictionary<string, object>)e;
            if ((string)requestBuilder.lastSent["position"] == AdPosition.Postroll)
            {
                playedPostrolls++;
            }
            if ((bool)options["ad.ignore"] == false)
            {
                Send(WillSendEvent.WILL_SEND_AD_STOP, Service.AD_STOP, parameters);
            }
            Log.Notice("{0} {1}ms", Service.AD_STOP, parameters["adTotalDuration"]);

            Dictionary<string, List<int>> expPatt = (Dictionary<string, List<int>>) options["ad.expectedPattern"];
            if (expPatt != null && expPatt.ContainsKey("post") && expPatt["post"][0] == this.playedPostrolls &&
                (string) parameters["position"] == AdPosition.Postroll)
            {
                FireStop();
            }

            adConnected = true;
            adConnectedTime = DateTime.Now;
        }

        private void AdErrorListener(object e)
        {
            Dictionary<string, object> parameters = (Dictionary<string, object>)e;
            if (adapter != null && !adapter.flags.isStarted && !isInitiated)
            {
                savedAdError = parameters;
                return; // Ignore ad errors before content
            }
            if (BlockAdError(parameters)) return;
            if (adsAdapter != null)
            {
                adsAdapter.FireManifest();
                adsAdapter.FireBreakStart();
            }
            if (adsAdapter == null || !adsAdapter.flags.isStarted)
            {
                parameters["adNumber"] = requestBuilder.GetNewAdNumber();
            }

            if ((bool)options["ad.ignore"] == false)
            {
                Send(WillSendEvent.WILL_SEND_AD_ERROR, Service.AD_ERROR, parameters);
            }
            Log.Notice(Service.AD_ERROR);
        }

        private void AdSavedError()
        {
            if (savedAdError != null)
            {
                AdErrorListener(savedAdError);
                savedAdError = null;
            }
        }

        private void AdSavedManifest()
        {
            if (savedAdManifest != null)
            {
                AdManifestListener(savedAdManifest);
                savedAdManifest = null;
            }
        }

        private bool BlockAdError( Dictionary<string,object> errorParams)
        {
            DateTime now = DateTime.Now;

            bool sameError = false;
            if (lastAdErrorParams != null) {
                sameError = true; 
                if (lastAdErrorParams.ContainsKey("errorCode"))
                {
                    sameError = sameError && errorParams.ContainsKey("errorCode") && lastAdErrorParams["errorCode"] == errorParams["errorCode"];
                }
                else
                {
                    if (errorParams.ContainsKey("errorCode"))
                    {
                        sameError = false;
                    }
                }
                if (lastAdErrorParams.ContainsKey("msg"))
                {
                    sameError = sameError && errorParams.ContainsKey("msg") && lastAdErrorParams["msg"] == errorParams["msg"];
                }
                else
                {
                    if (errorParams.ContainsKey("msg"))
                    {
                        sameError = false;
                    }
                }
            }
            if (sameError && DateTime.Compare(lastAdErrorTime,now.AddSeconds(-5)) > 0)
            {
                lastAdErrorTime = DateTime.Now;
                return true;
            }
            lastAdErrorTime = DateTime.Now;
            lastAdErrorParams = errorParams;
            return false;
        }

        private void AdClickListener(object e)
        {
            Dictionary<string, object> parameters = (Dictionary<string, object>)e;
            Send(WillSendEvent.WILL_SEND_AD_CLICK, Service.AD_CLICK, parameters);
            Log.Notice(Service.AD_CLICK);
        }

        private void AdManifestListener(object e)
        {
            if (!isAdsManifestSent)
            {
                if (adapter != null && !adapter.flags.isStarted && !this.isInitiated)
                {
                    savedAdManifest = e;
                    return; // Ignore ad manifest before content
                }
                Dictionary<string, object> parameters = (Dictionary<string, object>)e;
                isAdsManifestSent = true;
                if ((bool)this.options["ad.ignore"] == false)
                {
                    Send(WillSendEvent.WILL_SEND_AD_MANIFEST, Service.AD_MANIFEST, parameters);
                }
                Log.Notice(Service.AD_MANIFEST);
            }
        }

        private void AdBreakStartListener (object e)
        {
            Dictionary<string, object> parameters = (Dictionary<string, object>)e;

            requestBuilder.lastSent["breakNumber"] = requestBuilder.GetNewBreakNumber();

            if (adapter != null) adapter.FirePause();

            if (!(bool)options["ad.ignore"]) {
                Send(WillSendEvent.WILL_SEND_AD_POD_START, Service.AD_POD_START, parameters);
            }
            Log.Notice(Service.AD_POD_START);
            adConnected = false;
        }

        private void AdBreakStopListener(object e)
        {
            Dictionary<string, object> parameters = (Dictionary<string, object>)e;
            parameters["position"] = requestBuilder.lastSent["position"];
            if (!(bool)options["ad.ignore"])
            {
                Send(WillSendEvent.WILL_SEND_AD_POD_STOP, Service.AD_POD_STOP, parameters);
            }
            Log.Notice(Service.AD_POD_STOP);
            adConnected = false;
        }

        private void AdQuartileListener(object e)
        {
            Dictionary<string, object> parameters = (Dictionary<string, object>)e;

            if (parameters.ContainsKey("quartile"))
            {
                if (!(bool)options["ad.ignore"])
                {
                    Send(WillSendEvent.WILL_SEND_AD_QUARTILE, Service.AD_QUARTILE, parameters);
                }
                Log.Notice(Service.AD_QUARTILE);
            }
        }

    }
}
