﻿using System.Collections.Generic;
using Youbora.emitter;
using Youbora.constants;
using Youbora.log;

namespace Youbora.plugin
{
    public partial class Plugin : Emitter
    {
        private void StartPings()
        {
            if (!ping.IsRunning) ping.Start();
        }

        private void StopPings()
        {
            ping.Stop();
        }

        private void SendPing(object dt)
        {
            double diffTime = (double)dt;
            Dictionary<string,object> pingParams = new Dictionary<string, object>();
            pingParams.Add("diffTime", diffTime);
            Dictionary<string,object> entities = requestBuilder.GetChangedEntities();
            if (entities.Count != 0)
            {
                pingParams.Add("entities", entities);
            }

            if(adapter != null)
            {
                if (adapter.flags.isPaused)
                {
                    pingParams = requestBuilder.FetchParams(pingParams,new string[] {"pauseDuration"});
                }
                else
                {
                    pingParams = requestBuilder.FetchParams(pingParams, new string[] { "bitrate", "throughput", "fps"});
                }
                if (adapter.flags.isJoined)
                {
                    pingParams = requestBuilder.FetchParams(pingParams, new string[] { "playhead"});
                }
                if (adapter.flags.isBuffering)
                {
                    pingParams = requestBuilder.FetchParams(pingParams, new string[] { "bufferDuration"});
                }
                if (adapter.flags.isSeeking)
                {
                    pingParams = requestBuilder.FetchParams(pingParams, new string[] { "seekDuration"});
                }

                if (adsAdapter != null)
                {
                    if (adsAdapter.flags.isStarted)
                    {
                        pingParams = requestBuilder.FetchParams(pingParams, new string[] { "adPlayhead"});
                        if (adsAdapter.flags.isPaused)
                        {
                            pingParams = requestBuilder.FetchParams(pingParams, new string[] { "adPauseDuration"});
                        }
                        else
                        {
                            pingParams = requestBuilder.FetchParams(pingParams, new string[] { "adBitrate"});
                        }
                    }
                    if (adsAdapter.flags.isBuffering)
                    {
                        pingParams = requestBuilder.FetchParams(pingParams, new string[] { "adBufferDuration"});
                    }
                }
            }

            Send(WillSendEvent.WILL_SEND_PING, Service.PING, pingParams);
            if (startDelayed)
            {
                RetryStart();
            }
            Log.Verbose(Service.PING);
        }
    }
}