﻿using System;
using System.Collections.Generic;
using Youbora.emitter;
using Youbora.log;
using Youbora.version;
using Youbora.constants;
using Youbora.plugin.options;

namespace Youbora.plugin
{
    public partial class Plugin : Emitter
    {
        /**
         * Returns content"s playhead
         *
         * @memberof youbora.Plugin.prototype
         */
        public double GetPlayhead()
        {
            double ret = 0;
          if (adapter != null)
            {
                try
                {
                    ret = Math.Round(adapter.GetPlayhead());
                }
                catch (Exception err)
                {
                    Log.Warn("An error occurred while calling GetPlayhead{0}", err.ToString());
              }
            }
            return ret;
        }

        /**
        * Returns content"s PlayRate
        *
        * @memberof youbora.Plugin.prototype
        */
        public double? GetPlayrate()
        {
            double ret = 0;
            if (adapter != null)
            {
                try
                {
                    ret = adapter.GetPlayrate();
                }
                catch (Exception err)
                {
                    Log.Warn("An error occured while calling GetPlayrate{0}", err.ToString());
              }
            }
            return ret;
        } 
        
          /**
           * Returns content"s FramesPerSecond
           *
           * @memberof youbora.Plugin.prototype
           */
         public double? GetFramesPerSecond()
         {
            double? ret = (double?) options["content.fps"];
            if (ret == null && adapter != null)
            {
                try
                {
                    ret = adapter.GetFramesPerSecond();
                }
                catch (Exception err)
                {
                    Log.Warn("An error occured while calling GetFramesPerSecond{0}", err.ToString());
              }
            }
            return ret;
         }

          /**
           * Returns content"s DroppedFrames
           *
           * @memberof youbora.Plugin.prototype
           */
          public double? GetDroppedFrames()
          {
            double? ret = null;
            if (adapter != null)
            {
                try
                {
                    ret = adapter.GetDroppedFrames();
                }
                catch (Exception err)
                {
                    Log.Warn("An error occured while calling GetDroppedFrames{0}", err.ToString());
              }
            }

            return ret;
          }

        /**
        * Returns content"s Duration
        *
        * @memberof youbora.Plugin.prototype
        */
        public double? GetDuration()
        {
            double? ret = (double?)options["content.duration"];
            if (ret == null && adapter != null)
            {
                try
                {
                    ret = adapter.GetDuration();
                }
                catch (Exception err)
                {
                    Log.Warn("An error occured while calling GetDuration{0}", err.ToString());
              }
            }
            if (ret == 0) ret = null;
            return ret;
        }

          /**
           * Returns content"s Bitrate
           *
           * @memberof youbora.Plugin.prototype
           */
          public double? GetBitrate()
          {
            double? ret = null;
            try
            {
                 ret = Convert.ToDouble((Int32?)options["content.bitrate"]);
            }
            catch (Exception err)
            {
                try
                {
                    ret = Convert.ToDouble((Int64?)options["content.bitrate"]);
                }
                catch (Exception err2)
                {
                    try
                    {
                        ret = Convert.ToDouble(options["content.bitrate"]);
                    }
                    catch (Exception err3)
                    {
                        Log.Warn("The content.bitrate type is not an int. {0}", err.ToString() + err2.ToString() + err3.ToString());
                    }
                }
            }

            if ((ret == null || ret == 0) && adapter != null)
            {
                try
                {
                    ret = adapter.GetBitrate();
                }
                catch (Exception err)
                {
                    Log.Warn("An error occured while calling GetBitrate{0}", err.ToString());
              }

            }
            return ret;
          }


        /**
         * Returns content"s TotalBytes
         *
         * @memberof youbora.Plugin.prototype
         */
        public double? GetTotalBytes()
        {
            if (!(bool)options["content.sendTotalBytes"])
            {
                return null;
            }
            double? ret = null;
            try
            {
                ret = Convert.ToDouble((Int32?)options["content.totalBytes"]);
            }
            catch (Exception err)
            {
                try
                {
                    ret = Convert.ToDouble((Int64?)options["content.totalBytes"]);
                }
                catch (Exception err2)
                {
                    try
                    {
                        ret = Convert.ToDouble(options["content.totalBytes"]);
                    }
                    catch (Exception err3)
                    {
                        Log.Warn("The content.totalBytes type is not a valid number type. {0}", err.ToString() + err2.ToString() + err3.ToString());
                    }
                }
            }

            if ((ret == null || ret == 0) && adapter != null)
            {
                try
                {
                    ret = adapter.GetTotalBytes();
                }
                catch (Exception err)
                {
                    Log.Warn("An error occured while calling GetTotalBytes{0}", err.ToString());
                }

            }
            return ret;
        }

        /**
        * Returns content"s Throughput
        *
        * @memberof youbora.Plugin.prototype
        */
        public double? GetThroughput()
        {
            double? ret = (double?)options["content.throughput"];
            if (ret == null && adapter != null)
            {
                try
                {
                    ret = adapter.GetThroughput();
                }
                catch (Exception err)
                {
                    Log.Warn("An error occured while calling GetThroughput{0}", err.ToString());
              }
            }
            return ret;
        }

        /**
        * Returns content"s Rendition
        *
        * @memberof youbora.Plugin.prototype
        */
        public string GetRendition()
        {
            string ret = (string)options["content.rendition"];
            if (ret == null && adapter != null)
            {
                try
                {
                    ret = adapter.GetRendition();
                }
                catch (Exception err)
                {
                    Log.Warn("An error occured while calling GetRendition{0}", err.ToString());
              }
            }
            return ret;
        }

          /**
           * Returns content"s Title
           *
           * @memberof youbora.Plugin.prototype
           */
          public string GetTitle()
          {
            string ret = (string)options["content.title"];
            if (ret == null && adapter != null)
            {
                try
                {
                    ret = adapter.GetTitle();
                }
                catch (Exception err)
                {
                    Log.Warn("An error occured while calling GetTitle{0}", err.ToString());
              }
            }
            return ret;
          }

        /**
        * Returns content"s Title2
        *
        * @memberof youbora.Plugin.prototype
        */
        public string GetTitle2()
        {
            string ret = (string)options["content.program"];
            if (ret == null && adapter != null)
            {
                try
                {
                    ret = adapter.GetTitle2();
                }
                catch (Exception err)
                {
                    Log.Warn("An error occured while calling GetTitle2{0}", err.ToString());
              }
            }
            return ret;
        }

        /**
        * Returns content"s IsLive
        *
        * @memberof youbora.Plugin.prototype
        */
        public bool? GetIsLive()
        {
            bool? ret = (bool?)options["content.isLive"];
            if (ret == null && adapter != null)
            {
                try
                {
                    ret = adapter.GetIsLive();
                }
                catch (Exception err)
                {
                    Log.Warn("An error occured while calling GetIsLive{0}", err.ToString());
              }
            }
            if (ret == null) ret = false;
            return ret;
        }

        /**
        * Returns content"s Resource after being parsed by the resourceTransform
        *
        * @memberof youbora.Plugin.prototype
        */
        public string GetResource()
        {
            string ret = (string)options["content.resource"];
            if (ret == null && adapter != null)
            {
                try
                {
                    ret = adapter.GetResource();
                }
                catch (Exception err)
                {
                    Log.Warn("An error occured while calling GetResource{0}", err.ToString());
                    ret = null;
              }
            }
            return ret;
        }

        public string GetParsedResource()
        {
            string ret = null;
            if(!resourceTransform.IsBlocking())
            {
                ret = resourceTransform.GetResource();
            }
            if (ret == null)
            {
                ret = adapter.GetURLToParse();
            }
            if (ret != null && ret.Equals(GetResource()))
            {
                ret = null;
            }
            return ret;
        }

        /**
        * Returns content"s TransactionCode
        *
        * @memberof youbora.Plugin.prototype
        */
        public string GetTransactionCode()
        {
            return (string) options["content.transactionCode"];
        }

        /**
        * Returns content"s Metadata
        *
        * @memberof youbora.Plugin.prototype
        */
        public string GetMetadata()
        {
            return (string) options["content.metadata"];
        }

        /**
        * Returns content"s PlayerVersion
        *
        * @memberof youbora.Plugin.prototype
        */
        public string GetPlayerVersion()
        {
            string ret = "";
            if (adapter != null)
            {
                try
                {
                    ret = adapter.GetPlayerVersion();
                }
                catch (Exception err)
                {
                    Log.Warn("An error occured while calling GetPlayerVersion{0}", err.ToString());
              }
            }
            return ret;
        }

        /**
        * Returns content"s PlayerName
        *
        * @memberof youbora.Plugin.prototype
        */
        public string GetPlayerName()
        {
            string ret = "";
            if (adapter != null)
            {
                try
                {
                    ret = adapter.GetPlayerName();
                }
                catch (Exception err)
                {
                    Log.Warn("An error occured while calling GetPlayerName{0}", err.ToString());
              }
            }
            return ret;
        }

        /**
        * Returns content"s Cdn
        *
        * @memberof youbora.Plugin.prototype
        */
        public string GetCdn()
        {
            string ret = null;
            if (!resourceTransform.IsBlocking())
            {
                ret = resourceTransform.GetCdnName();
            }
            if (ret == null) ret = (string) options["content.cdn"];
            return ret;
        }

        /**
        * Returns content"s PluginVersion
        *
        * @memberof youbora.Plugin.prototype
        */
        public string GetPluginVersion()
        {
            string ret = GetAdapterVersion();
            if (ret == null) ret = LibVersion.VERSION + "-adapterless-csharp";

            return ret;
        }

        /**
        * Returns content"s LibVersion
        *
        * @memberof youbora.Plugin.prototype
        */
        public string GetLibVersion()
        {
            return LibVersion.VERSION;
        }

        /**
        * Returns ads adapter GetVersion or null
        *
        * @memberof youbora.Plugin.prototype
        */
        public string GetAdapterVersion()
        {
            string ret = null;
            if (adapter != null)
            {
                try
                {
                    ret = adapter.GetVersion();
                }
                catch (Exception err)
                {
                    Log.Warn("An error occured while calling GetPluginVersion{0}", err.ToString());
              }
            }
            return ret;
        }

        /**
        * Returns cdn traffic received in bytes or null
        *
        * @memberof youbora.Plugin.prototype
        */
        public double? GetCdnTraffic()
        {
            double? ret = null;
            if (adapter != null)
            {
                try
                {
                    ret = adapter.GetCdnTraffic();
                    if (ret == -1) ret = null;
                }
                catch (Exception err)
                {
                    Log.Warn("An error occured while calling GetCdnTraffic{0}", err.ToString());
              }
            }
            return ret;
        }

        /**
        * Returns p2p traffic received in bytes or null
        *
        * @memberof youbora.Plugin.prototype
        */
        public double? GetP2PTraffic()
        {
            double? ret = null;
            if (adapter != null)
            {
                try
                {
                    ret = adapter.GetP2PTraffic();
                    if (ret == -1) ret = null;
                }
                catch (Exception err)
                {
                    Log.Warn("An error occured while calling GetP2PTraffic{0}", err.ToString());
              }
            }
            return ret;
        }

        /**
        * Returns p2p traffic sent in bytes or null
        *
        * @memberof youbora.Plugin.prototype
        */
        public double? GetUploadTraffic()
        {
            double? ret = null;
            if (adapter != null)
            {
                try
                {
                    ret = adapter.GetUploadTraffic();
                    if (ret == -1) ret = null;
                }
                catch (Exception err)
                {
                    Log.Warn("An error occured while calling GetUploadTraffic{0}", err.ToString());
              }
            }
            return ret;
        }

        /**
        * Returns if p2p plugin is enabled or null
        *
        * @memberof youbora.Plugin.prototype
        */
        public bool? GetIsP2PEnabled()
        {
            bool? ret = null;
            if (adapter != null)
            {
                try
                {
                    ret = adapter.GetIsP2PEnabled();
                }
                catch (Exception err)
                {
                    Log.Warn("An error occured while calling GetIsP2PEnabled{0}", err.ToString());
              }
            }
            return ret;
        }


        public string GetStreamingProtocol()
        {
            var ret = (string)options["content.streamingProtocol"];
            if (StreamingProtocol.DASH != ret && 
                StreamingProtocol.HDS != ret &&
                StreamingProtocol.HLS != ret &&
                StreamingProtocol.MSS != ret &&
                StreamingProtocol.RTMP != ret &&
                StreamingProtocol.RTP != ret &&
                StreamingProtocol.RTSP != ret)
            {
                ret = null;
            }
            return ret;
        }

        public string GetTransportFormat()
        {
            string ret = (string)options["content.transportFormat"];
            if (ret == null && (bool)options["parse.manifest"] && !resourceTransform.IsBlocking())
            {
                ret = resourceTransform.GetTransportFormat();
            }
            // if ret is not in the list of possible options, ret = null
            if (TransportFormat.MP4 != ret && TransportFormat.MPEG2 != ret && TransportFormat.CMF != ret)
            {
                ret = null;
            }
            return ret;
        }

        /**Returns household id*/
        public string GetHouseholdId()
        {
            string ret = null;
            if (adapter != null)
            {
                try
                {
                    ret = adapter.GetHouseholdId();
                }
                catch (Exception err)
                {
                    Log.Warn("An error occurred while calling GetHouseholdId{0}", err.ToString());
              }
            }
            return ret;
        }

        /**
        * Returns latency of a live video, or null
        *
        * @memberof youbora.Plugin.prototype
        */
        public double? GetLatency()
        {
            double? ret = null;
            if (adapter != null)
            {
                try
                {
                    ret = adapter.GetLatency();
                    if (ret == -1 || GetIsLive() != true) ret = null;
                }
                catch (Exception err)
                {
                    Log.Warn("An error occurred while calling GetLatency{0}", err.ToString());
              }
            }
            return ret;
        }

        /**
        * Returns the amount of packets lost, or null
        *
        * @memberof youbora.Plugin.prototype
        */
        public double? GetPacketLoss()
        {
            double? ret = null;
            if (adapter != null)
            {
                try
                {
                    ret = adapter.GetPacketLoss();
                    if (ret == -1) ret = null;
                }
                catch (Exception err)
                {
                    Log.Warn("An error occurred while calling GetPacketLoss{0}", err.ToString());
              }
            }
            return ret;
        }

        /**
        * Returns the amount of packets sent, or null
        *
        * @memberof youbora.Plugin.prototype
        */
        public double? GetPacketSent()
        {
            double? ret = null;
            if (adapter != null)
            {
                try
                {
                    ret = adapter.GetPacketSent();
                    if (ret == -1) ret = null;
                }
                catch (Exception err)
                {
                    Log.Warn("An error occurred while calling GetPacketSent{0}", err.ToString());
              }
            }
            return ret;
        }
  // ----------------------------------------- CHRONOS ------------------------------------------

        /**
        * Returns JoinDuration chrono delta time
        *
        * @memberof youbora.Plugin.prototype
        */
        public double GetJoinDuration()
        {
            return (adapter != null) ? adapter.chronos.join.GetDeltaTime(false) : -1;
        }

        /**
        * Returns BufferDuration chrono delta time
        *
        * @memberof youbora.Plugin.prototype
        */
        public double GetBufferDuration()
        {
            return (adapter != null) ? adapter.chronos.buffer.GetDeltaTime(false) : -1;
        }

        /**
        * Returns SeekDuration chrono delta time
        *
        * @memberof youbora.Plugin.prototype
        */
        public double GetSeekDuration()
        {
            return (adapter != null) ? adapter.chronos.seek.GetDeltaTime(false) : -1;
        }

        /**
        * Returns pauseDuration chrono delta time
        *
        * @memberof youbora.Plugin.prototype
        */
        public double GetPauseDuration()
        {
            if (adapter != null)
            {
                if (adapter.flags.isPaused)
                {
                    return adapter.chronos.pause.GetDeltaTime(false);
                }
            }
            return -1;
        }

          /**
           * Returns content's saga
           *
           * @memberof youbora.Plugin.prototype
           */
         public string GetSaga()
         {
            return (string)options["content.saga"];
         }

          /**
         * Returns content's tv show
         *
         * @memberof youbora.Plugin.prototype
         */
        public string GetTvShow()
        {
            return (string)options["content.tvShow"];
        }

          /**
         * Returns content's season
         *
         * @memberof youbora.Plugin.prototype
         */
         public string GetSeason()
         {
            return (string)options["content.season"];
         }

        /**
        * Returns content's episode title
        *
        * @memberof youbora.Plugin.prototype
        */
        public string GetEpisodeTitle()
        {
            return (string)options["content.episodeTitle"];
        }

        /**
        * Returns content's channel
        *
        * @memberof youbora.Plugin.prototype
        */
         public string GetChannel()
         {
            return (string)options["content.channel"];
         }

        /**
        * Returns content's id
        *
        * @memberof youbora.Plugin.prototype
        */
        public string GetID()
        {
            return (string)options["content.id"];
        }

        /**
        * Returns content's IMDB id
        *
        * @memberof youbora.Plugin.prototype
        */
        public string GetImdbId()
        {
            return (string)options["content.imdbId"];
        }

        /**
        * Returns content's gracenote id
        *
        * @memberof youbora.Plugin.prototype
        */
        public string GetGracenoteID()
        {
            return (string)options["content.gracenoteId"];
        }

          /**
        * Returns content's type
        *
        * @memberof youbora.Plugin.prototype
        */
        public string GetContentType()
        {
            return (string)options["content.type"];
        }

        /**
        * Returns content's genre
        *
        * @memberof youbora.Plugin.prototype
        */
         public string GetGenre()
         {
            return (string)options["content.genre"];
         }

        /**
        * Returns content's language
        *
        * @memberof youbora.Plugin.prototype
        */
        public string GetVideoLanguage()
        {
            return (string)options["content.language"];
        }

        /**
        * Returns content's subtitles
        *
        * @memberof youbora.Plugin.prototype
        */
        public string GetSubtitles()
        {
            return (string)options["content.subtitles"];
        }

        /**
        * Returns content's contracted resolution
        *
        * @memberof youbora.Plugin.prototype
        */
        public string GetContractedResolution()
        {
            return (string)options["content.contractedResolution"];
        }

        /**
      * Returns content's cost
      *
      * @memberof youbora.Plugin.prototype
*/
        public string GetCost()
        {
            return (string)options["content.cost"];
        }

        /**
      * Returns content's price
      *
      * @memberof youbora.Plugin.prototype
*/
        public string GetPrice()
        {
            return (string)options["content.price"];
        }

        /**
      * Returns content's playback type
      *
      * @memberof youbora.Plugin.prototype
*/
        public string GetPlaybackType()
        {
            if ((string)options["content.playbackType"] != null)
            {
                return (string)options["content.playbackType"];
            }
            if (this.GetIsLive() == true)
            {
                return "Live";
            }
            return "VoD";
        }

        /**
      * Returns content's DRM
      *
      * @memberof youbora.Plugin.prototype
*/
        public string GetDRM()
        {
            return (string)options["content.drm"];
        }

        /**
      * Returns content's video codec
      *
      * @memberof youbora.Plugin.prototype
*/
        public string GetVideoCodec()
        {
            string ret = (string)options["content.encoding.videoCodec"];
            if (ret == null && adapter != null)
            {
                try
                {
                    ret = adapter.GetVideoCodec();
                }
                catch (Exception err)
                {
                    Log.Warn("An error occured while calling GetVideoCodec{0}", err.ToString());
                    ret = null;
                }
            }
            return ret;
        }

        /**
      * Returns content's audio codec
      *
      * @memberof youbora.Plugin.prototype
*/
        public string GetAudioCodec()
        {
            string ret = (string)options["content.encoding.audioCodec"];
            if (ret == null && adapter != null)
            {
                try
                {
                    ret = adapter.GetAudioCodec();
                }
                catch (Exception err)
                {
                    Log.Warn("An error occured while calling GetAudioCodec{0}", err.ToString());
                    ret = null;
                }
            }
            return ret;
        }

        /**
      * Returns content's codec settings
      *
      * @memberof youbora.Plugin.prototype
*/
        public string GetCodecSettings()
        {
            return (string)options["content.encoding.codecSettings"];
        }

        /**
      * Returns content's codec profile
      *
      * @memberof youbora.Plugin.prototype
        */
        public string GetCodecProfile()
        {
            return (string)options["content.encoding.codecProfile"];
        }

        /**
      * Returns content's container format
      *
      * @memberof youbora.Plugin.prototype
        */
        public string GetContainerFormat()
        {
            return (string)options["content.encoding.containerFormat"];
        }

        /**
      * Returns linked view id string
      *
      * @memberof youbora.Plugin.prototype
      */
        public string GetLinkedViewID()
        {
            return (string)options["linkedViewID"];
        }

        public Dictionary<string, object> GetVideoMetrics()
        {
            Dictionary<string, object> ret = (Dictionary<string, object>)options["content.metrics"];
            if (options["content.metrics"] == null && adapter != null)
            {
                try
                {
                    ret = adapter.GetMetrics();
                }
                catch (Exception err)
                {
                    Log.Warn("An error occurred while calling GetMetrigs{0}", err.ToString());
                }
            }
            return ret;
        }
    }
}
