﻿using System.Collections.Generic;
using Youbora.emitter;
using Youbora.constants;
using Youbora.log;
using Youbora.comm.request;
using Youbora.infinity;

namespace Youbora.plugin
{
    public partial class Plugin : Emitter
    {
        public string lastServiceSent = null;

        private void InitInfinity()
        {
            infinity.On(Infinity.Event.NAV,NavListener);
            infinity.On(Infinity.Event.SESSION_START, SessionStartListener);
            infinity.On(Infinity.Event.SESSION_STOP, SessionStopListener);
            infinity.On(Infinity.Event.EVENT, EventListener);
        }

        private void SendInfinity(string willSendEvent, string service, Dictionary<string, object> paramList)
        {
            paramList = requestBuilder.BuildParams(paramList, service);
            Dictionary<string, object> data = new Dictionary<string, object>() {
                ["plugin"] = this,
                ["adapter"] = GetAdapter(),
                ["adsAdapter"] = GetAdsAdapter()
            };

            Emit(willSendEvent, data);

            if (infinity.GetComm() != null && paramList != null && (bool)options["enabled"])
            {
                lastServiceSent = service;
                infinity.GetComm().SendRequest(new YBRequest(GetHost(), service, paramList), null, null);
            }
        }


        private void NavListener(object e)
        {
            Dictionary<string, object> input = (Dictionary<string, object>)e;
            Dictionary<string, object> paramList = new Dictionary<string, object>();
            if (input.ContainsKey("params"))
            {
                paramList = (Dictionary<string, object>)input["params"];
            }
            if (input.ContainsKey("page"))
            {
                paramList["page"] = input["page"];
            }
            if (input.ContainsKey("route"))
            {
                paramList["route"] = input["route"];
            }
            SendInfinity(WillSendEvent.WILL_SEND_NAV, Service.NAV, paramList);
            if (!beat.IsRunning)
            {
                beat.Start();
            }
            Log.Notice(Service.NAV);
        }

        private void SessionStartListener(object e)
        {
            Dictionary<string, object> input = (Dictionary<string, object>)e;
            Dictionary<string, object> paramList = new Dictionary<string, object>();
            if (input.ContainsKey("params"))
            {
                paramList = (Dictionary<string, object>)input["params"];
            }
            if (input.ContainsKey("page"))
            {
                paramList["page"] = input["page"];
            }
            if (input.ContainsKey("route"))
            {
                paramList["route"] = input["route"];
            }
            SendInfinity(WillSendEvent.WILL_SEND_SESSION_START, Service.SESSION_START, paramList);
            if (!beat.IsRunning)
            {
                beat.Start();
            }
            Log.Notice(Service.SESSION_START);
        }

        private void SessionStopListener(object e)
        {
            Dictionary<string, object> input = (Dictionary<string, object>)e;
            Dictionary<string, object> paramList = null;
            if (input.ContainsKey("params"))
            {
                paramList = (Dictionary<string, object>)input["params"];
            }
            SendInfinity(WillSendEvent.WILL_SEND_SESSION_STOP, Service.SESSION_STOP, paramList);
            if (beat.IsRunning)
            {
                beat.Stop();
            }
            Log.Notice(Service.SESSION_STOP);
        }

        private void EventListener(object e)
        {
            Dictionary<string, object> input = (Dictionary<string, object>)e;
            Dictionary<string, object> paramList = null;
            if (input.ContainsKey("params"))
            {
                paramList = (Dictionary<string, object>)input["params"];
            }
            SendInfinity(WillSendEvent.WILL_SEND_EVENT, Service.EVENT, paramList);
            Log.Notice(Service.EVENT + " " + (string)paramList["name"]);
        }

        public void SendBeat(object df)
        {
            double diffTime = (double) df;
            Dictionary<string, object> paramList = new Dictionary<string, object>() {
                { "diffTime", diffTime}
            };
            SendInfinity(WillSendEvent.WILL_SEND_BEAT, Service.BEAT, paramList);
            Log.Verbose(Service.BEAT);
        }

        public Dictionary<string,object> GetSessionMetrics()
        {
            return (Dictionary<string, object>)options["session.metrics"];
        }
    }
}
