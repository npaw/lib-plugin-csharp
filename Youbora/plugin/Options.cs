﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Youbora.log;
using Youbora.plugin.deprecatedOptions;

namespace Youbora.plugin.options
{
    public class Options: Dictionary<string, object>
    {
        public Options() {
            /** @prop {boolean} [enabled=true] If false, the plugin won"t send NQS requests. */
            this["enabled"] = true;

            /**
             * @prop {boolean} [app.https=null]
             * Define the security of NQS calls.
             * If true it will use "https://",
             * if false it will use "http://",
             * if null/undefined it will use "//".
             */
            this["app.https"] = false;

            /** @prop {string} [host="a-fds.youborafds01.com"] Host of the Fastdata service. */
            this["host"] = "a-fds.youborafds01.com";

            /**
             * @prop {string} [accountCode="nicetest"]
             * NicePeopleAtWork account code that indicates the customer account.
             */
            this["accountCode"] = "nicetest";

            /** @prop {string} [user.name] User ID value inside your system. */
            this["user.name"] = null;

            /** @prop {string} [user.anonymousId] User ID value inside your system. */
            this["user.anonymousId"] = null;

            /**
             *  @prop {boolean} [user.obfuscateIp=false]
             * If true, the view will have the IP obfuscated
             */
            this["user.obfuscateIp"] = false;

            /** @prop {string} [user.type] User type. */
            this["user.type"] = null;

            /** @prop {string} [user.email] User email. */
            this["user.email"] = null;

            // PARSERS

            /**
             * @prop {string} [parse.CdnNameHeader]
             * If defined, resource parse will try to fetch the CDN code from the custom header defined
             * by this property. ie: "x-cdn-forward"
             */
            this["parse.cdnNameHeader"] = "x-cdn-forward";

            /**
             * @prop {boolean} [parse.CdnNode=false]
             * If true the plugin will query the CDN to retrieve the node name.
             * It might slow performance down.
             */
            this["parse.cdnNode"] = false;

            /**
             * @prop {array<string>} [parse.CdnNode.list=false]
             * If true the plugin will query the CDN to retrieve the node name.
             * It might slow performance down.
             */
            this["parse.cdnNodeList"] = new string[] { "Akamai", "Amazon", "Cloudfront", "Level3", "Fastly", "Highwinds" , "Telefonica" , "Edgecast"};


            /**
            * @prop {boolean} [parse.manifest=false]
            * If true the plugin will look for location and segment values inside hls and dash manifest to retrieve the actual resource
            * It might slow performance down.
            */
            this["parse.manifest"] = false;

            // NETWORK

            /** @prop {string} [network.ip] IP of the viewer/user. ie= "100.100.100.100". */
            this["network.ip"] = null;

            /** @prop {string} [network.isp] Name of the internet service provider of the viewer/user. */
            this["network.isp"] = null;

            /**
             * @prop {string} [network.connectionType]
             * See a list of codes in {@link http://mapi.youbora.com:8081/connectionTypes}
             */
            this["network.connectionType"] = null;

            // DEVICE

            /**
             * @prop {string} [device.code]
             * Youbora"s device code. If specified it will rewrite info gotten from user agent.
             * See a list of codes in {@link http://mapi.youbora.com:8081/devices}
             */
            this["device.code"] = null;

            /**
             * @prop {string} [device.model]
             *  If specified it will rewrite info gotten from user agent.
             */
            this["device.model"] = null;

            /**
             * @prop {string} [device.osversion]
             *  If specified it will rewrite info gotten from user agent.
             */
            this["device.osVersion"] = null;

            /**
             * @prop {string} [device.brand]
             *  If specified it will rewrite info gotten from user agent.
             */
            this["device.brand"] = null;

            /**
             * @prop {string} [device.type]
             *  If specified it will rewrite info gotten from user agent.
             */
            this["device.type"] = null;

            /**
             * @prop {string} [device.name]
             *  If specified it will rewrite info gotten from user agent.
             */
            this["device.name"] = null;

            /**
             * @prop {string} [device.osName]
             *  If specified it will rewrite info gotten from user agent.
             */
            this["device.osName"] = null;

            // CONTENT

            /** @prop {string} [content.transactionCode] Custom unique code to identify the view. */
            this["content.transactionCode"] = null;

            /** @prop {string} [content.resource] URL/path of the current media resource. */
            this["content.resource"] = null;

            /** @prop {boolean} [content.isLive] True if the content is live false if VOD. */
            this["content.isLive"] = null;

            /** @prop {boolean} [content.isLive.noSeek] True if the player seeks automatically when resumed or ending buffer. Only for live content */
            this["content.isLive.noSeek"] = false;

            /** @prop {string} [content.title] Title of the media. */
            this["content.title"] = null;

            /** @prop {string} [content.program] Secondary title of the media. */
            this["content.program"] = null;

            /** @prop {string} [content.program] Secondary title of the media, alias for title2 */
            this["content.program"] = null;

            /** @prop {number} [content.duration] Duration of the media. */
            this["content.duration"] = null;

            /** @prop {int} [content.fps] Frames per second of the content in the current moment. */
            this["content.fps"] = null;

            /** @prop {int} [content.bitrate] Bitrate of the content in bits per second. */
            this["content.bitrate"] = null;


            /** @prop {int} [content.totalBytes] Total downloaded bytes of the content. */
            this["content.totalBytes"] = null;

            /** @prop {bool} [content.sendTotalBytes] Additionaly report totalbytes or not, default false. */
            this["content.sendTotalBytes"] = false;

            /** @prop {int} [content.throughput] Throughput of the client bandwith in bits per second. */
            this["content.throughput"] = null;

            /** @prop {string} [content.rendition] Name of the current rendition of the content. */
            this["content.rendition"] = null;

            /**
             * @prop {string} [content.cdn]
             * Codename of the CDN where the content is streaming from.
             * See a list of codes in {@link http://mapi.youbora.com:8081/cdns}
             * */
            this["content.cdn"] = null;

            /** @prop {string} [content.cdnNode] CDN node id */
            this["content.cdnNode"] = null;

            /** @prop {int} [content.cdnType] CDN node type
             * TCP_HIT / HIT: 1
             * TCP_MISS / MISS: 2
             * TCP_MEM_HIT: 3
             * TCP_IMS_* : 4
             */
            this["content.cdnType"] = null;

            /**
             * @prop {object} [content.metadata]
             * Item containing mixed extra information about the content like: director, parental rating,
             * device info or the audio channels.This object may store any serializable key:value info.
             */
            this["content.metadata"] = null;

            /** @prop {string} [content.streamingProtocol] Name of the streaming media protocol.
             * Can be:
             *   - HDS	(Adobe HDS)
             *   - HLS	(Apple HLS)
             *   - MSS	(Microsoft Smooth Streaming)
             *   - DASH	(MPEG-DASH)
             *   - RTMP	(Adobe RTMP)
             *   - RTP	(RTP)
             *   - RTSP	(RTSP)
             */
            this["content.streamingProtocol"] = null;

            /** @prop {string} [content.transportformat] Name of the transport format protocol.
             * Can be:
             *   - MPEG2
             *   - MP4
             *   - CMF
             */
            this["content.transportFormat"] = null;

            /** @prop {number} [content.saga] Saga of the media. */
            this["content.saga"] = null;

            /** @prop {number} [content.tvShow] TV Show of the media. */
            this["content.tvShow"] = null;

            /** @prop {number} [content.season] Season of the media. */
            this["content.season"] = null;

            /** @prop {number} [content.episodeTitle] Episode title of the media. */
            this["content.episodeTitle"] = null;

            /** @prop {number} [content.channel] Channel name of the media. */
            this["content.channel"] = null;

            /** @prop {number} [content.id] ID of the media. */
            this["content.id"] = null;

            /** @prop {number} [content.imdbId] IMDB id of the media. */
            this["content.imdbId"] = null;

            /** @prop {number} [content.gracenoteId] Gracenote id of the media. */
            this["content.gracenoteId"] = null;

            /** @prop {number} [content.type] Type of the media. */
            this["content.type"] = null;

            /** @prop {number} [content.genre] Genre of the media. */
            this["content.genre"] = null;

            /** @prop {number} [content.language] Language of the media. */
            this["content.language"] = null;

            /** @prop {number} [content.subtitles] Subtitles of the media. */
            this["content.subtitles"] = null;

            /** @prop {number} [content.contractedResolution] Contracted Resolution of the media. */
            this["content.contractedResolution"] = null;

            /** @prop {number} [content.cost] Cost of the media. */
            this["content.cost"] = null;

            /** @prop {number} [content.price] Price of the media. */
            this["content.price"] = null;

            /** @prop {number} [content.playbackType] Type of the media. Can be Vod, Live, catch-up or offline */
            this["content.playbackType"] = null;

            /** @prop {number} [content.drm] DRM of the media. */
            this["content.drm"] = null;

            // Encoding

            /** @prop {number} [content.encoding.videoCodec] Video codec of the media. */
            this["content.encoding.videoCodec"] = null;

            /** @prop {number} [content.encoding.audioCodec] Audio codec of the media. */
            this["content.encoding.audioCodec"] = null;

            /** @prop {number} [content.encoding.codecSettings] Codec settings of the media. */
            this["content.encoding.codecSettings"] = null;

            /** @prop {number} [content.encoding.codecProfile] Codec profile of the media. */
            this["content.encoding.codecProfile"] = null;

            /** @prop {number} [content.encoding.containerFormat] Container format of the media. */
            this["content.encoding.containerFormat"] = null;


            // ADS

            /**
             * @prop {object} [ad.metadata]
             * Item containing mixed extra information about ads like: request url.
             * This object may store any serializable key:value info.
             */
            this["ad.metadata"] = null;

            /**
             * @prop {string} [ad.campaign]
             * String containing the name of the campaign
             */
            this["ad.campaign"] = null;

            /**
             * @prop {string} [ad.resource]
             * String containing the ad resource
             */
            this["ad.resource"] = null;

            /**
             * @prop {string} [ad.title]
             * String containing the title of the campaign
             */
            this["ad.title"] = null;

            /**
             * @prop {string} [ad.provider]
             * String containing the provider of the ad
             */
            this["ad.provider"] = null;

            /**
             * @prop {string} [ad.creativeId]
             * String containing the id of the creative
             */
            this["ad.creativeId"] = null;

            /**
            * @prop {object} [ad.expectedPattern]
            * Json with the position of the breaks expected.
            * Arrays are the number of breaks, and the numbers in them, the number of ads for each break
            *
            * Example:
            * {pre: [1],
            * mid: [1,2],
            * post: [1]}
            * Would be a view with 1 preroll break with 1 ad, 2 midroll breaks, one with 1 ad and
            * the other with 2, and one postroll break with 1 ad.
            */
            this["ad.expectedPattern"] = null;

            /**
            * @prop {string} [ad.givenAds]
            * Number of ads given by the adserver for this break
            */
            this["ad.givenAds"] = null;

            /**
            * @prop {array<int>} [ad.breaksTime]
            * Array of ints for the time position of adbreaks
            */
            this["ad.breaksTime"] = null;

            /**
            * @prop {string} [ad.expectedBreaks]
            * Number of breaks expected for the view
            */
            this["ad.expectedBreaks"] = null;

            /**
            * @prop {int} [ad.givenBreaks]
            * Number of breaks given by the adserver for the view
            */
            this["ad.givenBreaks"] = null;

            /**
            * @prop {boolean} [ad.ignore]
            * False by default.
            * If true, youbora blocks ad events and calculates jointime ignoring ad time.
            */
            this["ad.ignore"] = false;

            /**
            * @prop {boolean} [ad.blockDetection]
            * False by default.
            * If true, youbora sends a request to an url that looks like an ad to check if it has response.
            */
            this["ad.blockDetection"] = false;

            // APP

            /** 
             * @prop {string} [app.name]
             * String containing the name of the app
             * */
            this["app.name"] = null;

            /** 
             * @prop {string} [app.releaseVersion]
             * String containing the app version
             * */
            this["app.releaseVersion"] = null;

            // SMARTSWITCH

            /**
             *  @prop {string} [smartswitch.configCode]
             * Config code for smartswitch
             * null by default
             */
            this["smartswitch.configCode"] = null;
            
            /**
             *  @prop {string} [smartswitch.groupCode]
             * Group code for smartswitch
             * null by default
             */
            this["smartswitch.groupCode"] = null;
            
           /**
            *  @prop {string} [smartswitch.contractCode]
            * Contract code for smartswitch
            * null by default
            */
            this["smartswitch.contractCode"] = null;

            // EXTRAPARAMS

            /** @prop {string} [customDimension.1] Custom parameter 1. */
            this["content.customDimension.1"] = null;

            /** @prop {string} [customDimension.2] Custom parameter 2. */
            this["content.customDimension.2"] = null;

            /** @prop {string} [customDimension.3] Custom parameter 3. */
            this["content.customDimension.3"] = null;

            /** @prop {string} [customDimension.4] Custom parameter 4. */
            this["content.customDimension.4"] = null;

            /** @prop {string} [customDimension.5] Custom parameter 5. */
            this["content.customDimension.5"] = null;

            /** @prop {string} [customDimension.6] Custom parameter 6. */
            this["content.customDimension.6"] = null;

            /** @prop {string} [customDimension.7] Custom parameter 7. */
            this["content.customDimension.7"] = null;

            /** @prop {string} [customDimension.8] Custom parameter 8. */
            this["content.customDimension.8"] = null;

            /** @prop {string} [customDimension.9] Custom parameter 9. */
            this["content.customDimension.9"] = null;

            /** @prop {string} [customDimension.10] Custom parameter 10. */
            this["content.customDimension.10"] = null;

            /** @prop {string} [customDimension.11] Custom parameter 11. */
            this["content.customDimension.11"] = null;

            /** @prop {string} [customDimension.12] Custom parameter 12. */
            this["content.customDimension.12"] = null;

            /** @prop {string} [customDimension.13] Custom parameter 13. */
            this["content.customDimension.13"] = null;

            /** @prop {string} [customDimension.14] Custom parameter 14. */
            this["content.customDimension.14"] = null;

            /** @prop {string} [customDimension.15] Custom parameter 15. */
            this["content.customDimension.15"] = null;

            /** @prop {string} [customDimension.16] Custom parameter 16. */
            this["content.customDimension.16"] = null;

            /** @prop {string} [customDimension.17] Custom parameter 17. */
            this["content.customDimension.17"] = null;

            /** @prop {string} [customDimension.18] Custom parameter 18. */
            this["content.customDimension.18"] = null;

            /** @prop {string} [customDimension.19] Custom parameter 19. */
            this["content.customDimension.19"] = null;

            /** @prop {string} [customDimension.20] Custom parameter 20. */
            this["content.customDimension.20"] = null;

            /** @prop {string} [ad.customDimension.1] Ad custom parameter 1. */
            this["ad.customDimension.1"] = null;

            /** @prop {string} [ad.customDimension.2] Ad custom parameter 2. */
            this["ad.customDimension.2"] = null;

            /** @prop {string} [ad.customDimension.3] Ad custom parameter 3. */
            this["ad.customDimension.3"] = null;

            /** @prop {string} [ad.customDimension.4] Ad custom parameter 4. */
            this["ad.customDimension.4"] = null;

            /** @prop {string} [ad.customDimension.5] Ad custom parameter 5. */
            this["ad.customDimension.5"] = null;

            /** @prop {string} [ad.customDimension.6] Ad custom parameter 6. */
            this["ad.customDimension.6"] = null;

            /** @prop {string} [ad.customDimension.7] Ad custom parameter 7. */
            this["ad.customDimension.7"] = null;

            /** @prop {string} [ad.customDimension.8] Ad custom parameter 8. */
            this["ad.customDimension.8"] = null;

            /** @prop {string} [ad.customDimension.9] Ad custom parameter 9. */
            this["ad.customDimension.9"] = null;

            /** @prop {string} [ad.customDimension.10] Ad custom parameter 10. */
            this["ad.customDimension.10"] = null;

            /** @prop {bool} [forceInit] */
            this["forceInit"] = false;

            /** @prop {Dictionary<string,object>} [session.metrics]
             *  The format must be:
             *  {
             *      metric1: {value: 123},
             *      metric2: {value: 123}
             *  }
             *  Being metric1, metric2 a custom name, and 123 the values.
             */
            this["session.metrics"] = null;

            /** @prop {Dictionary<string,object>} [content.metrics]
             *  The format must be:
             *  {
             *      metric1: {value: 123},
             *      metric2: {value: 123}
             *  }
             *  Being metric1, metric2 a custom name, and 123 the values.
             */
            this["content.metrics"] = null;

            /** @prop {string} [
             * ID]
             * Unique ID of the device 
             */
            this["deviceUUID"] = null;

            /** @prop {bool} [device.isAnonymous]
                * Blocks deviceUUID
            */
            this["device.isAnonymous"] = false;

            /**
            * @prop {array<string>} [errors.fatal=[]]
            * If it has elements on it, all the errors matching this code will fire the stop event to end the view
            */
            this["errors.fatal"] = new string[] { };

            /**
            * @prop {array<string>} [errors.nonFatal=[]]
            * If it has elements on it, all the errors matching this code won't fire a stop event to end the view
            */
            this["errors.nonFatal"] = new string[] { };

            /**
            * @prop {array<string>} [errors.ignore=[]]
            * If it has elements on it, all the errors matching this code wont be reported
            */
            this["errors.ignore"] = new string[] { };

            /**
            * @prop {string} linkedViewID
            * String to send on start events to link views with previous session events
            */
            this["linkedViewID"] = null;

            /**
            * @prop {bool} [waitForMetadata]
            * Boolean to delay the start event. Use with `pendingMetadata`
            */
            this["waitForMetadata"] = false;

            /**
            * @prop {List<string>} [pendingMetadata]
            * List of values that should be ready to send in start event. Use with `waitForMetadata` set to True.
            */
            this["pendingMetadata"] = new string[] { };
        }
        public Options(Dictionary<string,object> options) : this()
        {
            SetOptions(options);
        }

        /**
         * Recursively sets the properties present in the params object.
         * ie: this.username = params.username.
         *
         * @param {Object} options A literal or another Data containing values.
         * @param {Object} [base=this] Start point for recursion.
         */

        public void SetOptions(Dictionary<string, object> dictionary) {
            SetOptions(dictionary, this);
        }

        public void SetOptions(Dictionary<string, object> dictionary, Dictionary<string, object> parent)
        {
            try
            {
                foreach (var kv in dictionary)
                {
                    string key = kv.Key;
                    if (!parent.ContainsKey(key) && DeprecatedOptions.Exists(key))
                    {
                        key = DeprecatedOptions.GetNewName(key);
                    }
                    if (parent.ContainsKey(key))
                    {
                        Type t = parent[key]?.GetType();
                        if (t == typeof(List<string>))
                        {
                            parent[key] = ((JArray)kv.Value).ToObject<List<string>>();
                        }
                        else if (t == typeof(Dictionary<string, object>))
                        {
                            Dictionary<string, object> dict = new Dictionary<string, object>();

                            Type t2 = kv.Value.GetType();
                            if (t2 == typeof(JObject))
                            {
                                dict = ((JObject)kv.Value).ToObject<Dictionary<string, object>>();
                            }
                            else if (t2 == typeof(Dictionary<string, object>))
                            {
                                dict = (Dictionary<string, object>)kv.Value;
                            }

                            SetOptions(dict, (Dictionary<string, object>)parent[key]);
                        }
                        else
                        {
                            parent[key] = kv.Value;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Exception(ex);
            }
        }

        public string ToJson()
        {
            try
            {
                return JsonConvert.SerializeObject(this);
            }
            catch (Exception ex)
            {
                Log.Exception(ex);
                return "{}";
            }
        }
    }

    public static class StreamingProtocol
    {
        public static string HDS = "HDS";
        public static string HLS = "HLS";
        public static string MSS = "MSS";
        public static string DASH = "DASH";
        public static string RTMP = "RTMP";
        public static string RTP = "RTP";
        public static string RTSP = "RTSP";
    }
}
