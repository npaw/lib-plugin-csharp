﻿using System;
using System.Collections.Generic;

namespace Youbora.plugin.storage
{
    public sealed class Storage
    {
        static readonly Lazy<Storage> lazy = new Lazy<Storage>(() => new Storage());
        private static Dictionary<string, object> storageObject = new Dictionary<string, object>();

        private Storage(){ }

        public static Storage Instance => lazy.Value;

        public static void SetValue(string key, object value)
        {
            RemoveValue(key);
            storageObject.Add(key, value);
        }

        public static object GetValue(string key)
        {
            if (storageObject.ContainsKey(key))
            {
                return storageObject[key];
            }
            return null;
        }

        public static void RemoveValue(string key)
        {
            if (storageObject.ContainsKey(key))
            {
                storageObject.Remove(key);
            }
        }
    }
}
