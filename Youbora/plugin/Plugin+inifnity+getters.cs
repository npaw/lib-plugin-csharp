﻿using System;
using Youbora.emitter;
using Youbora.plugin.storage;

namespace Youbora.plugin
{
    public partial class Plugin : Emitter
    {

        public string GetSessionId()
        {
            return (string) Storage.GetValue("session");
        }

        public string GetContext()
        {
            return (string)Storage.GetValue("context");
        }

        public object GetStoredData()
        {
            return Storage.GetValue("data");
        }

        public bool GetIsSessionExpired()
        {
            long time = Convert.ToInt64(infinity.GetFirstActive().Ticks) + Convert.ToInt64((int)sessionExpire);
            long now = DateTime.Now.Ticks;
            if (GetSessionId() == null)
            {
                return true;
            }
            return (time < now);
        }

        public bool GetIsDataExpired()
        {
            long time = Convert.ToInt64(infinity.GetFirstActive().Ticks) + Convert.ToInt64(sessionExpire);
            long now = DateTime.Now.Ticks;
            if (GetStoredData() == null)
            {
                return true;
            }
            return (time < now);
        }

        public string GetPageName()
        {
            return null;
        }
    }
}
