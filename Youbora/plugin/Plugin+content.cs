﻿using System;
using System.Collections.Generic;
using Youbora.emitter;
using Youbora.constants;
using Youbora.adapter;
using Youbora.log;
using System.Reflection;

namespace Youbora.plugin
{
    public partial class Plugin : Emitter
    {
        Dictionary<string, object> lastErrorParams = null;
        DateTime? lastErrorTime = null;
        /**
           * Sets an adapter for video content.
           *
           * @param {Adapter} adapter
           *
           * @memberof youbora.Plugin.prototype
           */
        public void SetAdapter(Adapter adapter)
        {
            if (adapter == null) return;
            if (adapter.plugin != null)
            {
                Log.Warn("Adapters can only be added to a single plugin");
            }
            else
            {
                RemoveAdapter();
                this.adapter = adapter;
                adapter.plugin = this;
                adapter.SetIsAdsAdapter(false);
                // Register listeners
                adapter.On(Event.START, StartListener);
                adapter.On(Event.JOIN, JoinListener);
                adapter.On(Event.PAUSE, PauseListener);
                adapter.On(Event.RESUME, ResumeListener);
                adapter.On(Event.SEEK_BEGIN, SeekBeginListener);
                adapter.On(Event.SEEK_END, SeekEndListener);
                adapter.On(Event.BUFFER_BEGIN, BufferBeginListener);
                adapter.On(Event.BUFFER_END, BufferEndListener);
                adapter.On(Event.ERROR, ErrorListener);
                adapter.On(Event.STOP, StopListener);
                adapter.On(Event.VIDEO_EVENT, VideoEventListener);
            }
        }

        /**
        * Returns current adapter or null.
        *
        * @returns {Adapter}
        *
        * @memberof youbora.Plugin.prototype
        */
        public Adapter GetAdapter()
        {
            return adapter;
        }

        /**
         * Removes the current adapter. Fires stop if needed. Calls adapter.dispose().
         *
         * @memberof youbora.Plugin.prototype
         * */
        public void RemoveAdapter()
        {
            if (adapter != null)
            {
                adapter.Dispose();

                adapter.plugin = null;
                
                adapter.Off(Event.START, StartListener);
                adapter.Off(Event.JOIN, JoinListener);
                adapter.Off(Event.PAUSE, PauseListener);
                adapter.Off(Event.RESUME, ResumeListener);
                adapter.Off(Event.SEEK_BEGIN, SeekBeginListener);
                adapter.Off(Event.SEEK_END, SeekEndListener);
                adapter.Off(Event.BUFFER_BEGIN, BufferBeginListener);
                adapter.Off(Event.BUFFER_END, BufferEndListener);
                adapter.Off(Event.ERROR, ErrorListener);
                adapter.Off(Event.STOP, StopListener);
                adapter.Off(Event.VIDEO_EVENT, VideoEventListener);

                adapter = null;
            }
        }

        // ---------------------------------------- LISTENERS -----------------------------------------
        private void StartListener(object e)
        {
            if (!isInitiated)
            {
                viewTransform.NextView();
                InitComm();
                StartPings();
            }
            else if (initChrono.StartTime != null)
            {
                adapter.chronos.join.StartTime = initChrono.StartTime;
            }
            Dictionary<string, object> parameters = (Dictionary<string, object>)e;
            bool allParamsReady = GetResource() != null && GetIsLive() != null && (GetDuration() != null || GetIsLive() == true) && GetTitle() != null;
            if ((bool)options["forceInit"] || !this.IsExtraMetadataReady())
            {
                allParamsReady = false;
            }
            if (allParamsReady && !isInitiated)
            { //start
                Send(WillSendEvent.WILL_SEND_START, Service.START, parameters);
                AdSavedError();
                AdSavedManifest();
                Log.Notice("{0}", Service.START);
                isStarted = true;
            }
            else if (!isInitiated)
            {//init
                isInitiated = true;
                adapter.chronos.join.Start();
                Send(WillSendEvent.WILL_SEND_INIT, Service.INIT, parameters);
                AdSavedError();
                AdSavedManifest();
                Log.Notice("{0}", Service.INIT);
            }
        }

        private void JoinListener(object e)
        {
            Dictionary<string, object> parameters = (Dictionary<string, object>)e;
            if (adsAdapter == null || !adsAdapter.flags.isStarted)
            {
                if (adsAdapter != null)
                {
                    DateTime? startTime = null;
                    if (adapter.chronos.join.StartTime == null || totalPrerollsTime < 0)
                    {
                        startTime = DateTime.Now;
                    }
                    else
                    {
                        startTime = ((DateTime)adapter.chronos.join.StartTime).Add(TimeSpan.FromMilliseconds(totalPrerollsTime));
                    }
                    totalPrerollsTime = 0;
                }
                if (isInitiated && !isStarted)
                { 
                    Dictionary<string, object> parameters2 = new Dictionary<string, object>();
                    if (IsExtraMetadataReady())
                    {
                        Send(WillSendEvent.WILL_SEND_START, Service.START, parameters2);
                    }
                    else
                    {
                        startDelayed = true;
                    }
                    AdSavedError();
                    AdSavedManifest();
                    Log.Notice("{0}", Service.START);
                    isStarted = true;
                }
                if (adsAdapter != null && isBreakStarted)
                {
                    adsAdapter.FireBreakStop();
                }
                Send(WillSendEvent.WILL_SEND_JOIN, Service.JOIN, parameters);
                Log.Notice("{0} {1}ms", Service.JOIN, parameters["joinDuration"]);
            }
            else
            { // If it is currently showing ads, join is invalidated
                if (adapter.monitor != null) adapter.monitor.Stop();
                adapter.flags.isJoined = false;
                adapter.chronos.join.LastTime = adapter.chronos.join.StartTime;
            }
        }

        private void RetryStart()
        {
            if (IsExtraMetadataReady())
            {
                this.Send(WillSendEvent.WILL_SEND_START, Service.START, new Dictionary<string, object>());
                startDelayed = false;
            }
        }

        private void PauseListener(object e)
        {
            if (adapter != null)
            {
                if (adapter.flags.isBuffering || adapter.flags.isSeeking || (adsAdapter != null && adsAdapter.flags.isStarted))
                {
                    adapter.chronos.pause.Reset();
                }
            }

            Dictionary<string, object> parameters = (Dictionary<string, object>)e;
            Send(WillSendEvent.WILL_SEND_PAUSE, Service.PAUSE, parameters);
            Log.Notice("{0} at {1}s", Service.PAUSE, parameters["playhead"]);
        }

        private void ResumeListener(object e)
        {
            if (adsAdapter != null && isBreakStarted && adsAdapter.flags.isStarted)
            {
                adsAdapter.FireBreakStop();
            }
            Dictionary<string, object> parameters = (Dictionary<string, object>)e;
            Send(WillSendEvent.WILL_SEND_RESUME, Service.RESUME, parameters);
            Log.Notice("{0} {1}ms", Service.RESUME, parameters["pauseDuration"]);
            adapter.chronos.pause.Reset();
        }

        private void SeekBeginListener(object e)
        {
            if (adapter != null && adapter.flags.isPaused) adapter.chronos.pause.Reset();
            Log.Notice("Seek Begin");
        }

        private void SeekEndListener(object e)
        {
            Dictionary<string, object> parameters = (Dictionary<string, object>)e;
            Send(WillSendEvent.WILL_SEND_SEEK, Service.SEEK, parameters);
            Log.Notice("{0} to {1} in {2}ms", Service.SEEK, parameters["playhead"], parameters["seekDuration"]);
        }

        private void BufferBeginListener(object e)
        {
            if (adapter != null && adapter.flags.isPaused) adapter.chronos.pause.Reset();
            Log.Notice("Buffer Begin");
        }

        private void BufferEndListener(object e)
        {
            Dictionary<string, object> parameters = (Dictionary<string, object>)e;
            Send(WillSendEvent.WILL_SEND_BUFFER, Service.BUFFER, parameters);
            Log.Notice("{0} to {1} in {2}ms", Service.BUFFER, parameters["playhead"], parameters["bufferDuration"]);
        }

        private void ErrorListener(object e)
        {
            Dictionary<string, object> parameters = (Dictionary<string, object>)e;
            if (!BlockError(parameters))
            {
                FireError(parameters);
                AdSavedError();
                AdSavedManifest();
            }
        }

        private bool BlockError(Dictionary<string, object> errorParams)
        {
            DateTime now = DateTime.Now;

            bool sameError = false;
            if (lastErrorParams != null)
            {
                if (lastErrorParams.ContainsKey("errorCode") && errorParams.ContainsKey("errorCode"))
                {
                    sameError = lastErrorParams["errorCode"] == errorParams["errorCode"];
                }
                else
                {
                    sameError = true;
                }
                if (lastErrorParams.ContainsKey("msg") && errorParams.ContainsKey("msg"))
                {
                    sameError = sameError && lastErrorParams["msg"] == errorParams["msg"];
                }
            }
            if (sameError && (lastErrorTime != null || DateTime.Compare((DateTime)lastErrorTime, now.AddSeconds(-5)) > 0))
            {
                lastErrorTime = DateTime.Now;
                return true;
            }
            lastErrorTime = DateTime.Now;
            lastErrorParams = errorParams;
            return false;
        }

        private void StopListener(object e)
        {
            Dictionary<string, object> parameters = (Dictionary<string, object>)e;
            FireStop(parameters);
        }

        private void VideoEventListener(object e)
        {
            Dictionary<string, object> parameters = (Dictionary<string, object>)e;
            Send(WillSendEvent.WILL_SEND_VIDEO_EVENT, Service.VIDEO_EVENT, parameters);
        }

        private bool IsExtraMetadataReady()
        {
            // If the option is disabled, always is ready
            if ((bool)this.options["waitForMetadata"] == false || ((string[])this.options["pendingMetadata"]).Length < 1)
            {
                return true;
            }
            //If for existing parameters, one of them is false (no value for it), return false
            Dictionary<string, string> getters = this.requestBuilder.GetGetters();
            foreach (string param in (string[])options["pendingMetadata"])
            {
                if (getters.ContainsKey(param))
                {
                    string methodname = getters[param];
                    var methods = this.GetType().GetTypeInfo().DeclaredMethods;
                    MethodInfo getterMethod = null;
                    foreach (MethodInfo mi in methods)
                    {
                        if (mi.Name == methodname)
                        {
                            getterMethod = mi;
                        }
                    }
                    if (getterMethod != null)
                    {
                        object value = getterMethod.Invoke(this, null);
                        if (value == null)
                        {
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        public bool IsStopReady()
        {
            try
            {
                bool ret = true;
                if (isAdsManifestSent && options["ad.expectedPattern"] != null &&
                  ((Dictionary<string, List<int>>)options["ad.expectedPattern"]).ContainsKey("post") &&
                  ((List<int>)((Dictionary<string, List<int>>)options["ad.expectedPattern"])["post"])[0] > this.playedPostrolls)
                {
                    ret = false;
                }
                return ret;
            } catch (Exception err)
            {
                return true; //Wrong format, will crash somewhere else before so message is not needed
            }
        }
    }
}
