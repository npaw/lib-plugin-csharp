﻿using System.Collections.Generic;
using Youbora.emitter;
using Youbora.constants;
using Youbora.log;

namespace Youbora.plugin
{
    public partial class Plugin : Emitter
    {

      /**
       * Sends /init. Should be called once the user has requested the content. Does not need
       * a working adapter or player to work. it won't sent start if isInitiated is true.
       *
       * @param {Object} [parameters] Object of key:value parameters.
       *
       * @memberof youbora.Plugin.prototype
       */
        public void FireInit()
        {
            FireInit(new Dictionary<string, object>());
        }
        public void FireInit(Dictionary<string,object> parameters)
        {
            if (!isInitiated)
            {
                if (GetAdapter() == null || (!GetAdapter().flags.isStarted))
                {
                    viewTransform.NextView();
                    InitComm();
                    StartPings();
                    initChrono.Start();
                    isInitiated = true;
                    Send(WillSendEvent.WILL_SEND_INIT, Service.INIT, parameters);
                    AdSavedError();
                    AdSavedManifest();
                    Log.Notice("{0} {1}", Service.INIT, parameters["title"]);
                }
            }
        }

        /**
        * Sends /error. Should be used when the error is related to out-of-player errors: like async
        * resource load or player loading errors.
        *
        * @param {String} [code] Error Code, if an object is sent, it will be treated as parameters.
        * @param {String} [msg] Error Message
        * @param {String} [metadata] Object defining error metadata
        * @param {String} [level] Level of the error. Currently supports 'error' and 'fatal'
        *
        * @memberof youbora.Plugin.prototype
        */
        public void FireError()
        {
            FireError(null);
        }
        public void FireError(Dictionary<string,object> parameters)
        {
            FireInit();

            Send(WillSendEvent.WILL_SEND_ERROR, Service.ERROR, parameters);
            AdSavedError();
            AdSavedManifest();
            Log.Notice("{0}", Service.ERROR);

            if (parameters.ContainsKey("errorLevel"))
            {
                if ((string)parameters["errorLevel"] == "fatal")
                {
                    FireStop();
                }
            }
        }

        /**
        * Calls fireErrors and then stops pings.
        *
        * @param {String} [code] Error Code, if an object is sent, it will be treated as parameters.
        * @param {String} [msg] Error Message
        * @param {Object} [metadata] Object defining error metadata
        *
        * @memberof youbora.Plugin.prototype
        */
        public void FireFatalError()
        {
            FireFatalError(null);
        }
        public void FireFatalError(Dictionary<string,object> parameters)
        {
            FireError(parameters);
            FireStop();
        }

        /**
        * Fires /stop. Should be used to terminate sessions once the player is gone or if
        * plugin.fireError() is called.
        *
        * @param {Object} [parameters] Object of key:value parameters.
        *
        * @memberof youbora.Plugin.prototype
        */
        public void FireStop()
        {
            FireStop(new Dictionary<string, object>());
        }
        public void FireStop(Dictionary<string, object> parameters)
        {
            if (isInitiated || isStarted)
            {
                if (adapter != null)
                {
                    adapter.flags.isStopped = true;
                    if (adapter.monitor != null)
                    {
                        adapter.monitor.Stop();
                    }
                    if (adsAdapter != null && isBreakStarted)
                    {
                        adsAdapter.FireBreakStop();
                    }
                }
                Send(WillSendEvent.WILL_SEND_STOP, Service.STOP, parameters);
                Log.Notice("{0} at {1}", Service.STOP, parameters["playhead"]);
                Reset();
            }
        }
    }
}
