﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using Youbora.emitter;
using Youbora.util;
using Youbora.version;
using System.Globalization;

namespace Youbora.plugin
{
    public partial class Plugin : Emitter
    {
        /**
         * Returns service host
         *
         * @memberof youbora.Plugin.prototype
         */
        public string GetHost()
        {
            string host = (string) options["host"];
            if (viewTransform != null && viewTransform.response != null && viewTransform.response.Count != 0)
            {
                if (viewTransform.response.ContainsKey("host"))
                {
                    host = (string)viewTransform.response["host"];
                }
                else if (viewTransform.response.ContainsKey("msg"))
                {
                    var obj = JsonConvert.DeserializeObject<Dictionary<string, Dictionary<string, object>>>((string)viewTransform.response["msg"]);
                    if (obj.ContainsKey("q"))
                    {
                        if (obj["q"].ContainsKey("h"))
                        {
                            host = (string) obj["q"]["h"];
                        }
                    }
                }
            }
            return Util.AddProtocol(Util.StripProtocol(host), (bool) options["app.https"]);
        }


        public string GetUserType()
        {
            return (string)options["user.type"];
        }

        /**
        * Returns parse manifest
        *
        * @memberof youbora.Plugin.prototype
        */
        public bool IsParseManifest()
        {
            return (bool) options["parse.manifest"];
        }

        /**
        * Returns parse CdnNode Flag
        *
        * @memberof youbora.Plugin.prototype
        */
        public bool IsParseCdnNode()
        {
            return (bool) options["parse.cdnNode"];
        }

        /**
        * Returns Cdn list
        *
        * @memberof youbora.Plugin.prototype
        */
        public string[] GetParseCdnNodeList()
        {
            return (string[]) options["parse.cdnNodeList"];
        }

        /**
        * Returns Cdn header name
        *
        * @memberof youbora.Plugin.prototype
        */
        public string GetParseCdnNodeNameHeader()
        {
            return (string) options["parse.cdnNameHeader"];
        }

        /**
        * Returns obfuscateIp option
        *
        * @memberof youbora.Plugin.prototype
        */
        public bool GetObfuscateIp()
        {
            return (bool)options["user.obfuscateIp"];
        }

        /**
        * Returns content's Extraparam1
        *
        * @memberof youbora.Plugin.prototype
        */
        public string GetExtraparam1()
        {
            string ret = (string)options["content.customDimension.1"];
            return ret;
        }

        /**
        * Returns content's Extraparam2
        *
        * @memberof youbora.Plugin.prototype
        */
        public string GetExtraparam2()
        {
            string ret = (string)options["content.customDimension.2"];
            return ret;
        }

        /**
        * Returns content's Extraparam3
        *
        * @memberof youbora.Plugin.prototype
        */
        public string GetExtraparam3()
        {
            string ret = (string)options["content.customDimension.3"];
            return ret;
        }

        /**
        * Returns content's Extraparam4
        *
        * @memberof youbora.Plugin.prototype
        */
        public string GetExtraparam4()
        {
            string ret = (string)options["content.customDimension.4"];
            return ret;
        }

        /**
        * Returns content's Extraparam5
        *
        * @memberof youbora.Plugin.prototype
        */
        public string GetExtraparam5()
        {
            string ret = (string)options["content.customDimension.5"];
            return ret;
        }

        /**
        * Returns content's Extraparam6
        *
        * @memberof youbora.Plugin.prototype
        */
        public string GetExtraparam6()
        {
            string ret = (string)options["content.customDimension.6"];
            return ret;
        }

        /**
        * Returns content's Extraparam7
        *
        * @memberof youbora.Plugin.prototype
        */
        public string GetExtraparam7()
        {
            string ret = (string)options["content.customDimension.7"];
            return ret;
        }

        /**
        * Returns content's Extraparam8
        *
        * @memberof youbora.Plugin.prototype
        */
        public string GetExtraparam8()
        {
            string ret = (string)options["content.customDimension.8"];
            return ret;
        }

        /**
        * Returns content's Extraparam9
        *
        * @memberof youbora.Plugin.prototype
        */
        public string GetExtraparam9()
        {
            string ret = (string)options["content.customDimension.9"];
            return ret;
        }

        /**
        * Returns content's Extraparam10
        *
        * @memberof youbora.Plugin.prototype
        */
        public string GetExtraparam10()
        {
            string ret = (string)options["content.customDimension.10"];
            return ret;
        }

        /**
        * Returns content's Extraparam11
        *
        * @memberof youbora.Plugin.prototype
        */
        public string GetExtraparam11()
        {
            string ret = (string)options["content.customDimension.11"];
            return ret;
        }

        /**
        * Returns content's Extraparam12
        *
        * @memberof youbora.Plugin.prototype
        */
        public string GetExtraparam12()
        {
            string ret = (string)options["content.customDimension.12"];
            return ret;
        }

        /**
        * Returns content's Extraparam13
        *
        * @memberof youbora.Plugin.prototype
        */
        public string GetExtraparam13()
        {
            string ret = (string)options["content.customDimension.13"];
            return ret;
        }

        /**
        * Returns content's Extraparam14
        *
        * @memberof youbora.Plugin.prototype
        */
        public string GetExtraparam14()
        {
            string ret = (string)options["content.customDimension.14"];
            return ret;
        }

        /**
        * Returns content's Extraparam15
        *
        * @memberof youbora.Plugin.prototype
        */
        public string GetExtraparam15()
        {
            string ret = (string)options["content.customDimension.15"];
            return ret;
        }

        /**
        * Returns content's Extraparam16
        *
        * @memberof youbora.Plugin.prototype
        */
        public string GetExtraparam16()
        {
            string ret = (string)options["content.customDimension.16"];
            return ret;
        }

        /**
        * Returns content's Extraparam17
        *
        * @memberof youbora.Plugin.prototype
        */
        public string GetExtraparam17()
        {
            string ret = (string)options["content.customDimension.17"];
            return ret;
        }

        /**
        * Returns content's Extraparam18
        *
        * @memberof youbora.Plugin.prototype
        */
        public string GetExtraparam18()
        {
            string ret = (string)options["content.customDimension.18"];
            return ret;
        }

        /**
        * Returns content's Extraparam19
        *
        * @memberof youbora.Plugin.prototype
        */
        public string GetExtraparam19()
        {
            string ret = (string)options["content.customDimension.19"];
            return ret;
        }

        /**
        * Returns content's Extraparam20
        *
        * @memberof youbora.Plugin.prototype
        */
        public string GetExtraparam20()
        {
            string ret = (string)options["content.customDimension.20"];
            return ret;
        }



        /**
         * Returns ad's Extraparam1
         *
         * @memberof youbora.Plugin.prototype
         */
        public string GetAdExtraparam1()
        {
            return (string) options["ad.customDimension.1"];
        }

        /**
        * Returns ad's Extraparam2
        *
        * @memberof youbora.Plugin.prototype
        */
        public string GetAdExtraparam2()
        {
            return (string) options["ad.customDimension.2"];
        }
        /**
         * Returns ad's Extraparam3
         *
         * @memberof youbora.Plugin.prototype
         */
        public string GetAdExtraparam3()
        {
            return (string) options["ad.customDimension.3"];
        }
        /**
         * Returns ad's Extraparam4
         *
         * @memberof youbora.Plugin.prototype
         */
        public string GetAdExtraparam4()
        {
            return (string) options["ad.customDimension.4"];
        }
        /**
         * Returns ad's Extraparam5
         *
         * @memberof youbora.Plugin.prototype
         */
        public string GetAdExtraparam5()
        {
            return (string) options["ad.customDimension.5"];
        }
        /**
         * Returns ad's Extraparam6
         *
         * @memberof youbora.Plugin.prototype
         */
        public string GetAdExtraparam6()
        {
            return (string) options["ad.customDimension.6"];
        }
        /**
         * Returns ad's Extraparam7
         *
         * @memberof youbora.Plugin.prototype
         */
        public string GetAdExtraparam7()
        {
            return (string) options["ad.customDimension.7"];
        }
        /**
         * Returns ad's Extraparam8
         *
         * @memberof youbora.Plugin.prototype
         */
        public string GetAdExtraparam8()
        {
            return (string) options["ad.customDimension.8"];
        }
        /**
         * Returns ad's Extraparam9
         *
         * @memberof youbora.Plugin.prototype
         */
        public string GetAdExtraparam9()
        {
            return (string) options["ad.customDimension.9"];
        }
        /**
         * Returns ad's Extraparam10
         *
         * @memberof youbora.Plugin.prototype
         */
        public string GetAdExtraparam10()
        {
            return (string) options["ad.customDimension.10"];
        }

        /**
         * Returns PluginInfo
         *
         * @memberof youbora.Plugin.prototype
         */
        public string GetPluginInfo()
        {
            string ret = "lib: " + LibVersion.VERSION + ", adapter: " + GetAdapterVersion() + ", adAdapter: " + GetAdAdapterVersion();
            return ret;
        }

        /**
        * Returns Ip
        *
        * @memberof youbora.Plugin.prototype
        */
        public string GetIp()
        {
            return (string) options["network.ip"];
        }

        /**
        * Returns Isp
        *
        * @memberof youbora.Plugin.prototype
        */
        public string GetIsp()
        {
            return (string) options["network.isp"];
        }

        /**
        * Returns ConnectionType
        *
        * @memberof youbora.Plugin.prototype
        */
        public string GetConnectionType()
        {
            return (string) options["network.connectionType"];
        }

        /**
        * Returns DeviceInfo
        *
        * @memberof youbora.Plugin.prototype
        */
        public Dictionary<string,object> GetDeviceInfo()
        {
            Dictionary<string, object> deviceinfo = new Dictionary<string, object>();
            //code
            if (options["device.code"] != null)
            {
                deviceinfo["deviceCode"] = options["device.code"].ToString();
            }
            //model
            if (options["device.model"] != null)
            {
                deviceinfo["model"] = options["device.model"].ToString();
            }
            //brand
            if (options["device.brand"] != null)
            {
                deviceinfo["brand"] = options["device.brand"].ToString();
            }
            //device type
            if (options["device.type"] != null)
            {
                deviceinfo["deviceType"] = options["device.type"].ToString();
            }
            //device name
            if (options["device.name"] != null)
            {
                deviceinfo["deviceName"] = options["device.name"].ToString();
            }
            //osName
            if (options["device.osName"] != null)
            {
                deviceinfo["osName"] = options["device.osName"].ToString();
            }
            //osVersion
            if (options["device.osVersion"] != null)
            {
                deviceinfo["osVersion"] = options["device.osVersion"].ToString();
            }
            if (deviceinfo.Count > 0)
            {
                return deviceinfo;
            }
            return null;
        }

        /**
        * Returns Device Model
        *
        * @memberof youbora.Plugin.prototype
        */
        public string GetDeviceModel()
        {
            return (string) options["device.model"];
        }

        /**
        * Returns Device Brand
        *
        * @memberof youbora.Plugin.prototype
        */
        public string GetDeviceBrand()
        {
            return (string)options["device.brand"];
        }

        /**
        * Returns Device Type
        *
        * @memberof youbora.Plugin.prototype
        */
        public string GetDeviceType()
        {
            return (string)options["device.type"];
        }

        /**
        * Returns Device Name
        *
        * @memberof youbora.Plugin.prototype
        */
        public string GetDeviceName()
        {
            return (string)options["device.name"];
        }

        /**
        * Returns OS Name
        *
        * @memberof youbora.Plugin.prototype
        */
        public string GetOsName()
        {
            return (string)options["device.osName"];
        }

        /**
        * Returns OS Version
        *
        * @memberof youbora.Plugin.prototype
        */
        public string GetOsVersion()
        {
            return (string)options["device.osVersion"];
        }

        /**
        * Returns AccountCode
        *
        * @memberof youbora.Plugin.prototype
        */
        public string GetAccountCode()
        {
            return (string) options["accountCode"];
        }

        /**
        * Returns Username
        *
        * @memberof youbora.Plugin.prototype
        */
        public string GetUsername()
        {
            return (string) options["user.name"];
        }

        /**
        * Returns Username
        *
        * @memberof youbora.Plugin.prototype
        */
        public string GetAnonymousUser()
        {
            return (string)options["user.anonymousId"];
        }

        /**
        * Returns the nodehost
        *
        * @memberof youbora.Plugin.prototype
        */
        public string GetNodeHost()
        {
            if ((string) options["content.cdnNode"] != null)
            {
                return (string) options["content.cdnNode"];
            }
            return resourceTransform.GetNodeHost();
        }

         /**
        * Returns user email
        *
        * @memberof youbora.Plugin.prototype
        */
        public string GetEmail()
        {
            return (string)options["user.email"];
        }

        /**
        * Returns the node type id
        *
        * @memberof youbora.Plugin.prototype
        */
        public int? GetNodeType()
        {
            if((int?) options["content.cdnType"] != null)
            {
                return (int?) options["content.cdnType"];
            }
            return resourceTransform.GetNodeType();
        }

        /**
        * Returns the node type string
        *
        * @memberof youbora.Plugin.prototype
        */
        public string GetNodeTypeString()
        {
            return resourceTransform.GetNodeTypeString();
        }

        /**
        * Returns requestNumber value, to prevent /data calls being cached
        *
        * @memberof youbora.Plugin.prototype
        */
        public double GetRequestNumber()
        {
            return new Random().NextDouble();
        }

        public string GetYouboraId()
        {
            return (string)YouboraId;
        }
        public string GetReferer()
        {
            return "";
        }

        public string GetLanguage()
        {
            return CultureInfo.CurrentCulture.Name;
        }

        public string GetSmartswitchConfigCode()
        {
            return (string)options["smartswitch.configCode"];
        }

        public string GetSmartswitchGroupCode()
        {
            return (string)options["smartswitch.groupCode"];
        }

        public string GetSmartswitchContractCode()
        {
            return (string)options["smartswitch.contractCode"];
        }

        public string GetAppName()
        {
            return (string)options["app.name"];
        }

        public string GetAppReleaseVersion()
        {
            return (string)options["app.releaseVersion"];
        }

        public string GetDeviceUUID()
        {
            return deviceUUID.GetKey();
        }

        public bool? GetIsBlocked()
        {
            bool? ret = null;
            if (blockDetector != null)
            {
                ret = blockDetector.isBlocked;
            }
            return ret;
        }
    }
}
