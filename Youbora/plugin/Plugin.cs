﻿using System;
using System.Collections.Generic;
using Youbora.emitter;
using Youbora.timer;
using Youbora.chrono;
using Youbora.adapter;
using Youbora.comm.request;
using Youbora.comm.communication;
using Youbora.comm.transform.flowTransform;
using Youbora.comm.transform.viewTransform;
using Youbora.comm.transform.resourceTransform;
using Youbora.plugin.options;
using Youbora.plugin.storage;
using Youbora.plugin.requestBuilder;
using Youbora.infinity;
using Youbora.detectors;
using Youbora.deviceuuid;
using Newtonsoft.Json;

namespace Youbora.plugin
{
    public partial class Plugin : Emitter
    {
        public Infinity infinity = null;
        public bool isInitiated = false;
        public bool isStarted = false;
        public bool startDelayed = false;
        public Chrono initChrono = null;
        public Storage storage = null;
        public Options options = null;
        private Adapter adapter = null;
        private Adapter adsAdapter = null;
        public Timer ping = null;
        public Timer beat = null;
        public Timer refreshData = null;
        public RequestBuilder requestBuilder = null;
        public ResourceTransform resourceTransform = null;
        public BackgroundDetector backgroundDetector = null;
        public BlockDetector blockDetector = null;
        public DateTime? lastEventTime = null;
        public ViewTransform viewTransform = null;
        public FlowTransform flowTransform = null;
        private Communication commInstance = null;
        private DeviceUUID deviceUUID = null;
        public double sessionExpire = 180000;
        private string YouboraId = null;
        public bool isBreakStarted = false;
        private int playedPostrolls = 0;
        private bool isAdsManifestSent = false;
        private double totalPrerollsTime = 0;
        private object savedAdManifest = null;

        public Plugin(object optionsIn, Adapter adapterIn)
        {
            Dictionary<string, object> formattedOptions = JsonConvert.DeserializeObject<Dictionary<string, object>>((string)optionsIn);
            ConstructorMethod(formattedOptions, adapterIn);
        }

        public Plugin(Dictionary<string,object> optionsIn, Adapter adapterIn)
        {
            ConstructorMethod(optionsIn, adapterIn);
        }

        public Plugin(Dictionary<string,object> optionsIn)
        {
            ConstructorMethod(optionsIn, null);
        }
        public Plugin(object optionsIn)
        {
            Dictionary<string, object> formattedOptions = JsonConvert.DeserializeObject<Dictionary<string, object>>((string)optionsIn);
            ConstructorMethod(formattedOptions, null);
        }

        private void ConstructorMethod(Dictionary<string, object> optionsIn, Adapter adapterIn)
        {
            /** Instance of youbora infinity. */
            infinity = new Infinity(this);

            /** This flags indicates that /init has been called. */
            isInitiated = false;

            /** This flags indicates that /start has been delayed and will be sent after jointime. */
            startDelayed = false;

            initChrono = new Chrono();

            /** Stored {@link Options} of the session. */
            options = new Options(optionsIn);

            /** DeviceUUID manager */
            deviceUUID = new DeviceUUID(this);

            ping = new Timer(SendPing, 5000);
            beat = new Timer(SendBeat, 30000);
            refreshData = new Timer(CheckOldData, 3600000);
            refreshData.Start();

            requestBuilder = new RequestBuilder(this);
            resourceTransform = new ResourceTransform(this);
            flowTransform = new FlowTransform();

            lastEventTime = DateTime.Now;

            if (adapterIn != null)
            {
                SetAdapter(adapterIn);
            }

            RestartViewTransform();
            InitInfinity();
            backgroundDetector = new BackgroundDetector(this);

            if ((bool)options["ad.blockDetection"])
            {
                blockDetector = new BlockDetector(this);
            }
        }

        public void CheckOldData(object dt)
        {
            if (this.adapter != null && this.adapter.flags.isStarted)
            {
                return;
            }
            if (this.infinity.infinityStarted && !this.infinity.infinityStopped)
            {
                return;
            }
            this.RestartViewTransform();
        }

        public void RestartViewTransform()
        {
            // FastData
            viewTransform = new ViewTransform(this, null);
            viewTransform.On(ViewTransform.Event.DONE, ReceiveData);

            if (GetIsDataExpired() == true)
            {
                viewTransform.Init(); // request a new data
            }
            else
            {
                viewTransform.ReceiveData(GetStoredData()); // use stored data
            }
        }

        /**
        * This callback is called when a correct data response is received.
        *
        * @param {any} e Response from fastdata
        */
        private void ReceiveData(object e)
        {
            if (viewTransform.response.ContainsKey("beatTime")){
                beat.Interval = (int)viewTransform.response["beatTime"]*1000;
            }
            if (viewTransform.response.ContainsKey("sessionExpire"))
            {
                sessionExpire = (int)viewTransform.response["sessionExpire"] * 1000;
            }
            if (viewTransform.response.ContainsKey("youboraId"))
            {
                YouboraId = (string)viewTransform.response["youboraId"];
            }
            ping.Interval = (int)viewTransform.response["pingTime"]*1000;
            if (this.GetIsSessionExpired())
            {
                viewTransform.SetSession((string)viewTransform.response["code"]);
                Storage.SetValue("session", viewTransform.response["code"]);
            }
            else
            {
                viewTransform.SetSession(GetSessionId());
            }
        }

        /**
         * Reset all variables and stop all timers
         * @private
         */
        private void Reset()
        {
            StopPings();
            ResourceTransform resourceTransform = new ResourceTransform(this);
            if (adapter != null)
            {
                adapter.flags.Reset();
            }
            isInitiated = false;
            isStarted = false;
            startDelayed = false;
            isAdsManifestSent = false;
            totalPrerollsTime = 0;
            requestBuilder.lastSent["breakNumber"] = 0;
            requestBuilder.lastSent["adNumber"] = 0;
            initChrono.Reset();
            initChrono.StartTime = null;
            savedAdManifest = null;
            savedAdError = null;
            playedPostrolls = 0;
            isBreakStarted = false;
        }

        /**
         * Creates and enqueues related request using {@link Communication#sendRequest}.
         * It will fire will-send-events.
         *
         * @param {string} willSendEvent Name of the will-send event. Use {@link Plugin.Event} enum.
         * @param {string} service Name of the service. Use {@link Service} enum.
         * @param {Object} params Params of the request
         * @private
         */
        private void Send(string willSendEvent, string service, Dictionary<string,object> parameters)
        {
            /*DateTime now = DateTime.Now;
            if (lastEventTime != null)
            {
                DateTime lastEvThreshold = (DateTime) lastEventTime;
                lastEvThreshold.AddMinutes(10);
                if ((DateTime.Compare(now, lastEvThreshold)) > 0)
                {
                    // if last event was sent more than 10 minutes ago, it will use new view code
                    viewTransform.NextView();
                }
            }

            lastEventTime = now;
            if (service == Service.STOP)
            {
                lastEventTime = null;
            }*/

            parameters = requestBuilder.BuildParams(parameters, service);

            if (GetIsLive() == true) // not null!
            {
                parameters["mediaDuration"] = null;
                parameters["playhead"] = null;
            }

            Emit(willSendEvent, parameters);

            if (commInstance != null && parameters != null && (bool) options["enabled"])
            {
                lastServiceSent = service;
                commInstance.SendRequest(new YBRequest(GetHost(), service, parameters), null, null);
            }
        }

        /**
        * Initializes comm and its transforms.
        * @private
        */
        private void InitComm()
        {
            string resource = GetResource();
            if (adapter != null && adapter.GetURLToParse() != null)
            {
                resource = adapter.GetURLToParse();
            }
            resourceTransform.Init(resource);
            commInstance = new Communication(this);
            commInstance.AddTransform(flowTransform);
            commInstance.AddTransform(viewTransform);
            commInstance.AddTransform(resourceTransform);
        }

        /**
         * Returns the current {@link youbora.Communication} instance.
         *
         * @returns {youbora.Communication} communication instance
         */
        public Communication GetComm()
        {
            return commInstance;
        }

        /**
         * Modifies current options. See {@link Options.setOptions}.
         *
         * @param {any} options
         */
        public void SetOptions(Dictionary<string,object> newOptions)
        {
            if (newOptions != null)
            {
                options.SetOptions(newOptions);
            }
        }
        public void SetOptions(object newOptions)
        {
            if (newOptions != null)
            {
                Dictionary<string, object> formattedOptions = JsonConvert.DeserializeObject<Dictionary<string, object>>((string)newOptions);

                options.SetOptions(formattedOptions);
            }
        }

        /**
         * Disable request sending.
         */
        public void Disable()
        {
            SetOptions("{enabled:false}");
        }

        /**
         * Re-enable request sending.
         */
        public void Enable()
        {
            SetOptions("{enabled:true}");
        }
    }
}
