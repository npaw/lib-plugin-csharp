﻿using System.Collections.Generic;
using Youbora.adapter;

namespace Youbora
{
    public class Youbora
    {
        public Dictionary<string, Adapter> Adapters { get; set; } = new Dictionary<string, Adapter>();
        public void RegisterAdapter(string key, Adapter adapter)
        {
            Adapters.Add(key, adapter);
        }

        public void UnregisterAdapter(string key)
        {
            Adapters.Remove(key);
        }
    }
}
