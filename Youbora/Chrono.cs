﻿using System;
using Youbora.log;

namespace Youbora.chrono

{
    /// <summary>
    /// This class calculates time lapses between two points.
    /// </summary>
    public class Chrono
    {
        public DateTime? StartTime { get; set; }
        public DateTime? LastTime { get; set; }

        /// <summary>
        /// Start the chrono.
        /// </summary>
        public void Start()
        {
            StartTime = DateTime.Now;
            LastTime = null;
        }

        /// <summary>
        /// Stop the chrono.
        /// </summary>
        /// <returns>Time lapse in ms.</returns>
        public double Stop()
        {
            LastTime = DateTime.Now;
            return GetDeltaTime();
        }

        /// <summary>
        /// Returns the time between start() and the last stop() in ms. Returns -1 if start wasn't called.
        /// It will force a stop if needed.
        /// </summary>
        /// <returns>Time lapse in ms.</returns>
        public double GetDeltaTime() { return GetDeltaTime(true); }

        /// <summary>
        /// Returns the time between start() and the last stop() in ms. Returns -1 if start wasn't called.
        /// </summary>
        /// <param name="stopIfNeeded">If true, it will force a stop() if it wasn't called before.</param>
        /// <returns>Time lapse in ms.</returns>
        public double GetDeltaTime(bool stopIfNeeded)
        {
            try
            {
                if (StartTime != null)
                {
                    if (LastTime != null)
                    {
                        TimeSpan diff = (DateTime)LastTime - (DateTime)StartTime;
                        return Math.Round(diff.TotalMilliseconds);
                    }
                    else
                    {
                        if (stopIfNeeded)
                        {
                            return Stop();
                        }
                        else
                        {
                            TimeSpan diff = DateTime.Now - (DateTime)StartTime;
                            return Math.Round(diff.TotalMilliseconds);
                        }
                    }
                }
                else
                {
                    return -1;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return -1;
            }
        }

        internal void Reset()
        {
            StartTime = null;
            LastTime = null;
        }
    }
}
