﻿## [6.5.19] - 2023-01-26
### Fixed
- If fire sessionStart after sessionStop, it will ask a new fastdata code for the new session

## [6.5.18] - 2022-01-17
### Removed
- `content.package` option

## [6.5.17] - 2021-10-28
### Fixed
- Player name for ad error events
### Added
- Forced content pause when ads are being played
- Default value for parsedResource if parse.manifest is not used but getURLToParse from the adapter provides a valid url

## [6.5.16] - 2021-08-31
### Fixed
- Small chrono issues
- Requestbuilder failing for some cases

## [6.5.15] - 2021-07-15
### Added
- `ObfuscateIp` for sessions

## [6.5.14] - 2021-03-31
### Added
- Support for `CMF` transport format

## [6.5.13] - 2021-02-11
### Added
- `linkedViewId` option
- 4th parameter for additional parameters on `fireEvent` method

## [6.5.12] - 2021-01-15
### Added
 - Edgecast to the list of CDN we can detect

## [6.5.11] - 2020-12-29
### Added
 - Network and user parameters on session start event

## [6.5.10] - 2020-11-20
### Added
 - `errors.fatal` , `errors.nonFatal` and `errors.ignore` options.

## [6.5.9] - 2020-10-30
### Added
 - Check to not report latency while playing VOD content.

## [6.5.8] - 2020-08-03
### Added
 - Transport format option
 - Transport format detection
 - Playhead on error
 - Hls parser case for partial urls

## [6.5.7] - 2020-04-20
### Added
 - `parentId` for start, init and error when sessions are active

## [6.5.6] - 2020-03-17
### Added
 - `-chsarp` in plugin version for views reported as adapterless
 - Moved constants

## [6.5.5] - 2020-02-25
### Added
 - GetAudioCodec and GetVideoCodec getters in adapter

## [6.5.4] - 2020-01-22
### Added
 - `/init` is now sent before `/error` if no view was created to contain it
### Removed
 - `mediaDuration` parameter from `/joinTime` event

## [6.5.3] - 2020-01-14
### Added
 - `parse.manifest` option, replacing `parse.dash`, `parse.hls` and `parse.locationHeader` 

## [6.5.2] - 2020-01-03
### Updated
- `adPosition` and `breakPosition` parameters replaced by `position`
- Small refactor for breakNumber and adNumber getters
### Added
- Amazon CDN detection
### Removed
- Duplicated osVersion in deviceInfo
### Fixed
- Background issues with session events
- Reported playhead is now integer

## [6.5.1] - 2019-12-13
### Fixed
- Background issues with session events
- Reported playhead is now integer

## [6.5.0] - 2019-11-18
### Added
- SmartAds v3.0
- `parse.dash` option
- `parse.locationHeader` option
- Deprecatedoptions object
- Sessions update
- Simplified init logic
### Removed
- Preload metrics

## [6.0.22] - 2019-11-07
### Fixed
- Checked a case where pings could be sent twice
- Updated the encoding of the requests (strings and jsons)

## [6.0.21] - 2019-09-09
### Fixed
- Timer fix when there is no context
- Additional case for content.bitrate option type (now can parse from all the types that can be casted to int32, 64 and double)

## [6.0.20] - 2019-08-30
### Fixed
- Timer fallback when SynchronizationContext is null

## [6.0.19] - 2019-08-27
### Fixed
- Tryng to workaround SynchronizationContext being null if plugin is set too early.

## [6.0.18] - 2019-08-16
### Fixed
- Issue with pings and `content.bitrate` option not using the right type.

## [6.0.17] - 2019-08-14
### Added
- CreativeId and adProvider parameters with `ad.provider` and `ad.creativeId` options
- Fingerprint updated to the new `deviceUUID` option, and `device.isAnonymous` options
- Updated parse cdn feature with new Akamai pragma headers.

## [6.0.16] - 2019-08-08
### Added
- Device parameters in deviceinfo object

## [6.0.15] - 2019-05-06
### Added
- Replaced host, now is `a-fds.youborafds01.com`

## [6.0.14] - 2019-04-08
### Added
- Fingerprint feature

## [6.0.13] - 2019-04-05
### Added
- `waitForMetadata` and `pendingMetadata` options for delaying start feature

## [6.0.12] - 2019-04-02
### Added
-fireEvent method for adapters to report custom events.
-content.metrics and session.metrics options to report custom metrics in ping and beat events.
-getMetrics method in adapter, to report custom metrics in ping.
-Automatic data request every hour if no views or sessions are active.
-New options: user.email, content.package, content.saga, content.tvShow, content.season, content.episodeTitle, content.channel, content.id, content.imdbId, content.gracenoteId, content.type, content.genre, content.language, content.subtitles, content.contractedResolution, content.cost, content.price, content.playbackType, content.drm, content.encoding.videoCodec, content.encoding.audioCodec, content.encoding.codecSettings, content.enconding.codecProfile, content.encoding.containerFormat
-New option alias: user.type, user.name, user.obfuscateIp, user.anonymousId, app.https, content.customdimension.x instead of customDimension.x
-Deprecated isInfinity

## [6.0.11] - 2019-02-12
### Fixed
- Crash when task has no callback

## [6.0.10] - 2019-02-07
### Added
- `app.name` and `app.releaseVersion` options

## [6.0.9] - 2019-01-30
### Added
- Telefonica CDN Host and type detection

## [6.0.8] - 2019-01-15
### Added
- Lib version in start requests

## [6.0.7] - 2018-12-28
### Added
- Device options
- System sent in all requests

## [6.0.6] - 2018-10-19
### Added
- Smartswitch options: `smartswitch.configCode`, `smartswitch.groupCode` and `smartswitch.contractCode`
### Fixed
- Blocked pause requests while buffering or seeking

## [6.0.5] - 2018-10-16
### Added
- Support for model, osversion and brand device info as options

## [6.0.4] - 2018-09-19
### Added
- Added debug log with response status from nqs