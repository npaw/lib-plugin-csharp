﻿using Youbora.plugin;
using Youbora.plugin.storage;

namespace Youbora.deviceuuid
{
    public class DeviceUUID
    {
        private string key = null;
        private Plugin pluginref = null;

        public DeviceUUID(Plugin plugin)
        {
            pluginref = plugin;
            key = GetPreviousKey();
        }

        public string GetKey()
        {
            if ((bool)pluginref.options["device.isAnonymous"] == true)
            {
                return null;
            }

            if (key == null)
            {
                GenerateKey();
            }
            return key;
        }

        private void GenerateKey()
        {
            key = (string)pluginref.options["deviceUUID"];
            Storage.SetValue("youboraUUID", key);
        }

        private string GetPreviousKey()
        {
            return (string)Storage.GetValue("youboraUUID");
        }
    }
}
