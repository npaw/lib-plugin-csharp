﻿using Youbora.plugin;
using Youbora.util;

namespace Youbora.detectors
{
    public class BackgroundDetector {

        private Plugin pluginInstance = null;
        public bool isInBackground = false;
        private double? lastBeatTime = null;
        private bool wasPlaying = false;
        private bool wasJoined = false;

        public BackgroundDetector(Plugin plugin)
        {
            pluginInstance = plugin;
        }

        //entered background
        public void ToBackground(object e, object args)
        {
            isInBackground = true;
            // video
            if (pluginInstance.GetAdsAdapter() != null)
            {
                pluginInstance.GetAdsAdapter().FireStop();
            }
            if (pluginInstance.GetAdapter() != null)
            {
                wasPlaying = pluginInstance.GetAdapter().flags.isStarted;
                wasJoined = pluginInstance.GetAdapter().flags.isJoined;
                pluginInstance.GetAdapter().FireStop();
            }
            else
            {
                pluginInstance.FireStop();
            }

            // infinity
            lastBeatTime = Util.GetUnixTimestampNow();
            pluginInstance.SendBeat(pluginInstance.beat.Chrono.GetDeltaTime()); //calculate time
            pluginInstance.beat.Stop();
        }


        //leaving background
        public void ToForeground(object e, object args)
        {
            if (isInBackground)
            {
                isInBackground = false;
                // video
                if (pluginInstance.GetAdapter() != null && wasPlaying)
                {
                    pluginInstance.GetAdapter().FireStart();
                }
                if (pluginInstance.GetAdapter() != null && wasJoined)
                {
                    pluginInstance.GetAdapter().FireJoin();
                }

                // infinity
                if (pluginInstance.infinity.infinityStarted && !pluginInstance.infinity.infinityStopped)
                {
                    double now = Util.GetUnixTimestampNow();
                    if (lastBeatTime == null)
                    {
                        lastBeatTime = now;
                    }
                    if (now - lastBeatTime < (int)pluginInstance.sessionExpire)
                    {
                        pluginInstance.SendBeat(now - lastBeatTime);
                        pluginInstance.beat.Start();
                    }
                    else
                    {
                        pluginInstance.infinity.FireSessionStop();
                        pluginInstance.RestartViewTransform();
                        pluginInstance.infinity.Begin();
                    }
                }
            }
        }

    }
}
