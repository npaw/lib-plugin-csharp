﻿using System;
using Youbora.plugin;
using Youbora.util;
using Youbora.comm.request;
using System.Collections.Generic;

namespace Youbora.detectors
{
    public class BlockDetector
    {
        private Plugin pluginInstance = null;
        public bool? isBlocked = null;

        public BlockDetector(Plugin plugin)
        {
            pluginInstance = plugin;
            try
            {
                YBRequest req = new YBRequest("https://a.zxcvads.com/a/ads/-image-ad_ads.html", null, null, new Dictionary<string, object>()
                {
                    ["maxRetries"] = 0
                });
                req.OnSuccess = (response) => {
                    isBlocked = false;
                };
                req.OnFailure = () =>
                {
                    isBlocked = true;
                };
                req.Send();
            }
            catch (Exception err)
            {
                isBlocked = true;
            }
        }
    }
}
