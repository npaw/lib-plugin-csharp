﻿using System;
using System.Collections.Generic;
using Youbora.util;
using Youbora.log;
using Youbora.comm.request;
using Youbora.comm.transform;
using Youbora.plugin;

namespace Youbora.comm.communication
{
    public class Communication
    {
        public List<Transform> transforms = new List<Transform>();
        public Queue<YBRequest> requests = new Queue<YBRequest>();
        private Plugin pluginRef = null;

        public Communication(Plugin plugin) {
            pluginRef = plugin;
        }

        public void SendRequest(YBRequest request, Action callback, Dictionary<string,object> callbackParams)
        {
            if (request != null)
            {
                request.On(YBRequest.Event.SUCCESS, callback);
                RegisterRequest(request);
            }
        }

        public void BuildRequest(string host, string service, Dictionary<string, object> parameters, Action callback)
        {
            YBRequest request = new YBRequest(host, service,parameters);
            request.On(YBRequest.Event.SUCCESS, callback);
            ProcessRequests();
        }

        public void AddTransform(Transform transform)
        {
            if (transform == null) return;
            transforms.Add(transform);
            transform.On(Transform.Event.DONE, ProcessRequests);
        }

        public void RemoveTransform(Transform transform)
        {
            if (transforms.Contains(transform))
            {
                transforms.Remove(transform);
            }
            else
            {
                Log.Warn("Trying to remove unexisting transform");
            }
        }

        private int ToTransform(YBRequest request)
        {
            foreach (Transform transform in transforms)
            {
                if (transform.IsBlocking(request))
                {
                    return Transform.STATE_BLOCKED;
                }
                else
                {
                    transform.Parse(request);
                }

                if (transform.GetState() == Transform.STATE_OFFLINE)
                {
                    return Transform.STATE_OFFLINE;
                }
            }
            return Transform.STATE_NO_BLOCKED;

        }

        private void RegisterRequest(YBRequest request)
        {
            // Add time mark
            request.SetParam("timemark", Util.GetUnixTimestampNow().ToString());
            requests.Enqueue(request);
            ProcessRequests();
        }

        private void ProcessRequests()
        {
            Queue<YBRequest> auxQueue = new Queue<YBRequest>();
            while(requests.Count > 0)
            {
                YBRequest request = requests.Dequeue();
                int actualState = ToTransform(request);
                if(actualState == Transform.STATE_NO_BLOCKED)
                {
                    request.Host = pluginRef.GetHost();
                    request.Send();
                }
                else if (actualState == Transform.STATE_BLOCKED)
                {
                    auxQueue.Enqueue(request);
                }
            }
            requests = auxQueue;
        }

    }

}
