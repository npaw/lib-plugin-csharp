﻿using System.Collections.Generic;
using Youbora.constants;
using Youbora.comm.transform.resourceParsers.formatParsers.parser;
using Youbora.comm.transform.resourceParsers.formatParsers.hlsParser;
using Youbora.comm.transform.resourceParsers.formatParsers.dashParser;
using Youbora.comm.transform.resourceParsers.formatParsers.locationHeaderParser;
using Youbora.plugin;
using Youbora.comm.transform.resourceparsers.cdnparsers.cdnconfig;
using System.Net.Http;
using Youbora.comm.request;

namespace Youbora.comm.transform.resourceTransform
{
    public class ResourceTransform : Transform
    {
        private Plugin plugin = null;
        private string realResource = null;
        private string initResource = null;
        private string transportFormat = null;
        private string cdnName = null;
        private string cdnNodeHost = null;
        private string cdnNodeTypeString = null;
        private int? cdnNodeType = null;
        private Dictionary<string,string> responses = new Dictionary<string, string>();
        private bool parseManifestEnabled = false;
        private bool cdnEnabled = false;
        private string[] cdnList = null;
        private string cdnNameHeader = null;
        /**
         * This class parses resource to fetch HLS transportstreams and CDN-related info.
         *
         * @constructs ResourceTransform
         * @extends youbora.Transform
         * @memberof youbora
         *
         * @param {Plugin} plugin Instance of {@link Plugin}
         */
        public ResourceTransform(Plugin plugin)
        {
            this.plugin = plugin;

            realResource = null;
            initResource = null;
            transportFormat = null;
            cdnName = null;
            cdnNodeHost = null;
            cdnNodeTypeString = null;
            cdnNodeType = null;
            responses = null;
            isBusy = false;

            transformName = "Resource";
        }

        /**
         * Get the resource. If the transform is done, the real (parsed) resource will be returned
         * Otherwise the initial one is returned.
         *
         * @return {string} The initial or parsed resource
         */
        public string GetResource()
        {
            if (realResource != null)
            {
                return realResource;
            }
            if (plugin.options["content.resource"] != null)
            {
                return (string)plugin.options["content.resource"];
            }
            return initResource;
        }

        /**
         * Get the transport format. If the transform detected the chunk format it will be returned, if not, null.
         *
         * @return {string} The transport format
         */
        public string GetTransportFormat()
        {
            return transportFormat;
        }

        /**
         * Get the parsed CDN name.
         *
         * @return {string} The CDN name or null if unknown
         */
        public string GetCdnName()
        {
            return cdnName;
        }

        /**
         * Get the parsed CDN node.
         *
         * @return {string} The CDN node or null if unknown
         */
        public string GetNodeHost()
        {
            return cdnNodeHost;
        }

        /**
         * Get the parsed CDN type string, as returned in the cdn header response.
         *
         * @return {string} The CDN type string
         */
        public string GetNodeTypeString()
        {
            return cdnNodeTypeString;

        }

        /**
         * Get the parsed CDN type, parsed from the type string.
         *
         * @return {string} The CDN type
         */
        public int? GetNodeType()
        {
            return cdnNodeType;
        }

        /**
         * Start the execution. Can be called more than once. If already running, it will be ignored,
         * if ended it will restart.
         * @param {string} resource the original resource
         */
        public void Init(string resource)
        {
            if (!isBusy)
            {
                isBusy = true;
                initResource = resource;
                parseManifestEnabled = plugin.IsParseManifest();
                CdnParser cdnParser = new CdnParser();
                cdnEnabled = plugin.IsParseCdnNode();
                cdnList = (string[]) plugin.GetParseCdnNodeList().Clone();
                cdnNameHeader = plugin.GetParseCdnNodeNameHeader();
                cdnParser.SetBalancerHeaderName(cdnNameHeader); 

                if (parseManifestEnabled) {
                    if (!IsFinalUrl(initResource))
                    {
                        ParseManifest(null, initResource);
                    }
                    else
                    {
                        realResource = initResource;
                        ParseCDN();
                    }
                }
                else 
                {
                    ParseCDN();
                }

            }
        }

        public new void Done()
        {
            base.Done();
        }
        
        public void ParseManifest(HttpResponseMessage lastManifest, string lastSrc)
        {
            List<Parser> parserArray = new List<Parser>(){new LocationHeaderParser(), new DashParser(), new HlsParser()};
            string src = lastSrc;
            if(src == null)
            {
                src = initResource;
            }
            ParseManifest(parserArray, lastManifest, src, null);
        }

        private async void ParseManifest(List<Parser> parserArray, HttpResponseMessage lastManifest, string lastSrc, string format)
        {
            if (parserArray.Count > 0)
            {
                string manifest = null;
                if (lastManifest != null)
                {
                    manifest = await lastManifest.Content.ReadAsStringAsync();
                }
                Parser parser = parserArray[0];
                if (parser.ShouldExecute(manifest))
                {
                    parser.On(Parser.Event.DONE, () =>
                    {
                        parserArray.RemoveAt(0);
                        string f = format;
                        if (f == null)
                        {
                            f = parser.GetTransportFormat();
                        }
                        ParseManifest(parserArray, parser.GetLastManifest(), parser.GetResource(), f);
                    });
                    parser.Parse(lastSrc, lastManifest);
                }
                else
                {
                    parserArray.RemoveAt(0);
                    ParseManifest(parserArray, lastManifest, lastSrc, format);
                }
            }
            else
            {
                transportFormat = format;
                realResource = lastSrc;
                ParseCDN();
            }
        }

        private void ParseCDN()
        {
            int counter = 0;
            while (cdnEnabled && cdnList.Length > counter)
            {
                string cdn = cdnList[counter];

                if (GetNodeHost() != null) return;// abort

                CdnParser cdnParser = CdnParser.Create(cdn);
      
                if (cdnParser != null)
                {
                    cdnParser.On(CdnParser.Event.DONE, () =>
                    {
                        responses = (Dictionary<string, string>) cdnParser.GetResponses();
                        cdnName = cdnParser.GetParsedCdnName();
                        cdnNodeHost = cdnParser.GetParsedNodeHost();
                        cdnNodeTypeString = cdnParser.GetParsedNodeTypeString();
                        cdnNodeType = cdnParser.GetParsedNodeType();

                        //if (GetNodeHost() != null)
                        //{
                            Done();
                        /*}
                        else
                        {
                            ParseCDN();
                        }*/
                    });

                  // Run parse
                  cdnParser.Parse(GetResource(), responses);
                }
                else
                {
                    ParseCDN();
                }
                ++counter;
            }

            Done();
        }

        /**
         * Replaces the resource and/or Cdn info for the /start service.
         *
         * @param {YBRequest} request YBRequest to transform.
         */
        public override void Parse(YBRequest request)
        {
            if (request.Service == Service.START)
            {
                Dictionary<string, object> lastSent = plugin.requestBuilder.lastSent;
                if (request.Params.ContainsKey("parsedResource"))
                {
                    lastSent["parsedResource"] = request.Params["parsedResource"];
                    lastSent["transportFormat"] = request.Params["transportFormat"] = GetTransportFormat();
                }
                if (cdnEnabled)
                {
                    if (!request.Params.ContainsKey("cdn")) request.Params["cdn"] = null;
                    string cdn = (string) request.Params["cdn"];
                    if (cdn == null|| cdn == "") cdn = GetCdnName();
                    lastSent["cdn"] = request.Params["cdn"] = cdn;

                    lastSent["nodeHost"] = request.Params["nodeHost"] = GetNodeHost();
                    lastSent["nodeType"] = request.Params["nodeType"] = GetNodeType();
                    lastSent["nodeTypeString"] = request.Params["nodeTypeString"] = GetNodeTypeString();
                }
            }
        }

        private bool IsFinalUrl(string url)
        {
            List<string> segmentExtensions = new List<string>() { ".ts", ".mp4", ".m4s", ".cmfv" };
            for (int i = 0; i<segmentExtensions.Count; ++i)
            {
                string ext = segmentExtensions[i];
                if (url.EndsWith(ext))
                {
                    return true;
                }
            }
            return false;
        }
    }
}
