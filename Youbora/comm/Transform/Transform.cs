﻿using Youbora.comm.request;
using Youbora.emitter;

namespace Youbora.comm.transform
{
    public abstract class Transform : Emitter
    {
        public bool isBusy = true;
        public bool sendRequest = true;
        public string transformName = "Transform";
        public static int STATE_OFFLINE = 2;
        public static int STATE_BLOCKED = 1;
        public static int STATE_NO_BLOCKED = 0;
        /**
         * List of events that could be fired from this class.
         * @enum
         */
        public static class Event
        {
            /** Notifies that this Transform is done processing. */
            public static string DONE = "done";
        }
        /**
         * Transform classes in YOUBORA help the library parse and work with data.
         *
         * A Transform makes some kind of task that may block requests until it's done, or applies changes
         * to the requests right before they're finally sent.
         *
         * {@link ResourceTransform}, {@link ViewTransform}... all extend from this class.
         *
         * @constructs Transform
         * @extends youbora.Emitter
         * @memberof youbora
         * @abstract
         */
        public Transform()
        {
            /**
             * Whether the Transform is currently working or not.
             * @private
             */
            isBusy = true;
            sendRequest = true;
            transformName = "Transform";
        }

        /**
         * Override this method to transform info.
         *
         * @param {YBRequest} request Request to transform.
         */
        public abstract void Parse(YBRequest request);

        /**
         * By default this will return true until {@link #done()} is called. This can be overridden
         * in order to block {@link Request}s based on any criteria. For instance its
         * {@link Request#getService()}.
         *
         * @param {YBRequest} request Request to transform.
         * @return {bool} True if queue shall be blocked.
         */
        public virtual bool IsBlocking(YBRequest request) {
            return IsBlocking();
        }
        public bool IsBlocking()
        {
            return isBusy;
        }

        /**
         * Emits DONE event
         */
        public void Done()
        {
            isBusy = false;
            Emit(Event.DONE, null);
        }

        // offline
        public bool HasToSend(YBRequest request)
        {
            return sendRequest;
        }

        public int GetState()
        {
            if (!sendRequest)
            {
                return STATE_OFFLINE;
            }
            if (isBusy)
            {
                return STATE_BLOCKED;
            }
            return STATE_NO_BLOCKED;
        }

    }
}
