﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Youbora.constants;
using Youbora.log;
using Youbora.util;
using Youbora.comm.request;
using Youbora.plugin;
using Newtonsoft.Json;
using System.Net.Http;

namespace Youbora.comm.transform.viewTransform
{
    public class ViewTransform : Transform
    {
        private Plugin plugin = null;
        private bool httpSecure = false;
        private string session = null;
        private string viewIndex = null;
        public Dictionary<string, object> response = new Dictionary<string, object>();

        public ViewTransform(Plugin plugin, string session)
        {
            viewIndex = Util.GetUnixTimestampNow().ToString();
            this.session = session;
            httpSecure = (bool)plugin.options["app.https"];
            this.plugin = plugin;
            transformName = "View";
        }

        public void Init()
        {
            try
            {
                string service = Service.DATA;
                Dictionary<string, object> parameters = new Dictionary<string, object>()
                {
                    ["outputformat"] = "json"
                };

                // if (plugin.options.offline) ...

                parameters = plugin.requestBuilder.BuildParams(parameters, service);
                Log.Notice("{0} {1}", service, parameters["system"]);
                if ((string)parameters["system"] == "nicetest")
                {
                    // "nicetest" is the default accountCode. If found here, it's very likely that the customer has forgotten to set it.
                    Log.Error("No accountCode has been set. Please set your accountCode inside plugins options.");
                }

                YBRequest dataReq = new YBRequest(plugin.GetHost(), service, parameters);
                dataReq.OnSuccess += async (req) =>
                {
                    await ReceiveData(req);
                };
                dataReq.OnFailure += () =>
                {
                    FailedData(null);
                };
                dataReq.Send();
            } catch (Exception e)
            {
                Log.Error("EXC {0}", e.ToString());
            }

        }

        public async Task ReceiveData(object req)
        {
            try
            {
                string json = await ((HttpResponseMessage)req).Content.ReadAsStringAsync();
                response["msg"] = json;
                var obj = JsonConvert.DeserializeObject<Dictionary<string, Dictionary<string, object>>>(json);
                if (obj.ContainsKey("q"))
                {

                    if (obj["q"].ContainsKey("h"))
                    {
                        response["host"] = (string)obj["q"]["h"];
                    }

                    if (obj["q"].ContainsKey("pt"))
                    {
                        response["pingTime"] = int.Parse((string)obj["q"]["pt"]);
                    }
                    if (obj["q"].ContainsKey("i"))
                    {
                        var obj2 = JsonConvert.DeserializeObject<Dictionary<string,string>>(obj["q"]["i"].ToString());
                        if (obj2.ContainsKey("bt"))
                        {
                            response["beatTime"] = int.Parse(obj2["bt"]);
                        }
                        if (obj2.ContainsKey("exp"))
                        {
                            response["sessionExpire"] = int.Parse(obj2["exp"]);
                        }
                    }
                    if (obj["q"].ContainsKey("f"))
                    {
                        var obj3 = JsonConvert.DeserializeObject<Dictionary<string, string>>(obj["q"]["f"].ToString());
                        if (obj3.ContainsKey("yid"))
                        {
                            response["youboraId"] = obj3["yid"];
                        }
                    }
                    if (obj["q"].ContainsKey("c"))
                    {
                        response["code"] = obj["q"]["c"];
                    }
                }
                else
                {
                    Log.Error("Error: FastData response is wrong.");
                }
                Log.Notice("FastData '{0}' is ready.", (string)response["code"]);
                Done();
            }
            catch (Exception ex)
            {
                Log.Error("Error: FastData response is wrong: {0}", ex.ToString());
            }
        }

        private void FailedData(object req)
        {
            Log.Error("Fastdata request has failed.");
        }

        public string NextView()
        {
            viewIndex = Util.GetUnixTimestampNow().ToString();
            return GetViewCode();
        }

        public string GetViewCode()
        {
            if (response.ContainsKey("code"))
            {
                return response["code"] + "_" + viewIndex;
            }
            return null;
        }

        public string GetSession()
        {
            return session;
        }

        public void SetSession(string session)
        {
            this.session = session;
        }

        public override void Parse(YBRequest request)
        {
            if (request.Host == null || request.Host == "")
            {
                request.Host = (string) response["host"];
            }
            if (plugin.GetAccountCode() != null)
            {
                request.Params["system"] = plugin.GetAccountCode();
            }
            if (GetSession() == null)
            {
                SetSession((string)response["code"]);
            }
            request.Params["sessionRoot"] = GetSession();

            if (request.Service == Service.PING || request.Service == Service.START)
            {
                if (!request.Params.ContainsKey("pingTime") || request.Params["pingTime"] == null || request.Params["pingTime"].ToString() == "")
                {
                    request.Params["pingTime"] = (int) response["pingTime"];
                }
            }

            if (request.Service != Service.NAV &&
              request.Service != Service.SESSION_START &&
              request.Service != Service.SESSION_STOP &&
              request.Service != Service.EVENT &&
              request.Service != Service.BEAT)
            {
                request.Params["code"] = GetViewCode();
            }
            else
            {
                request.Params["sessionId"] = GetSession();
            }

            if ( // Creating view event
              request.Service == Service.START ||
              request.Service == Service.INIT ||
              request.Service == Service.ERROR)
            {
                if (plugin.infinity.infinityStarted)
                {
                    request.Params["parentId"] = GetSession();
                    request.Params["navContext"] = plugin.GetContext();                
                }
            }
        }
    }
}
