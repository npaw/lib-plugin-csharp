﻿using System;
using Youbora.log;
using Youbora.comm.request;
using System.Collections.Generic;
using System.Net.Http;
using Youbora.comm.transform.resourceParsers.formatParsers.parser;

namespace Youbora.comm.transform.resourceParsers.formatParsers.locationHeaderParser
{
    public class LocationHeaderParser : Parser
    {
        public override void Parse(string resource, HttpResponseMessage manifest)
        {
            this.realResource = resource;
            if (manifest == null)
            {
                try
                {
                    var request = new YBRequest(resource, null, null, new Dictionary<string, object>()
                    {
                        ["cache"] = true
                    });
                    request.OnSuccess = (response) =>
                    {
                        ParseWithManifest(response);
                    };
                    request.OnFailure = () =>
                    {
                        Done();
                    };
                    request.Send();
                }
                catch (Exception ex)
                {
                    Log.Exception(ex);
                    Done();
                }
            }
            else
            {
                ParseWithManifest(manifest);
            }
        }


        private void ParseWithManifest(HttpResponseMessage response)
        {
            this.lastManifest = response;
            Uri location = response.Headers.Location;
            if (location != null)
            {
                this.realResource = location.ToString();
                Parse(realResource, null);
            }
            else
            {
                Done();
            }
        }


        public override bool ShouldExecute(string lastManifestStr)
        {
            return true;
        }
    }
}
