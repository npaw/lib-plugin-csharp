﻿using System;
using Youbora.log;
using Youbora.comm.request;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using Youbora.comm.transform.resourceParsers.formatParsers.parser;
using Youbora.constants;

namespace Youbora.comm.transform.resourceParsers.formatParsers.dashParser
{
    public class DashParser : Parser
    {
        public override void Parse(string resource, HttpResponseMessage manifest)
        {
            if (manifest == null)
            {
                try
                {
                    var request = new YBRequest(resource, null, null, new Dictionary<string, object>() { ["cache"] = true });
                    request.OnSuccess = async (response) =>
                    {
                        ParseLocation(response, resource);
                    };
                    request.OnFailure = () =>
                    {
                        Done();
                    };
                    request.Send();

                }
                catch (Exception ex)
                {
                    Log.Exception(ex);
                    Done();
                }
            }
            else
            {
                ParseLocation(manifest, resource);
            }
        }

        private async void ParseLocation(HttpResponseMessage lastManifest, string resource)
        {
            string manifest = await lastManifest.Content.ReadAsStringAsync();
            try
            {
                this.lastManifest = lastManifest;
                var match = new Regex(@".*<Location>(.+)<\/Location>.*", RegexOptions.IgnoreCase).Match(manifest);
                if (match.Success)
                {
                    Parse(match.Groups[1].Value, null);
                }
                else
                {
                    ParseFinalResource(manifest, resource);
                }
            }
            catch (Exception ex)
            {
                Log.Warn("Dash parse failed");
                Done();
            }
        }

        private void ParseFinalResource(string manifest, string resource)
        {
            try
            {
                var baseUrlMatches = new Regex(@"[\s\S]*<BaseURL>(.+)<\/BaseURL>[\s\S]*", RegexOptions.IgnoreCase).Match(manifest);
                var segmentUrlMatches = new Regex(@"[\s\S]*<SegmentURL[\s\S]*media=(.+)[\s\S]*", RegexOptions.IgnoreCase).Match(manifest);
                var segmentTemplateMatches = new Regex(@"[\s\S]*<SegmentTemplate[\s\S]*media=(.+)[\s\S]*", RegexOptions.IgnoreCase).Match(manifest);
                GetManifestMetadata(manifest);
                if (baseUrlMatches.Success && IsFullUrl(baseUrlMatches.Groups[1].Value))
                {
                    this.realResource = baseUrlMatches.Groups[1].Value.Replace("\"", "");
                }
                else if (segmentUrlMatches.Success && IsFullUrl(segmentUrlMatches.Groups[1].Value))
                {
                    this.realResource = segmentUrlMatches.Groups[1].Value.Replace("\"", "");
                }
                else if (segmentTemplateMatches.Success && IsFullUrl(segmentTemplateMatches.Groups[1].Value))
                {
                    this.realResource = segmentTemplateMatches.Groups[1].Value.Replace("\"", "");
                }
                else
                {
                    this.realResource = resource;
                }
            }
            catch (Exception err)
            {
                Log.Warn("Dash manifest parse failed");
            }
            Done();
        }

        private void GetManifestMetadata(string manifest)
        {
            try
            {
                var matches = new Regex(@"[\s\S]*<AdaptationSet[\s\S]*mimeType=\u0022video\/([^\u0022]+)[\s\S]*", RegexOptions.IgnoreCase).Match(manifest);
                string ret = null;
                switch (matches.Groups[1].Value) {
                    case "mp4":
                    case "m4s":
                        ret = TransportFormat.MP4;
                        break;
                    case "cmfv":
                        ret = TransportFormat.CMF;
                        break;
                    case "mp2t":
                        ret = TransportFormat.MPEG2;
                        break;
                }
                transportFormat = ret;
            } 
            catch (Exception err) 
            {
                Log.Warn("Couldnt find the transport format");
            }
        }

        private bool IsFullUrl(string url)
        {
            return url.Contains("http");
        }

        public override bool ShouldExecute(string lastManifestStr)
        {
            bool ret = true;
            if (lastManifestStr != null)
            {
                return lastManifestStr.Contains("<MPD");
            }
            return ret;
        }
    }
}
