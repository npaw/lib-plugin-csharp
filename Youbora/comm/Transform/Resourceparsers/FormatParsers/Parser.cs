﻿using Youbora.emitter;
using System.Net.Http;

namespace Youbora.comm.transform.resourceParsers.formatParsers.parser
{
    public abstract class Parser : Emitter
    {
        protected string realResource;
        protected string transportFormat;
        protected HttpResponseMessage lastManifest;

        public static class Event
        {
            public static string DONE = "done";
        }

        public Parser()
        {
            realResource = null;
            transportFormat = null;
            lastManifest = null;
        }

        protected void Done()
        {
            Emit(Event.DONE, null);
        }

        public abstract void Parse(string resource, HttpResponseMessage lastManifest);

        public string GetResource()
        {
            return realResource;
        }

        public string GetTransportFormat()
        {
            return transportFormat;
        }

        public HttpResponseMessage GetLastManifest()
        {
            return lastManifest;
        }

        public abstract bool ShouldExecute(string manifest);
    }
}
