﻿using System;
using Youbora.log;
using Youbora.comm.request;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Net.Http;
using Youbora.comm.transform.resourceParsers.formatParsers.parser;
using Youbora.constants;

namespace Youbora.comm.transform.resourceParsers.formatParsers.hlsParser
{
    public class HlsParser : Parser
    {
        public override void Parse(string resource, HttpResponseMessage manifest)
        {
            Parse(resource, manifest, null);
        }

        public void Parse(string resource, string parentResource)
        {
            Parse(resource, null, parentResource);
        }

        public void Parse(string resource, HttpResponseMessage manifest, string parentResource)
        {
            try
            {
                //get first line ending in .m3u8, .m3u, .mp4 or .ts
                var match = new Regex(@"(\S*?(\.m3u8|\.m3u|\.ts|\.mp4|\.cmfv)(\?\S*|\n|\r|$))", RegexOptions.IgnoreCase).Match(resource);
                if (match.Success)
                {
                    string prevParent = "";
                    var res = match.Groups[1].Value;
                    int index = parentResource.LastIndexOf("/");
                    if (!res.StartsWith("http") && index >= 0)
                    {
                        // If does not start with http, add parentResource relative route.
                        var a = res.ToCharArray();
                        if (a[0] == '/' && a[1] != '/')
                        {
                            // if its a relative route not using the same path, but the same domain
                            int ind = parentResource.IndexOf('/') + 1;
                            ind += parentResource.Substring(ind, parentResource.Length).IndexOf('/') + 1; // Second /
                            ind += parentResource.Substring(ind, parentResource.Length).IndexOf('/'); // Third /
                            res = parentResource.Substring(0, ind) + res;
                        } 
                        else
                        {
                           // if its sharing the(omitted) path
                           res = parentResource.Substring(0, index) + "/" + res;
                        }
                        // Does not start with http add parentResource relative route.
                        prevParent = parentResource.Substring(0, parentResource.LastIndexOf("/")) + "/";
                    }

                    if (match.Groups[2].Value == ".m3u8" || match.Groups[2].Value == ".m3u")
                    {
                        if (manifest == null)
                        {
                            // It is m3u8 or m3u...
                            var request = new YBRequest(res, null, null, new Dictionary<string, object>() { ["cache"] = true });
                            request.OnSuccess = async (response) =>
                            {
                                ParseWithManifest(response, prevParent, match);
                            };
                            request.OnFailure = () =>
                            {
                                Done();
                            };

                            request.Send();
                        }
                        else
                        {
                            ParseWithManifest(manifest, prevParent, match);
                        }
                    }
                    else
                    {
                        // It is mp4, m4s or ts...
                        switch (match.Groups[2].Value)
                        {
                            case ".mp4":
                            case ".m4s":
                                transportFormat = TransportFormat.MP4;
                                break;
                            case ".cmfv":
                                transportFormat = TransportFormat.CMF;
                                break;
                            case ".ts":
                                transportFormat = TransportFormat.MPEG2;
                                break;
                        }
                        this.realResource = res;
                        Done();
                    }
                }
                else
                {
                    Done();
                }

            }
            catch (Exception ex)
            {
                Log.Exception(ex);
                Done();
            }
        }

        private async void ParseWithManifest(HttpResponseMessage manifest, string parentResource,Match match)
        {
            this.lastManifest = manifest;
            Parse(await manifest.Content.ReadAsStringAsync(), null, parentResource + match.Value);
        }

        public override bool ShouldExecute(string lastManifestStr)
        {
            bool ret = true;
            if (lastManifestStr != null)
            {
                return lastManifestStr.Contains("#EXTM3U");
            }
            return ret;
        }
    }
}
