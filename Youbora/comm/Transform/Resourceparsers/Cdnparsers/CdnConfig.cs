﻿using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Net.Http;

namespace Youbora.comm.transform.resourceparsers.cdnparsers
{
    /// <summary>
    /// CDN parser config class.
    /// </summary>
    public class CdnConfig
    {
        /// <summary>
        /// Active parsers.
        /// </summary>
        public List<CdnParsers> Parsers { get; set; }

        /// <summary>
        /// Special headers to add to the request.
        /// </summary>
        public Dictionary<string, string> requestHeaders { get; set; }

        public string CdnName;

        public HttpMethod requestMethod;

        public class CdnParsers
        {
            public CdnParserType Type { get; set; }
            public string Name { get; set; }
            public Regex Regex;
        }

        /// <summary>
        /// What to parse.
        /// </summary>
        public enum CdnParserType { Host, Type, HostAndType, TypeAndHost , Name};
    }
}