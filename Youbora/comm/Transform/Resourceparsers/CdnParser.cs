﻿using System;
using System.Collections.Generic;
using Youbora.emitter;
using Youbora.log;
using Youbora.comm.request;
using System.Net.Http;
using System.Text.RegularExpressions;

namespace Youbora.comm.transform.resourceparsers.cdnparsers.cdnconfig
{
    public class CdnParser: Emitter
    {
        public class CdnParserOptions
        {
            public string cdnName = null;
            public List<CdnConfig.CdnParsers> parsers = new List<CdnConfig.CdnParsers>();
            public Dictionary<string,string> requestHeaders = new Dictionary<string, string>();
            public int parseType = 0;
            public HttpMethod requestMethod = HttpMethod.Head;
            public int ParseType(string val)
            {
                return ParseType();
            }
            public int ParseType()
            {
                return parseType;
            }
        }
        private CdnParserOptions options = new CdnParserOptions();
        private string cdnName = null;
        private string cdnNodeHost = null;
        private string cdnNodeTypeString = null;
        private int? cdnNodeType = null;
        private Dictionary<string,string> responses = new Dictionary<string,string>();

        public CdnParser()
        {

        }

        private CdnParser(CdnParserOptions options)
        {
            this.options.cdnName = options.cdnName;
            this.options.parsers = options.parsers;
            this.options.requestHeaders = options.requestHeaders;
            this.options.parseType = options.parseType;
            this.options.requestMethod = options.requestMethod;
        }

        public static CdnParser Create(string key)
        {
            if (CdnParser.CdnsAvailable.ContainsKey(key))
            {
                var options = new CdnParserOptions();
                options.parsers = CdnParser.CdnsAvailable[key].Parsers;
                options.cdnName = key;
                options.requestHeaders = CdnParser.CdnsAvailable[key].requestHeaders;
                if  (CdnParser.CdnsAvailable[key].requestMethod != null)
                {
                    options.requestMethod = CdnParser.CdnsAvailable[key].requestMethod;
                }
                else
                {
                    options.requestMethod = HttpMethod.Head;
                }
                options.parseType = 0;
                return new CdnParser(options);  
            }
            Log.Warn("Tried to create an unexisting CdnParser named {0}", key);
            return null;
        }

        public void Done()
        {
            Emit(Event.DONE,null);
        }

        public CdnParser AddParser(CdnConfig.CdnParsers parser)
        {
            this.options.parsers.Add(parser);
            return this;
        }

        public CdnParser SetCdnName (string name)
        {
            this.options.cdnName = name;
            return this;
        }

        public CdnParser SetRequestHeader(string key, string value)
        {
            this.options.requestHeaders[key] = value;
            return this;
        }

       public CdnParser SetParseType( int parser)
        {
            this.options.parseType = parser;
            return this;
        }

        public string GetParsedCdnName()
        {
            return cdnName;
        }

        public string GetParsedNodeHost()
        {
            return cdnNodeHost;
        }
        public string GetParsedNodeTypeString()
        {
            return cdnNodeTypeString;
        }

        public int? GetParsedNodeType()
        {
            return cdnNodeType;
        }

        public object GetResponses()
        {
            return responses;
        }
         public void Parse(string url, Dictionary<string,string> responses)
         {

            string headerString = "{}";
            if (this.options.requestHeaders != null)
            {
                headerString = this.options.requestHeaders.ToString();
            }
            if (responses != null && responses.ContainsKey(headerString) && responses[headerString] != null)
            {
                ParseResponse(responses[headerString]);
             }
             else
             {
                RequestResponse(url);
             }
         }

        private void RequestResponse(string url)
        {
            string headerString = "";
            if (this.options.requestHeaders != null)
            {
                headerString = this.options.requestHeaders.ToString();
            }
            YBRequest request = new YBRequest(url, null, null, new Dictionary<string, object>() {
                ["method"] = this.options.requestMethod,
                ["maxRetries"] = 0,
                ["requestHeaders"] = this.options.requestHeaders
            });

            request.OnSuccess += (resp) => {
                string response = resp.Headers.ToString();
                responses.Add("headerString",response);
                ParseResponse(response);
            };

            request.OnFailure += () =>
            {
                Done();
            };

           request.Send();
        }

        public void ParseResponse(string headers)
        {
            foreach (var parser in options.parsers)
            {
                foreach (var line in headers.Split('\n'))
                {
                    int index = line.IndexOf(":");
                    if (index != -1)
                    {
                        string key = line.Substring(0, index);
                        if (key == parser.Name)
                        {
                            ExecuteParser(parser, line.Substring(index + 1));
                        }
                    }
                }
            }
            Done();
        }

        public void SetBalancerHeaderName(string name)
        {
            CdnsAvailable["Balancer"].Parsers[0].Name = name;
        }

        private void ExecuteParser( CdnConfig.CdnParsers parser, string value)
        {
            try
            {
                var match = parser.Regex.Match(value);
                if (match != null)
                {
                    if (this.options.cdnName != null) cdnName = options.cdnName;
                    switch (parser.Type)
                    {
                        case CdnConfig.CdnParserType.Host:
                             cdnNodeHost = match.Groups[1].Value;
                             break;
                        case CdnConfig.CdnParserType.Type:
                            cdnNodeTypeString = match.Groups[1].Value;
                            cdnNodeType = options.ParseType(cdnNodeTypeString);
                            break;
                        case CdnConfig.CdnParserType.HostAndType:
                            cdnNodeHost = match.Groups[1].Value;
                            cdnNodeTypeString = match.Groups[2].Value;
                            cdnNodeType = options.ParseType(cdnNodeTypeString);
                            break;
                        case CdnConfig.CdnParserType.TypeAndHost:
                            cdnNodeTypeString = match.Groups[1].Value;
                            cdnNodeType = options.ParseType(cdnNodeTypeString);
                            cdnNodeHost = match.Groups[2].Value;
                            break;
                        case CdnConfig.CdnParserType.Name:
                            cdnName = match.Groups[1].Value.ToUpper();
                            break;
                    }
                }
            }
            catch (Exception err)
            {
                Log.Warn("CDN parsing for {0} could not parse header value {1}, {2}", this.options.cdnName, value, err.ToString());
            }
        }

        public static class Event
        {
            public static string DONE = "done";
        }
        public static class ElementType
        {
            public static string HOST = "host";
            public static string TYPE = "type";
            public static string HOST_AND_TYPE = "host+type";
            public static string TYPE_AND_HOST = "type+host";
            public static string NAME = "name";
        }
        public static Dictionary<string, CdnConfig> CdnsAvailable { get; set; } = new Dictionary<string, CdnConfig>()
        {
            ["Amazon"] = new CdnConfig()
            {
                CdnName = "AMAZON",
                Parsers = new List<CdnConfig.CdnParsers>()
                {
                    new CdnConfig.CdnParsers() {
                        Name = "x-amz-cf-pop",
                        Type = CdnConfig.CdnParserType.Host,
                        Regex = new Regex(@"(.+)")
                    },
                    new CdnConfig.CdnParsers() {
                        Name = "x-cache",
                        Type = CdnConfig.CdnParserType.Type,
                        Regex = new Regex(@"(.+)\sfrom.+")
                    }
                },
            },
            ["Telefonica"] = new CdnConfig()
            {
                CdnName = "TELEFO",
                Parsers = new List<CdnConfig.CdnParsers>()
                {
                    new CdnConfig.CdnParsers() {
                        Name = "x-tcdn",
                        Type = CdnConfig.CdnParserType.HostAndType,
                        Regex = new Regex(@"Host:(.+)\sType:(.+)")
                    }
                },
                requestHeaders = new Dictionary<string, string>()
                {
                    ["x-tcdn"] = "host"
                }
            },
            ["Level3"] = new CdnConfig()
            {
                CdnName = "LEVEL3",
                Parsers = new List<CdnConfig.CdnParsers>()
                {
                    new CdnConfig.CdnParsers() {
                        Name = "X-WR-DIAG",
                        Type = CdnConfig.CdnParserType.HostAndType,
                        Regex = new Regex(@"Host:(.+)\sType:(.+)")
                    }
                },
                requestHeaders = new Dictionary<string, string>()
                {
                    ["X-WR-DIAG"] = "host"
                }
            },
            ["Akamai"] = new CdnConfig()
            {
                CdnName = "AKAMAI",
                Parsers = new List<CdnConfig.CdnParsers>()
                {
                    new CdnConfig.CdnParsers() {
                        Name = "X-Cache",
                        Type = CdnConfig.CdnParserType.TypeAndHost,
                        Regex = new Regex(@"(.+)\sfrom.+AkamaiGHost\/(.+)\).*")
                    }
                },
                requestHeaders = new Dictionary<string, string>()
                {
                    ["Pragma"] = "akamai-x-cache-on"
                },
                requestMethod = HttpMethod.Get
            },
            ["Cloudfront"] = new CdnConfig()
            {
                CdnName = "CLOUDFRT",
                Parsers = new List<CdnConfig.CdnParsers>()
                {
                    new CdnConfig.CdnParsers() {
                        Name = "X-Amz-Cf-Id",
                        Type = CdnConfig.CdnParserType.Host,
                        Regex = new Regex(@"(.+)")
                    },
                    new CdnConfig.CdnParsers() {
                        Name = "X-Cache",
                        Type = CdnConfig.CdnParserType.Type,
                        Regex = new Regex(@"(\S+)\s.+")
                    }
                },
            },
            ["Highwinds"] = new CdnConfig()
            {
                CdnName = "HIGHNEGR",
                Parsers = new List<CdnConfig.CdnParsers>()
                {
                    new CdnConfig.CdnParsers() {
                        Name = "X-HW",
                        Type = CdnConfig.CdnParserType.HostAndType,
                        Regex = new Regex(@".+,[0-9]+\.(.+)\.(.+)")
                    }
                },
            },
            ["Fastly"] = new CdnConfig()
            {
                CdnName = "FASTLY",
                Parsers = new List<CdnConfig.CdnParsers>()
                {
                    new CdnConfig.CdnParsers() {
                        Name = "X-Served-By",
                        Type = CdnConfig.CdnParserType.Host,
                        Regex = new Regex(@"([^,\s]+)$")
                    },
                    new CdnConfig.CdnParsers() {
                        Name = "X-Cache",
                        Type = CdnConfig.CdnParserType.Type,
                        Regex = new Regex(@"([^,\s]+)$")
                    }
                }
            },
            ["Edgecast"] = new CdnConfig()
            {
                CdnName = "EDGECAST",
                Parsers = new List<CdnConfig.CdnParsers>()
                {
                    new CdnConfig.CdnParsers() {
                        Name = "Server",
                        Type = CdnConfig.CdnParserType.Host,
                        Regex = new Regex(@"\((.+)\/.+\)")
                    },
                    new CdnConfig.CdnParsers() {
                        Name = "X-Cache",
                        Type = CdnConfig.CdnParserType.Type,
                        Regex = new Regex(@"(.+)")
                    }
                }
            },
            ["Balancer"] = new CdnConfig()
            {
                Parsers = new List<CdnConfig.CdnParsers>()
                {
                    new CdnConfig.CdnParsers() {
                        Name = null,
                        Type = CdnConfig.CdnParserType.Host,
                        Regex = new Regex(@"(.+)")
                    }
                }
            }
        };
    }  
}
