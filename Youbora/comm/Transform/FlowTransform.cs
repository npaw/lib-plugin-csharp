﻿using System.Collections.Generic;
using Youbora.constants;
using Youbora.comm.request;

namespace Youbora.comm.transform.flowTransform
{
    public class FlowTransform : Transform
    {
        private List<string> services = new List<string>(){Service.INIT, Service.START}; // and offline events!
        
        public override bool IsBlocking(YBRequest request)
        {
            if (isBusy && request != null)
            {
                if (services.Contains(request.Service))
                {
                    Done();
                }
                else if (request.Service == Service.ERROR)
                {
                    return false;
                }
            }
            return base.IsBlocking();
        }

        public override void Parse(YBRequest request)
        {
            return;
        }
    }
}
