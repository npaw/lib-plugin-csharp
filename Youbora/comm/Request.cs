﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;
using Youbora.log;
using Youbora.emitter;

namespace Youbora.comm.request
{
    public class YBRequest : Emitter
    {

        public static class Event {
            /** Request successful */
            public static string SUCCESS = "load";
            /** Request successful */
            public static string ERROR = "error";
            /** Request aborted */
            public static string ABORT = "abort";
        }

        public static Dictionary<string, object> DefaultConfig = new Dictionary<string, object>()
        {
            ["method"] = HttpMethod.Get,
            ["requestHeaders"] = new Dictionary<string, string>(),
            ["maxRetries"] = 3,
            ["retryAfter"] = 5000
        };

        public static Action<HttpResponseMessage> OnEverySuccess;

        public string Host { get; set; }
        public string Service { get; set; }
        public Dictionary<string, object> Params { get; set; }
        public Dictionary<string, object> Config { get; set; }
        public HttpClient HttpRequest { get; set; }
        public Action<HttpResponseMessage> OnSuccess;
        public Action OnFailure;

        private int retries = 0;

        public YBRequest(string res) : this(res, "", null) { }
        public YBRequest(string host, string service, Dictionary<string, object> args) : this(host, service, args, null) { }
        public YBRequest(string host, string service, Dictionary<string, object> args, Dictionary<string, object> conf)
        {
            try
            {
                this.Host = host;
                this.Service = service;
                this.Params = args;

                conf = conf ?? DefaultConfig;
                this.Config = new Dictionary<string, object>();
                this.Config["method"] = conf.ContainsKey("method") ? conf["method"] : YBRequest.DefaultConfig["method"];
                this.Config["requestHeaders"] = conf.ContainsKey("requestHeaders") ? conf["requestHeaders"] : YBRequest.DefaultConfig["requestHeaders"];
                this.Config["maxRetries"] = conf.ContainsKey("maxRetries") ? conf["maxRetries"] : YBRequest.DefaultConfig["maxRetries"];
                this.Config["retryAfter"] = conf.ContainsKey("retryAfter") ? conf["retryAfter"] : YBRequest.DefaultConfig["retryAfter"];
            }
            catch (Exception ex)
            {
                Log.Exception(ex);
            }
        }

        public string GetUrl()
        {
            try
            {
                return this.Host + this.Service + this.GetParams();
            }
            catch (Exception ex)
            {
                Log.Exception(ex);
                return "";
            }
        }

        public string GetParams()
        {
            try
            {
                if (this.Params != null && this.Params.Count > 0)
                {
                    var ret = "?";

                    foreach (KeyValuePair<string, object> kv in this.Params)
                    {
                        if (kv.Value != null)
                        {
                            ret += kv.Key + "=";
                            if (typeof(Dictionary<string, Object>) == kv.Value.GetType())
                            {
                                ret += Uri.EscapeDataString(JsonConvert.SerializeObject(kv.Value));
                            }
                            else if (typeof(bool) == kv.Value.GetType())
                            {
                                ret += kv.Value.ToString().ToLower();
                            }
                            else
                            {
                                ret += Uri.EscapeDataString(kv.Value.ToString());
                            }
                            ret += "&";
                        }
                    }
                    return ret.Remove(ret.Length - 1);
                }
                else
                {
                    return "";
                }
            }
            catch (Exception ex)
            {
                Log.Exception(ex);
                return "";
            }
        }

        public void SetParam(string key, object value)
        {
            try
            {
                if (key != null)
                {
                    this.Params[key] = value;
                }
            }
            catch (Exception ex)
            {
                Log.Exception(ex);
            }
        }

        public async void Send()
        {
            try
            {
                using (this.HttpRequest = new HttpClient())
                {
                    var url = this.GetUrl();
                    Log.Notice("XHR Req: " + url);

                    try
                    {
                        var mtd = this.Config["method"];
                        var method = (HttpMethod)mtd;
                        HttpRequestMessage request = new HttpRequestMessage(method, url);
                        HttpResponseMessage response;

                        if (this.Config["requestHeaders"] != null)
                        {
                            foreach (var kv in (Dictionary<string, string>)this.Config["requestHeaders"])
                            {
                                request.Headers.Add(kv.Key, kv.Value);
                            }
                        }

                        if ((HttpMethod)this.Config["method"] == HttpMethod.Head)
                        {
                            response = await this.HttpRequest.SendAsync(request, HttpCompletionOption.ResponseHeadersRead);
                        }
                        else
                        {
                            response = await this.HttpRequest.SendAsync(request);

                        }
                        response.EnsureSuccessStatusCode();

                        // On Success
                        Log.Debug("Request status code: {0}",response.StatusCode.ToString());
                        this.OnSuccess?.Invoke(response);
                        YBRequest.OnEverySuccess?.Invoke(response);
                    }
                    catch (HttpRequestException)
                    {
                        // On Failure
                        if (this.OnFailure != null)
                        {
                            this.OnFailure.Invoke();
                        }
                        else
                        {
                            // Default retry
                            this.retries++;
                            if (this.retries > (int)this.Config["maxRetries"])
                            {
                                Log.Error("Error: Aborting failed request. Max retries reached.");
                            }
                            else
                            {
                                Log.Warn("Warning: Request failed. Retrying in {0}ms ({1} of {2}).", this.Config["retryAfter"], this.retries, this.Config["maxRetries"]);
                                await Task.Delay((int)this.Config["retryAfter"]);
                                this.Send();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Exception(ex);
            }
        }


    }
}
