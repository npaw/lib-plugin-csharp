﻿using System;

namespace Youbora.util
{
    public static class Util
    {
        public static string AddProtocol(string url, bool httpSecure)
        {

            if (httpSecure == true)
            {
                return "https://" + url;
            }
            else
            {
                return "http://" + url;
            }
        }

        public static string BuildRenditionString(double bitrate)
        {
            if (bitrate > 0)
            {
                if (bitrate < 1000)
                {
                    return Math.Round(bitrate).ToString() + "bps";
                }
                else if (bitrate < 1000000)
                {
                    return Math.Round(bitrate/1000).ToString() + "Kbps";
                 }
                else
                {
                    return (Math.Round(bitrate/10000)/100).ToString() + "Mbps";
                }
            }
            return "";
        }

        public static string BuildRenditionString(int width, int height, double bitrate)
        {
            if (width == 0 || height == 0) return BuildRenditionString(bitrate);
            string newBitrate = BuildRenditionString(bitrate);
            string rendition = width.ToString() + "x" + height.ToString();
            if (newBitrate != null && newBitrate != "")
            {
                rendition += "@" + newBitrate;
            }
            return rendition;
        }

        public static string StripProtocol(string host)
        {
            if (host.StartsWith("//"))
            {
                host = host.Substring(2);
            }
            else if (host.StartsWith("http://"))
            {
                host = host.Substring(7);
            }
            else if (host.StartsWith("https://"))
            {
                host = host.Substring(8);
            }

            return host;
        }

        public static double GetUnixTimestampNow()
        {
            return (double)(Int64)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalMilliseconds;
        }
    }
}
