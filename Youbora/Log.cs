﻿using System;

namespace Youbora.log
{
    /// <summary>
    /// Logging class.
    /// </summary>
    public static class Log
    {
        public static class Level
        {
            public static int SILENT = 6;
            public static int ERROR = 5;
            public static int WARNING = 4;
            public static int NOTICE = 3;
            public static int DEBUG = 2;
            public static int VERBOSE = 1;
        }
        /// <summary>
        /// Define an action that will be executed each time Youbora generates a log.
        /// </summary>
        public static Action<string> RemoteLog { get; set; }

        /// <summary>
        /// Youbora will show all messages inferior to this level:
        /// 0: no errors;
        /// 1: errors;
        /// 2: + warnings (DEFAULT);
        /// 3: + life-cycle logs;
        /// 4: + debug messages;
        /// 5: + expose HTTP requests;
        /// </summary>
        public static int DebugLevel { get; set; } = 4;

        /// <summary>
        /// Logs a console message.
        /// </summary>
        /// <param name="msg">Message string</param>
        /// <param name="errorLevel">Defines the level of the error sent. Only errors with level lower than DebugLevel will be displayed.</param>
        public static void Report(string msg, int errorLevel)
        {
            if (errorLevel >= Log.DebugLevel)
            {
                msg = string.Format("[youbora:{0}] {1}", errorLevel, msg);
                System.Diagnostics.Debug.WriteLine(msg);
                RemoteLog?.Invoke(msg);
            }
        }

        /// <summary>
        /// Logs a console message for a exception.
        /// </summary>
        /// <param name="ex">Exception</param>
        /// <param name="errorLevel">Defines the level of the error sent. Only errors with level lower than DebugLevel will be displayed.</param>
        public static void Report(Exception ex, int errorLevel)
        {
            Report(ex.Message + "\r\n" + ex.StackTrace.ToString(), errorLevel);
        }

        /// <summary>
        /// Logs a console message for a exception. Level 5.
        /// </summary>
        public static void Exception(Exception ex)
        {
            Report(ex, 5);
        }

        /// <summary>
        /// Logs a console message for a exception.
        /// </summary>
        public static void Exception(Exception ex, int errorLevel)
        {
            Report(ex, errorLevel);
        }

        /// <summary>
        /// Logs a console message. Level 5.
        /// </summary>
        public static void Error(string msg, params object[] args)
        {
            Error(string.Format(msg, args));
        }

        /// <summary>
        /// Logs a console message. Level 5.
        /// </summary>
        public static void Error(string msg)
        {
            Report(msg, 5);
        }

        /// <summary>
        /// Logs a console message. Level 4.
        /// </summary>
        public static void Warn(string msg, params object[] args)
        {
            Warn(string.Format(msg, args));
        }

        /// <summary>
        /// Logs a console message. Level 4.
        /// </summary>
        public static void Warn(string msg)
        {
            Report(msg, 4);
        }

        /// <summary>
        /// Logs a console message. Level 3.
        /// </summary>
        public static void Notice(string msg, params object[] args)
        {
            Notice(string.Format(msg, args));
        }

        /// <summary>
        /// Logs a console message. Level 3.
        /// </summary>
        public static void Notice(string msg)
        {
            Report(msg, 3);
        }

        /// <summary>
        /// Logs a console message. Level 2.
        /// </summary>
        public static void Debug(string msg, params object[] args)
        {
            Debug(string.Format(msg, args));
        }

        /// <summary>
        /// Logs a console message. Level 2.
        /// </summary>
        public static void Debug(string msg)
        {
            Report(msg, 2);
        }

        /// <summary>
        /// Logs a console message. Level 1.
        /// </summary>
        public static void Verbose(string msg, params object[] args)
        {
            Verbose(string.Format(msg, args));
        }

        /// <summary>
        /// Logs a console message. Level 1.
        /// </summary>
        public static void Verbose(string msg)
        {
            Report(msg, 1);
        }
    }
}
