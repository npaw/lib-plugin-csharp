﻿using System;
using Youbora.timer;
using Youbora.chrono;

namespace Youbora.adapter.playheadMonitor
{
    public class PlayheadMonitor
    {
        public static class Type
        {
            public static int SEEK = 2;
            public static int BUFFER = 1;
            public static int NONE = 0;
        }
        public static double kBUFFER_TRHESHOLD_RATIO = 0.5;
        public static double kSEEK_THRESHOLD_RATIO = 2;
        private Adapter adapter = null;
        private bool seekEnabled = false;
        private bool bufferEnabled = false;
        public int interval = 800;
        private Chrono chrono = null;
        private static double lastPlayhead = 0;
        private Timer timer = null;


        public PlayheadMonitor(Adapter newAdapter, int type)
        {
            ConstructorMethod(newAdapter, type);
        }

        public PlayheadMonitor(Adapter newAdapter, int type, int newInterval)
        {
            interval = newInterval;
            ConstructorMethod(newAdapter, type);
        }

        private void ConstructorMethod(Adapter newAdapter, int type)
        {
            adapter = newAdapter;
            seekEnabled = type > 1;
            bufferEnabled = type > 0 && type != 2;
            chrono = new Chrono();
            lastPlayhead = 0;
            if (interval > 0)
            {
                timer = new Timer(Progress, interval);
            }
        }

        public void Start()
        {
            if (timer != null)
            {
                timer.Start();
            }
        }

        public void Stop()
        {
            if (timer != null)
            {
                timer.Stop();
            }
        }

        public void SkipNextTick()
        {
            lastPlayhead = 0;
        }

        private void Progress(object state)
        {
            // Reset timer
            double deltatime = chrono.Stop();
            chrono.Start();

            // Define thresholds
            double bufferThreshold = deltatime * kBUFFER_TRHESHOLD_RATIO;
            double seekThreshold = deltatime * kSEEK_THRESHOLD_RATIO;

            if (adapter != null && adapter.GetPlayrate() > 0)
            {
                bufferThreshold *= adapter.GetPlayrate();
                seekThreshold *= adapter.GetPlayrate();
            }

            // Calculate diff playhead
            double currentPlayhead = GetPlayhead();
            double diffPlayhead = Math.Abs(lastPlayhead - currentPlayhead) * 1000;

            if (diffPlayhead < bufferThreshold)
            {
                // Playhead is stalling > buffer
                if (bufferEnabled && lastPlayhead > 0 && !adapter.flags.isPaused && !adapter.flags.isSeeking)
                {
                    adapter.FireBufferBegin(false);
                }
            }
            else if (diffPlayhead > seekThreshold)
            {
                // Playhead has jumped > seek
                if (seekEnabled && lastPlayhead > 0)
                {
                    adapter.FireSeekBegin(true);
                }
            }
            else
            {
                // Healthy
                if (seekEnabled)
                {
                    adapter.FireSeekEnd();
                }
                if (bufferEnabled)
                {
                    adapter.FireBufferEnd();
                }
            }

            // Update Playhead
            lastPlayhead = currentPlayhead;
        }

        private double GetPlayhead()
        {
            return adapter.GetPlayhead();
        }

    }
}
