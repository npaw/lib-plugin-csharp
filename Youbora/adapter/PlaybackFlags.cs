﻿namespace Youbora.adapter.playbackFlags
{
    public class PlaybackFlags
    {
        public bool isStarted = false;
        public bool isJoined = false;
        public bool isPaused = false;
        public bool isSeeking = false;
        public bool isBuffering = false;
        public bool isEnded = false;
        public bool isStopped = false;
        public int lastQuartileSent = 0;

        public PlaybackFlags()
        {
            Reset();
        }

        public void Reset()
        {
            isStarted = false;
            isJoined = false;
            isPaused = false;
            isSeeking = false;
            isBuffering = false;
            isEnded = false;
            isStopped = false;
            lastQuartileSent = 0;
        }
    }
}
