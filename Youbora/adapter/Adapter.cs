﻿using System.Collections.Generic;
using Youbora.emitter;
using Youbora.log;
using Youbora.version;
using Youbora.adapter.playbackChronos;
using Youbora.adapter.playbackFlags;
using Youbora.adapter.playheadMonitor;
using Youbora.plugin;
using System;
using Youbora.chrono;
using Youbora.constants;

namespace Youbora.adapter
{
    public abstract class Adapter: Emitter
    {

        public PlaybackFlags flags = new PlaybackFlags();
        public PlaybackChronos chronos = new PlaybackChronos();
        public PlayheadMonitor monitor = null;
        public object player = null;
        public Plugin plugin;
        private bool? isAdsAdapter = null;

        public Adapter(object playerref)
        {
            SetPlayer(playerref);
            Log.Notice("Adapter {0} with lib {1} is ready", GetVersion(), LibVersion.VERSION);
        }

        public void SetPlayer(object playerref)
        {
            if (player != null) UnregisterListeners();
            player = playerref;
            if (player != null) RegisterListeners();
        }

        public abstract void RegisterListeners();

        public abstract void UnregisterListeners();


        public void Dispose()
        {
            if (monitor != null)
            {
                monitor.Stop();
            }
            FireStop();
            UnregisterListeners();
            player = null;
        }

        public void Monitorplayhead(bool bufferMonitor, bool seekMonitor)
        {
            int type = 0;
            if (bufferMonitor) type |= PlayheadMonitor.Type.BUFFER;
            if (seekMonitor) type |= PlayheadMonitor.Type.SEEK;
            monitor = new PlayheadMonitor(this, type);
        }
        public void Monitorplayhead(bool bufferMonitor, bool seekMonitor, int interval)
        {
            int type = 0;
            if (bufferMonitor) type |= PlayheadMonitor.Type.BUFFER;
            if (seekMonitor) type |= PlayheadMonitor.Type.SEEK;
            monitor = new PlayheadMonitor(this, type, interval);
        }

        public virtual double GetPlayhead()
        {
            return 0;
        }

        public virtual double GetPlayrate()
        {
            if (flags.isPaused)
            {
                return 0;
            }
            return 1;
        }

        public virtual double? GetFramesPerSecond()
        {
            return null;
        }

        public virtual double GetDroppedFrames()
        {
            return 0;
        }

        public virtual double? GetDuration()
        {
            return null;
        }

        public virtual double GetBitrate()
        {
            return -1;
        }

        public virtual double? GetTotalBytes()
        {
            return null;
        }

        public virtual double GetThroughput()
        {
            return -1;
        }

        public virtual string GetRendition()
        {
            return null;
        }

        public virtual string GetTitle()
        {
            return null;
        }

        public virtual string GetTitle2()
        {
            return null;
        }

        public virtual bool? GetIsLive()
        {
            return false;
        }

        public virtual string GetResource()
        {
            return null;
        }

        public virtual string GetPlayerVersion()
        {
            return null;
        }

        public virtual string GetPlayerName()
        {
            return null;
        }

        public virtual string GetVersion()
        {
            return "-generic-csharp";
        }

        public virtual double? GetCdnTraffic()
        {
            return null;
        }

        public virtual double? GetP2PTraffic()
        {
            return null;
        }

        public virtual double? GetUploadTraffic()
        {
            return null;
        }

        public virtual bool? GetIsP2PEnabled()
        {
            return false;
        }

        public virtual string GetHouseholdId()
        {
            return null;
        }

        public virtual double? GetLatency()
        {
            return null;
        }

        public virtual double? GetPacketLoss()
        {
            return null;
        }

        public virtual double? GetPacketSent()
        {
            return null;
        }

        public virtual string GetAudioCodec()
        {
            return null;
        }

        public virtual string GetVideoCodec()
        {
            return null;
        }

        public virtual string GetURLToParse()
        {
            return null;
        }

        public virtual Dictionary<string,object> GetMetrics()
        {
            return null;
        }

        // Ads only

        public virtual string GetPosition()
        {
            return null;
        }

        public virtual int? GetGivenBreaks()
        {
            return null;
        }

        public virtual int? GetExpectedBreaks()
        {
            return null;
        }

        public virtual Dictionary<string,List<double>> GetExpectedPattern()
        {
            return null;
        }

        public virtual List<double> GetBreaksTime()
        {
            return null;
        }

        public virtual int? GetGivenAds()
        {
            return null;
        }

        public virtual int? GetExpectedAds()
        {
            return null;
        }

        public virtual bool? GetIsVisible()
        {
            return null;
        }

        public virtual bool? GetAudioEnabled()
        {
            return null;
        }

        public virtual bool? GetIsSkippable()
        {
            return null;
        }

        public virtual bool? GetIsFullscreen()
        {
            return null;
        }

        public virtual string GetCampaign()
        {
            return null;
        }

        public virtual string GetCreativeId()
        {
            return null;
        }

        public virtual string GetProvider()
        {
            return null;
        }

        /* 
         * Fire Events
         */
        public virtual void FireInit()
        {
            FireInit(new Dictionary<string, object>());
        }
        public virtual void FireInit(Dictionary<string,object> parameters)
        {
            if (this.plugin != null)
            {
                this.plugin.FireInit(parameters);
            }
        }

        public virtual void FireStart()
        {
            FireStart(new Dictionary<string, object>());
        }
        public virtual void FireStart(Dictionary<string, object> parameters)
        {
            if (!flags.isStarted)
            {
                flags.isStarted = true;
                chronos.total.Start();
                chronos.join.Start();
                Emit(Event.START, parameters);
            }
        }

        public virtual void FireJoin()
        {
            FireJoin(new Dictionary<string, object>());
        }
        public virtual void FireJoin(Dictionary<string, object> parameters)
        {
            if (!flags.isJoined && !flags.isStarted && !IsAds() && plugin != null && plugin.isInitiated)
            {
                FireStart();
            }
            if (flags.isStarted && !flags.isJoined)
            {
                flags.isStarted = true;
                if (monitor != null) monitor.Start();
                flags.isJoined = true;
                chronos.join.Stop();
                Emit(Event.JOIN, parameters);
            }
        }

        public virtual void FirePause()
        {
            FirePause(new Dictionary<string, object>());
        }
        public virtual void FirePause(Dictionary<string, object> parameters)
        {
            if (flags.isBuffering)
            {
                FireBufferEnd();
            }
            if (flags.isJoined && !flags.isPaused)
            {
                flags.isPaused = true;
                chronos.pause.Start();
                Emit(Event.PAUSE, parameters);
            }
        }

        public virtual void FireResume()
        {
            FireResume(new Dictionary<string, object>());
        }
        public virtual void FireResume(Dictionary<string, object> parameters)
        {
            if (flags.isJoined && flags.isPaused)
            {
                chronos.pause.Stop();
                if (monitor != null) monitor.SkipNextTick();
                Emit(Event.RESUME, parameters);
                flags.isPaused = false;
            }
        }


        public virtual void FireBufferBegin()
        {
            FireBufferBegin(new Dictionary<string, object>(), false);
        }
        public virtual void FireBufferBegin(bool convertFromSeek)
        {
            FireBufferBegin(new Dictionary<string, object>(), convertFromSeek);
        }
        public virtual void FireBufferBegin(Dictionary<string, object> parameters)
        {
            FireBufferBegin(parameters, false);
        }
        public virtual void FireBufferBegin(Dictionary<string,object> parameters, bool convertFromSeek)
        {
            if (flags.isJoined && !flags.isBuffering)
            {
                if (flags.isSeeking)
                {
                    if (convertFromSeek)
                    {
                        Log.Notice("Converting current buffer to seek");
                        chronos.buffer.StartTime = chronos.seek.StartTime;
                        chronos.seek.Reset();
                        flags.isSeeking = false;
                    }
                    else
                    {
                        return;
                    }
                }
                else
                {
                    chronos.buffer.Start();
                }
                flags.isBuffering = true;
                Emit(Event.BUFFER_BEGIN, parameters);
            }
        }
        public virtual void FireBufferEnd()
        {
            FireBufferEnd(new Dictionary<string, object>());
        }
        public virtual void FireBufferEnd(Dictionary<string, object> parameters)
        {
            if (flags.isJoined && flags.isBuffering)
            {
                flags.isBuffering = false;
                chronos.buffer.Stop();
                if (monitor != null) monitor.SkipNextTick();
                Emit(Event.BUFFER_END, parameters);
            }
        }
        public virtual void CancelBuffer()
        {
            if (flags.isJoined && flags.isBuffering)
            {
                flags.isBuffering = false;
                chronos.buffer.Stop();
                if (monitor != null) this.monitor.SkipNextTick();
            }
        }

        public virtual void FireSeekBegin()
        {
            FireSeekBegin(new Dictionary<string, object>(), false);
        }
        public virtual void FireSeekBegin(bool convertFromBuffer)
        {
            FireSeekBegin(new Dictionary<string, object>(), convertFromBuffer);
        }
        public virtual void FireSeekBegin(Dictionary<string, object> parameters)
        {
            FireSeekBegin(parameters, false);
        }
        public virtual void FireSeekBegin(Dictionary<string,object> parameters, bool convertFromBuffer)
        {
            if (plugin != null && (bool)plugin.GetIsLive() && (bool)plugin.options["content.isLive.noSeek"]) return;
            if (flags.isJoined && !flags.isSeeking)
            {
                if (flags.isBuffering)
                {
                    if (convertFromBuffer != false)
                    {
                        Log.Notice("Converting current buffer to seek");
                        chronos.seek.StartTime = chronos.buffer.StartTime;
                        chronos.buffer.Reset();
                        flags.isBuffering = false;
                    }
                    else
                    {
                        return;
                    }
                }
                else
                {
                    chronos.seek.Start();
                }
                flags.isSeeking = true;
                Emit(Event.SEEK_BEGIN, parameters);
            }
        }

        public virtual void FireSeekEnd()
        {
            FireSeekEnd(new Dictionary<string, object>());
        }
        public virtual void FireSeekEnd(Dictionary<string, object> parameters)
        {
            if (plugin != null && (bool)plugin.GetIsLive() && (bool)plugin.options["content.isLive.noSeek"]) return;
            if (flags.isJoined && flags.isSeeking)
            {
                flags.isSeeking = false;
                chronos.seek.Stop();
                if (monitor != null) monitor.SkipNextTick();
                Emit(Event.SEEK_END, parameters);
            }
        }
        public virtual void CancelSeek()
        {
            if (flags.isJoined && flags.isSeeking)
            {
                flags.isSeeking = false;

                chronos.seek.Stop();

                if (monitor != null) monitor.SkipNextTick();
            }
        }

        public virtual void FireStop()
        {
            FireStop(new Dictionary<string, object>());
        }
        public virtual void FireStop(Dictionary<string, object> parameters)
        {
            if (IsAds() || (plugin != null && plugin.IsStopReady())) {
                if ((IsAds() && flags.isStarted) || (!IsAds() && (flags.isStarted || (plugin != null && plugin.isInitiated))))
                {
                    if (monitor != null) monitor.Stop();
                    flags.Reset();
                    chronos.total.Stop();
                    chronos.join.Stop();
                    chronos.pause.Stop();
                    chronos.buffer.Stop();
                    chronos.seek.Stop();

                    Emit(Event.STOP, parameters);

                    chronos.pause.Reset();
                    chronos.buffer.Reset();
                    chronos.seek.Reset();
                    chronos.viewedMax.RemoveRange(0, chronos.viewedMax.Count);
                }
            }
        }

        public virtual void FireError(string code = null, string message = null, object metadata = null, string level = null)
        {
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            if (code != null) parameters["errorCode"] = code;
            if (message != null) parameters["msg"] = message;
            if (metadata != null) parameters["errorMetadata"] = metadata;
            if (level != null) parameters["errorLevel"] = level;
            FireError(parameters);
        }
        public virtual void FireError(Dictionary<string, object> parameters)
        {
            string[] ignored = (string[])plugin.options["errors.ignore"];
            string[] fatal = (string[])plugin.options["errors.fatal"];
            string code = parameters.ContainsKey("errorCode") ? (string)parameters["errorCode"] : null;

            if  (code != null && Array.IndexOf(ignored, code) >= 0)
            {
                // ignore error
            } 
            else 
            {
                Emit(Event.ERROR, parameters);
                if (code != null && Array.IndexOf(fatal, code) >= 0) {
                    FireStop();
                 }
            }
        }

        public virtual void FireFatalError(string code = null, string message = null, object metadata = null, string level = null)
        {
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            if (code != null) parameters["errorCode"] = code;
            if (message != null) parameters["msg"] = message;
            if (metadata != null) parameters["errorMetadata"] = metadata;
            if (level != null) parameters["errorLevel"] = level;
            FireFatalError(parameters);
        }

        public virtual void FireFatalError(Dictionary<string, object> parameters)
        {
            string[] ignored = (string[])plugin.options["errors.ignore"];
            string[] nonfatal = (string[])plugin.options["errors.nonFatal"];
            string code = parameters.ContainsKey("errorCode") ? (string)parameters["errorCode"] : null;

            if (code != null && Array.IndexOf(ignored, code) >= 0)
            {
                // ignore error
            }
            else
            {
                Emit(Event.ERROR, parameters);
                if (code == null || Array.IndexOf(nonfatal, code) < 0)
                {
                    FireStop();
                }
            }
        }

        public virtual void FireCasted()
        {
            FireCasted(new Dictionary<string, object>());
        }
        public virtual void FireCasted(Dictionary<string, object> parameters)
        {
            parameters["casted"] = true;
            this.FireStop(parameters);
        }

        public virtual void FireClick()
        {
            FireClick(new Dictionary<string, object>());
        }
        public virtual void FireClick(string url)
        {
            var parameters = new Dictionary<string, object>();
            parameters["url"] = url;
            FireClick(parameters);
        }
        public virtual void FireClick(Dictionary<string, object> parameters)
        {
            Emit(Event.CLICK, parameters);
        }

        // Only for ads

        public virtual void FireQuartie(int quartileNumber)
        {
            FireQuartile(new Dictionary<string, object>()
            {
                ["quartile"] = quartileNumber
            });
        }
        public virtual void FireQuartile(Dictionary<string,object> parameters)
        {
            try
            {
                int quartileNumber = (int)parameters["quartile"];
                if (flags.isStarted && quartileNumber < 4 && quartileNumber > flags.lastQuartileSent)
                {
                    flags.lastQuartileSent++;
                    Emit(Event.QUARTILE, parameters);
                }
            } catch (Exception ex)
            {
                Log.Error(ex.ToString());
            }

        }
        public void StartChronoView()
        {
            if (GetIsVisible() != null && !plugin.backgroundDetector.isInBackground)
            {
                if (chronos.viewedMax.Count == 0 || chronos.viewedMax[chronos.viewedMax.Count - 1].LastTime != null)
                {
                    chronos.viewedMax.Add(new Chrono());
                    chronos.viewedMax[chronos.viewedMax.Count - 1].Start();
                }
            }
        }

        public void StopChronoView()
        {
            if (chronos.viewedMax.Count > 0 && chronos.viewedMax[chronos.viewedMax.Count - 1].LastTime == null)
            {
                chronos.viewedMax[chronos.viewedMax.Count - 1].Stop();
            }
        }

        public void FireManifest(string type, string message)
        {
            FireManifest(new Dictionary<string, object>() {
                ["errorType"] = type,
                ["errorMessage"] = message
            });
        }
        public void FireManifest()
        {
            FireManifest(new Dictionary<string, object>());
        }
        public void FireManifest(Dictionary<string, object> dimensions)
        {
            Emit(Event.MANIFEST, dimensions);
        }

        public void FireSkip()
        {
            FireSkip(new Dictionary<string, object>());
        }
        public void FireSkip(Dictionary<string, object> parameters)
        {
            parameters["skipped"] = true;
            FireStop(parameters);
        }

        public void FireBreakStart()
        {
            FireBreakStart(new Dictionary<string, object>());
        }
        public void FireBreakStart(Dictionary<string, object> parameters)
        {
            if (!plugin.isBreakStarted)
            {
                plugin.isBreakStarted = true;
                Emit(Event.PODSTART, parameters);
            }
        }

        public void FireBreakStop()
        {
            FireBreakStop(new Dictionary<string, object>());
        }
        public void FireBreakStop(Dictionary<string, object> parameters)
        {
            if (plugin.isBreakStarted)
            {
                Emit(Event.PODSTOP, parameters);
                plugin.isBreakStarted = false;
            }
        }

        public virtual void FireEvent(string eventName, Dictionary<string, object> dimensions, Dictionary<string, object> values, Dictionary<string, object> topLevelDimensions = null)
        {
            Dictionary<string,object> parameters = topLevelDimensions == null ? new Dictionary<string, object>() : topLevelDimensions;
            parameters["dimensions"] = dimensions;
            parameters["values"] = values;
            parameters["name"] = eventName;
            Dictionary<string, object> returnDict = new Dictionary<string, object>()
            {
                ["params"] = parameters
            };
            Emit(Event.VIDEO_EVENT, returnDict);
        }

        private bool IsAds()
        {
            bool ret = false;
            if (isAdsAdapter != null)
            {
                ret = (bool)isAdsAdapter;
            }
            return ret;
        }

        public void SetIsAdsAdapter(bool value)
        {
            isAdsAdapter = value;
        }
    }
}

