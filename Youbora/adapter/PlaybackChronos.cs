﻿using System.Collections.Generic;
using Youbora.chrono;

namespace Youbora.adapter.playbackChronos
{
    public class PlaybackChronos
    {

        public Chrono join = null;
        public Chrono seek = null;
        public Chrono pause = null;
        public Chrono buffer = null;
        public Chrono total = null;
        public List<Chrono> viewedMax = null;

        public PlaybackChronos()
        {
            Reset();
        }

        public void Reset()
        {
            join = new Chrono();
            seek = new Chrono();
            pause = new Chrono();
            buffer = new Chrono();
            total = new Chrono();
            viewedMax = new List<Chrono>();
        }
    }
}
