﻿namespace Youbora.version
{
    public static class LibVersion
    {
        public static string VERSION { get; } = "6.5.19";
    }
}
