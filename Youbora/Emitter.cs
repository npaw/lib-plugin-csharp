﻿using System;
using System.Collections.Generic;

namespace Youbora.emitter
{
    public class Emitter
    {
        private Dictionary<string, List<Action>> listeners = new Dictionary<string, List<Action>>();
        private Dictionary<string, List<Action<object>>> listenersArg = new Dictionary<string, List<Action<object>>>();
        public void On(string eventName, Action callback)
        {
            if (!listeners.ContainsKey(eventName))
            {
                listeners[eventName] = new List<Action>();
            }
            listeners[eventName].Add(callback);
        }

        public void On(string eventName, Action<object> callback)
        {
            if (!listenersArg.ContainsKey(eventName))
            {
                listenersArg[eventName] = new List<Action<object>>();
            }
            listenersArg[eventName].Add(callback);
        }
        public void Off(string eventName, Action callback)
        {
            if (listeners.ContainsKey(eventName))
            {
                listeners[eventName].Remove(callback);
            }
        }

        public void Off(string eventName, Action<object> callback)
        {
            if (listenersArg.ContainsKey(eventName))
            {
                listenersArg[eventName].Remove(callback);
            }
        }

        public void Emit(string eventName, object data)
        {
            if (listeners.ContainsKey(eventName))
            {
                foreach (Action callback in listeners[eventName])
                {
                    callback();
                }
            }
            if (listenersArg.ContainsKey(eventName))
            {
                foreach (Action<object> callback in listenersArg[eventName])
                {
                    callback(data);
                }
            }
        }
    }
}
