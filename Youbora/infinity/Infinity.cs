﻿using System;
using System.Collections.Generic;
using Youbora.emitter;
using Youbora.comm.communication;
using Youbora.plugin;
using Youbora.plugin.storage;
using Youbora.util;

namespace Youbora.infinity
{
    public class Infinity: Emitter
    {
        public static class Event
        {
            public static string NAV = "nav";
            public static string SESSION_START = "sessionStart";
            public static string SESSION_STOP = "sessionStop";
            public static string BEAT = "beat";
            public static string EVENT = "event";
        }

        private Plugin pluginRef = null;
        private Communication comm = null;
        private DateTime? firstActive = null;
        public bool infinityStarted = false;
        public bool infinityStopped = false;
        public string context = null;
        private Dictionary<string, object> RegisteredProperties = null;

        public DateTime GetFirstActive()
        {
            if (firstActive != null)
            {
                return (DateTime) firstActive;
            }
            if (plugin.storage.Storage.GetValue("lastactive") != null) {
                return (DateTime) plugin.storage.Storage.GetValue("lastactive");
            }
            return DateTime.Now;
        }

        private object registeredProperties = null;
        public Infinity(Plugin plugin)
        {
            this.pluginRef = plugin;
            infinityStarted = false;
            infinityStopped = false;
        }   

        public void AndBeyond(Dictionary<string, object> dimensions)
        {
            Begin(dimensions);
        }

        public void Begin()
        {
            Begin(new Dictionary<string, object>());
        }

        public void Begin(Dictionary<string, object> dimensions)
        {
            this.registeredProperties = Storage.GetValue("inifnityRegisteredProperties");
            if (pluginRef.GetContext() != null)
            {
                FireNav(dimensions); // returning
            } 
            else 
            {
                FireSessionStart(dimensions);
            }
        }

        public void FireNav(Dictionary<string,object> dimensions)
        {
            if (!infinityStarted || infinityStopped) return;
            if (pluginRef.GetContext() == null) GenerateNewContext();
            Emit(Event.NAV, GetParamsJson(dimensions, null , null, true, false));
            DateTime now = DateTime.Now;

            pluginRef.SendBeat(pluginRef.beat.Chrono.GetDeltaTime(false));
            pluginRef.beat.Chrono.StartTime = now;
            SetLastActive();
        }

        public void FireSessionStart(Dictionary<string, object> dimensions)
        {
            if (infinityStarted && !infinityStopped) return;
            if (infinityStopped)
            {
                pluginRef.RestartViewTransform();
            }
            infinityStarted = true;
            infinityStopped = false;
            comm = new Communication(pluginRef);
            comm.AddTransform(pluginRef.viewTransform);
            GenerateNewContext();
            Emit(Event.SESSION_START, GetParamsJson(dimensions, null, null, true, true));
            SetLastActive();
        }

        public void FireSessionStop()
        {
            infinityStopped = true;
            Emit(Event.SESSION_STOP, new Dictionary<string, object>());
            Storage.RemoveValue("data");
            Storage.RemoveValue("session");
            Storage.RemoveValue("lastactive");
            Storage.RemoveValue("context");
        }

        public void FireEvent(string eventName, Dictionary<string,object> dimensions, Dictionary<string,object> values, Dictionary<string, object> topLevelDimensions = null)
        {
            if (!infinityStarted || infinityStopped) return;
            Emit(Event.EVENT, GetParamsJson(dimensions, values, eventName, false, false, topLevelDimensions));
            SetLastActive();
        }

        public void Register(Dictionary<string, object> dimensions, Dictionary<string, object> values)
        {
            RegisteredProperties = new Dictionary<string,object>() {
                ["dimensions"] = dimensions,
                ["values"]= values
            };
            Storage.SetValue("inifnityRegisteredProperties", registeredProperties);
        }

        public void RegisterOnce(Dictionary<string, object> dimensions, Dictionary<string, object> values)
        {
            if (RegisteredProperties == null)
            {
                Register(dimensions, values);
            }
        }

        public void Unregister()
        {
            RegisteredProperties = null;
            Storage.RemoveValue("inifnityRegisteredProperties");
        }

        public Communication GetComm()
        {
            return comm;
        }

        private Dictionary<string, object> GetParamsJson(Dictionary<string, object> dimensions, Dictionary<string, object> values, string eventName, bool isNav, bool isStart, Dictionary<string, object> topLevelDimensions = null)
        {
            Dictionary<string, object> parameters = topLevelDimensions == null ? new Dictionary<string, object>() : topLevelDimensions;
            if (!isNav)
            {
                parameters["dimensions"] = dimensions;
                parameters["values"] = values;
                parameters["name"] = eventName;
            }
            Dictionary<string, object> returnDict = new Dictionary<string, object>() {
                ["params"] = parameters
            };
            if (isNav || isStart)
            {
                if (dimensions != null && dimensions.ContainsKey("page"))
                {
                    returnDict["page"] = dimensions["page"];
                    returnDict["route"] = dimensions["page"];
                    dimensions.Remove("page");
                }
                else
                {
                    returnDict["page"] = "-";
                    returnDict["route"] = "-";
                }
            }
            return returnDict;
        }

        private void GenerateNewContext()
        {
            context = Util.GetUnixTimestampNow().ToString();
            Storage.SetValue("context", context);
        }

        private void SetLastActive()
        {
            if (firstActive == null)
            {
                firstActive = GetFirstActive();
            }
            Storage.SetValue("lastactive", DateTime.Now);
        }

        public void NewSession(Dictionary<string,object> options)
        {
            FireSessionStop();
            Storage.SetValue("data", null);
            pluginRef.SetOptions(options);
            pluginRef.RestartViewTransform();
            FireSessionStart(null);
        }
    }
}
